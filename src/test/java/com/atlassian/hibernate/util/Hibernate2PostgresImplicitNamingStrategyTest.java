package com.atlassian.hibernate.util;

import com.atlassian.hibernate.adapter.ConfigurationV2Bridge;
import com.atlassian.hibernate.sample.model.SessionFactoryFactory;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.hibernate.dialect.PostgreSQL91Dialect;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;
import org.junit.Test;

import java.io.File;
import java.util.EnumSet;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Hibernate2PostgresImplicitNamingStrategyTest {
    @Test
    public void testSchemaExport() throws Exception {
        assertEquals(
                "alter table AuctionItem drop constraint FK522A9BD6C9FF4F7F\n" +
                "alter table AuctionItem drop constraint FK522A9BD657291C03\n" +
                "alter table Bid drop constraint FK104DDAD3189F4\n" +
                "alter table Bid drop constraint FK104DD317B13\n" +
                "drop table if exists AuctionItem cascade\n" +
                "drop table if exists AuctionUser cascade\n" +
                "drop table if exists Bid cascade\n" +
                "drop sequence if exists hibernate_sequence\n" +
                "create sequence hibernate_sequence start 1 increment 1\n" +
                "create table AuctionItem (id int8 not null, description varchar(255), ends timestamp, condition int4, seller int8 not null, successfulBid int8, primary key (id))\n" +
                "create table AuctionUser (id int8 not null, userName varchar(255) not null, \"password\" varchar(255), email varchar(255), firstName varchar(255), \"initial\" char(1), lastName varchar(255), primary key (id))\n" +
                "create table Bid (id int8 not null, isBuyNow char(1) not null, amount float4 not null, datetime timestamp not null, item int8 not null, bidder int8 not null, primary key (id))\n" +
                "alter table AuctionItem add constraint auctionitem_description_key unique (description)\n" +
                "alter table AuctionItem add constraint FK522A9BD6C9FF4F7F foreign key (seller) references AuctionUser\n" +
                "alter table AuctionItem add constraint FK522A9BD657291C03 foreign key (successfulBid) references Bid\n" +
                "alter table Bid add constraint FK104DDAD3189F4 foreign key (bidder) references AuctionUser\n" +
                "alter table Bid add constraint FK104DD317B13 foreign key (item) references AuctionItem",
                generateCreateScripts());
    }

    private String generateCreateScripts() throws Exception {
        SchemaExport schemaExport = new SchemaExport();
        File file = File.createTempFile("script", ".txt");
        try {
            schemaExport.setOutputFile(file.getAbsolutePath());
            ConfigurationV2Bridge config = SessionFactoryFactory.createConfig();
            config.getProperties().setProperty("hibernate.dialect", PostgreSQL91Dialect.class.getName());
            schemaExport.create(EnumSet.of(TargetType.SCRIPT), config.getMetadata());
            List<String> lines = Files.readLines(file, Charsets.UTF_8);
            return String.join("\n", lines);
        } finally {
            file.delete();
        }
    }
}
