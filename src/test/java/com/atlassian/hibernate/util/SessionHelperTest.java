package com.atlassian.hibernate.util;

import com.atlassian.hibernate.adapter.bridge.session.SessionBridgeAssociations;
import com.atlassian.hibernate.sample.model.Name;
import com.atlassian.hibernate.sample.model.User;
import com.atlassian.hibernate.test.HibernateAdapterTestBase;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SessionHelperTest extends HibernateAdapterTestBase {
    @Test
    public void testSaveWithIdentifier() throws Exception {
        doInV5Transaction(session -> {
            final User user = createTestUser("username", new Name("first", null, "last"), "email");
            Session realSession = SessionBridgeAssociations.getSessionBridge(session).getRealV5Session();
            SessionHelper.save(realSession, user, 200L);
        });
        doInV5Transaction(session -> {
            final User loaded = session.get(User.class, 200L);
            assertNotNull(loaded);
        });
    }

    @Test
    public void testCreateQuery_WithNamedParameters() {
        final Session session = Mockito.mock(Session.class);
        final Query query = Mockito.mock(Query.class);
        Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);

        final Query actual = SessionHelper.createQuery(
                session,
                "select * from table where column = :param1 and column2 = :param2",
                new Object[] { "value1", "value2" },
                new Type[] { StringType.INSTANCE, StringType.INSTANCE });

        assertEquals(query, actual);
        Mockito.verify(query).setParameter("param1", "value1", StringType.INSTANCE);
        Mockito.verify(query).setParameter("param2", "value2", StringType.INSTANCE);
    }

    @Test
    public void testCreateQuery_WithIndexParameters() {
        final Session session = Mockito.mock(Session.class);
        final Query query = Mockito.mock(Query.class);
        Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);

        final Query actual = SessionHelper.createQuery(
                session,
                "select * from table where column = ? and column2 = ?",
                new Object[] { "value1", "value2" },
                new Type[] { StringType.INSTANCE, StringType.INSTANCE });

        assertEquals(query, actual);
        Mockito.verify(query).setParameter(0, "value1", StringType.INSTANCE);
        Mockito.verify(query).setParameter(1, "value2", StringType.INSTANCE);
    }
}
