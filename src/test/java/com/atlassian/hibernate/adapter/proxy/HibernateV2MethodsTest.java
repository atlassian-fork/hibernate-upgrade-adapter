package com.atlassian.hibernate.adapter.proxy;

import com.atlassian.hibernate.sample.model.AuctionItem;
import com.atlassian.hibernate.sample.model.User;
import com.atlassian.hibernate.test.HibernateAdapterTestBase;
import net.sf.hibernate.Hibernate;
import net.sf.hibernate.HibernateException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class HibernateV2MethodsTest extends HibernateAdapterTestBase {
    @Test
    public void testGetClass() throws HibernateException {
        final long id = createTestAuctionItem();
        super.doInV2Transaction(session -> {
            final AuctionItem loaded = (AuctionItem) session.get(AuctionItem.class, id);
            assertNotEquals(User.class, loaded.getSeller().getClass());
            assertEquals(
                    "net.sf.hibernate.Hibernate.getClass should work on v5 hibernate proxies",
                    User.class, Hibernate.getClass(loaded.getSeller()));
        });
    }
}
