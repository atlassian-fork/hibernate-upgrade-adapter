package com.atlassian.hibernate.adapter.adapters.session;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.persister.AbstractCollectionPersisterV2Adapter;
import com.atlassian.hibernate.adapter.bridge.factory.SessionFactoryBridgeAssociations;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;

import static org.junit.Assert.assertNotNull;

public class AbstractCollectionPersisterV2AdapterTest {
    @Test
    public void testConstructor() {
        final org.hibernate.persister.collection.AbstractCollectionPersister persister =
                Mockito.mock(org.hibernate.persister.collection.AbstractCollectionPersister.class);

        final SessionFactoryWithBridge sessionFactory = Mockito.mock(SessionFactoryWithBridge.class);
        final net.sf.hibernate.engine.SessionFactoryImplementor sessionFactoryV2 =
                Mockito.mock(net.sf.hibernate.engine.SessionFactoryImplementor.class);
        Mockito.when(sessionFactory.getProperties()).thenReturn(new HashMap<>());
        SessionFactoryBridgeAssociations.associate(sessionFactory, sessionFactory);
        Mockito.when(sessionFactory.getV2orV5SessionFactory()).thenReturn(sessionFactoryV2);

        final net.sf.hibernate.collection.AbstractCollectionPersister adapted =
                AbstractCollectionPersisterV2Adapter.adaptCollectionPersister(persister, sessionFactory);
        assertNotNull("AbstractCollectionPersister v2 adapted successfully", adapted);
    }

    public interface SessionFactoryWithBridge extends org.hibernate.engine.spi.SessionFactoryImplementor, HibernateBridge {
    }
}
