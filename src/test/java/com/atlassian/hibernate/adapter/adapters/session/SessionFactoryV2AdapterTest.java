package com.atlassian.hibernate.adapter.adapters.session;

import com.atlassian.hibernate.adapter.ConfigurationV2Bridge;
import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.HibernateBridgeMode;
import com.atlassian.hibernate.sample.model.SessionFactoryFactory;
import com.atlassian.hibernate.test.HibernateAdapterTestBase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SessionFactoryV2AdapterTest extends HibernateAdapterTestBase {
    @Test
    public void testGetAllClassMetadata() throws Exception {
        bridgeMode = HibernateBridgeMode.V5_ADAPTER;
        assertEquals(4, sessionFactoryV2.getAllClassMetadata().size());
    }

    @Test
    public void testGetAllCollectionMetadata() throws Exception {
        bridgeMode = HibernateBridgeMode.V5_ADAPTER;
        assertEquals(3, sessionFactoryV2.getAllCollectionMetadata().size());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testGetDialect_WhenConfiguredWithV5Dialect() throws Exception {
        ConfigurationV2Bridge config = SessionFactoryFactory.createConfig();
        config.setProperty("hibernate.dialect", org.hibernate.dialect.H2Dialect.class.getName());
        config.setProperty("hibernate.atlassian.adapter.lazy.init.factories", "true");

        config.setBridgeMode(HibernateBridgeMode.V5_ADAPTER);
        final net.sf.hibernate.SessionFactory sessionFactory = config.buildSessionFactory();
        sessionFactoryV2 = HibernateBridge.get(sessionFactory).getV2orV5SessionFactory();
        sessionFactoryV5 = HibernateBridge.get(sessionFactory).getV5SessionFactory();
        assertEquals(net.sf.hibernate.dialect.H2Dialect.class, ((net.sf.hibernate.engine.SessionFactoryImplementor) sessionFactoryV2).getDialect().getClass());
        assertEquals(org.hibernate.dialect.H2Dialect.class, ((org.hibernate.engine.spi.SessionFactoryImplementor) sessionFactoryV5).getDialect().getClass());
    }
}
