package com.atlassian.hibernate.adapter.adapters.generator;

import com.atlassian.hibernate.adapter.generator.IdentifierGeneratorFactoryV5Bridge;
import org.hibernate.boot.registry.classloading.internal.ClassLoaderServiceImpl;
import org.hibernate.boot.registry.classloading.spi.ClassLoaderService;
import org.hibernate.dialect.SQLServerDialect;
import org.hibernate.engine.config.internal.ConfigurationServiceImpl;
import org.hibernate.engine.config.spi.ConfigurationService;
import org.hibernate.engine.jdbc.env.spi.IdentifierHelper;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.hibernate.envers.enhanced.OrderedSequenceGenerator;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.hibernate.type.StringType;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

public class IdentifierGeneratorFactoryV5BridgeTest {
    private IdentifierGeneratorFactoryV5Bridge bridge;

    private static Properties createConfig() {
        final Properties properties = new Properties();
        properties.put(IdentifierGenerator.ENTITY_NAME, "entityName");
        return properties;
    }

    @Before
    public void setUp() {
        bridge = new IdentifierGeneratorFactoryV5Bridge();

        final JdbcEnvironment jdbcEnvironment = Mockito.mock(JdbcEnvironment.class);
        Mockito.when(jdbcEnvironment.getDialect()).thenReturn(new SQLServerDialect());
        Mockito.when(jdbcEnvironment.getIdentifierHelper()).thenReturn(Mockito.mock(IdentifierHelper.class));

        final ServiceRegistryImplementor serviceRegistry = Mockito.mock(ServiceRegistryImplementor.class);
        Mockito.when(serviceRegistry.getService(JdbcEnvironment.class)).thenReturn(jdbcEnvironment);
        Mockito.when(serviceRegistry.getService(ClassLoaderService.class)).thenReturn(new ClassLoaderServiceImpl());
        Mockito.when(serviceRegistry.getService(ConfigurationService.class)).thenReturn(new ConfigurationServiceImpl(Collections.emptyMap()));
        bridge.injectServices(serviceRegistry);
    }

    @Test
    public void testCreateIdentifierGenerator_ForSequence() {
        testCreateIdentifierGenerator_ForSequence(
                "INCREMENT BY 10 START WITH 2",
                SequenceStyleGenerator.class);
    }

    @Test
    public void testCreateIdentifierGenerator_ForOrderedSequence() {
        testCreateIdentifierGenerator_ForSequence(
                "INCREMENT BY 10 START WITH 2 ORDER",
                OrderedSequenceGenerator.class);
    }

    void testCreateIdentifierGenerator_ForSequence(
            final String parameters, final Class expectedGeneratorClass) {
        final Properties config = createConfig();
        config.put("sequence", "x.SEQ_JOURNAL_ENTRY_ID");
        config.put("parameters", parameters);

        final SequenceStyleGenerator generator = (SequenceStyleGenerator) bridge.createIdentifierGenerator(
                "sequence", StringType.INSTANCE, config);
        assertEquals(expectedGeneratorClass, generator.getClass());
        assertEquals("initialValue", 2, generator.getDatabaseStructure().getInitialValue());
        assertEquals("incrementSize", 10, generator.getDatabaseStructure().getIncrementSize());
    }
}
