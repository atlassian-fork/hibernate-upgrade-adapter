package com.atlassian.hibernate.adapter.adapters;

import com.atlassian.hibernate.sample.model.AuctionItem;
import com.atlassian.hibernate.test.HibernateAdapterTestBase;
import net.sf.hibernate.Query;
import net.sf.hibernate.Session;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class QueryV2AdapterTest extends HibernateAdapterTestBase {
    @Test
    @SuppressWarnings("unchecked")
    public void testCreateSQLQuery_ParameterIndexZeroBased() throws Exception {
        Session session = sessionFactoryV2.openSession();
        try {
            createTestAuctionItem(session);
            Query query = session.createSQLQuery("select {item.*} from AuctionItem item where condition = ?", "item", AuctionItem.class);
            query.setLong(0, 2L);

            List<AuctionItem> list = query.list();
            assertEquals("1 result returned", 1, list.size());
        } finally {
            session.close();
        }
    }
}
