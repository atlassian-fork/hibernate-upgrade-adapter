package com.atlassian.hibernate.adapter.adapters;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FlushModeAdapterTest {
    @Test
    public void testV5toV2() {
        assertEquals(net.sf.hibernate.FlushMode.NEVER, FlushModeAdapter.adapt(org.hibernate.FlushMode.MANUAL));
        assertEquals(net.sf.hibernate.FlushMode.COMMIT, FlushModeAdapter.adapt(org.hibernate.FlushMode.COMMIT));
        assertEquals(net.sf.hibernate.FlushMode.AUTO, FlushModeAdapter.adapt(org.hibernate.FlushMode.AUTO));
        assertEquals(net.sf.hibernate.FlushMode.AUTO, FlushModeAdapter.adapt(org.hibernate.FlushMode.ALWAYS));
    }

    @Test
    public void testV2toV5() {
        assertEquals(org.hibernate.FlushMode.MANUAL, FlushModeAdapter.adapt(net.sf.hibernate.FlushMode.NEVER));
        assertEquals(org.hibernate.FlushMode.AUTO, FlushModeAdapter.adapt(net.sf.hibernate.FlushMode.AUTO));
        assertEquals(org.hibernate.FlushMode.COMMIT, FlushModeAdapter.adapt(net.sf.hibernate.FlushMode.COMMIT));
    }
}
