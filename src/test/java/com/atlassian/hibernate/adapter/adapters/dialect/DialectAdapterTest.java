package com.atlassian.hibernate.adapter.adapters.dialect;

import com.atlassian.hibernate.adapter.adapters.DialectAdapter;
import net.sf.hibernate.HibernateException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DialectAdapterTest {
    @Test
    public void testAdaptV2toV5() throws HibernateException {
        assertEquals(null, DialectAdapter.adaptV2toV5(null));
        assertEquals("", DialectAdapter.adaptV2toV5(""));
        assertEquals(
                "com.atlassian.hibernate.util.dialect.PostgresPlusDialectCompatibleBlob",
                DialectAdapter.adaptV2toV5("net.sf.hibernate.dialect.PostgreSQLDialect"));
        assertEquals(
                "org.hibernate.dialect.MySQLDialect",
                DialectAdapter.adaptV2toV5("net.sf.hibernate.dialect.MySQLDialect"));
    }

    @Test
    public void testAdapt() {
        net.sf.hibernate.dialect.Dialect v2 = new net.sf.hibernate.dialect.H2Dialect();
        org.hibernate.dialect.Dialect v5 = new org.hibernate.dialect.H2Dialect();
        DialectAdapter.mapDialectObjects(v2, v5);

        assertEquals(v5, DialectAdapter.adapt(v2));
        assertEquals(v2, DialectAdapter.adapt(v5));
    }
}
