package com.atlassian.hibernate.adapter.test;

import com.atlassian.hibernate.adapter.type.PersistentEnumUserType;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import static org.junit.Assert.assertEquals;

public class PersistentEnumUserTypeTest {
    @Test
    @SuppressWarnings("deprecation")
    public void testAssembleDisassemble() throws Exception {
        PersistentEnumUserType type = new PersistentEnumUserType(Hibernate2Enum.class);
        Serializable disassembled = type.disassemble(Hibernate2Enum.VALUE2);

        // simulate an external cache like hazelcast
        disassembled = serializeDeserialize(disassembled);

        Hibernate2Enum assembled = (Hibernate2Enum) type.assemble(disassembled, null);
        assertEquals("Equality must be maintained", true, assembled == Hibernate2Enum.VALUE2);
    }

    private static Serializable serializeDeserialize(final Serializable value) throws Exception {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        ObjectOutputStream outputStream = new ObjectOutputStream(bytes);
        outputStream.writeObject(value);
        ObjectInputStream inputStream = new ObjectInputStream(new ByteArrayInputStream(bytes.toByteArray()));
        return (Serializable) inputStream.readObject();
    }

    @SuppressWarnings("deprecation")
    private static class Hibernate2Enum implements Serializable, net.sf.hibernate.PersistentEnum {
        private final int id;

        public static final Hibernate2Enum VALUE1 = new Hibernate2Enum(1);
        public static final Hibernate2Enum VALUE2 = new Hibernate2Enum(2);

        public Hibernate2Enum(int id) {
            this.id = id;
        }

        @Override
        public int toInt() {
            return id;
        }

        public static Hibernate2Enum fromInt(int id) {
            switch (id) {
                case 1:
                    return VALUE1;
                case 2:
                    return VALUE2;
                default:
                    throw new RuntimeException("Unable to determine TestEnum for id: " + id);
            }
        }
    }
}
