package com.atlassian.hibernate.adapter.mapping;

import javassist.util.proxy.MethodHandler;
import org.hibernate.boot.Metadata;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PersistentClassChangeTrackerTest {

    private static net.sf.hibernate.mapping.Property createCollectionPropertyV2(
            final net.sf.hibernate.mapping.PersistentClass cls,
            final boolean lazy) {

        final net.sf.hibernate.mapping.Property property = new net.sf.hibernate.mapping.Property();
        property.setName("property");
        final net.sf.hibernate.mapping.Collection collection = new net.sf.hibernate.mapping.Bag(cls);
        collection.setLazy(lazy);
        property.setValue(collection);
        return property;
    }

    private static net.sf.hibernate.mapping.Property createCollectionPropertyV2(
            final net.sf.hibernate.mapping.PersistentClass cls,
            final int outerJoinFetchSetting) {

        final net.sf.hibernate.mapping.Property property = new net.sf.hibernate.mapping.Property();
        property.setName("property");
        final net.sf.hibernate.mapping.Collection collection = new net.sf.hibernate.mapping.Bag(cls);
        collection.setOuterJoinFetchSetting(outerJoinFetchSetting);
        property.setValue(collection);
        return property;
    }

    private static org.hibernate.mapping.Property createCollectionPropertyV5(
            final org.hibernate.mapping.PersistentClass cls,
            final boolean lazy) {

        final org.hibernate.mapping.Property property = new org.hibernate.mapping.Property();
        property.setName("property");
        final org.hibernate.mapping.Collection collection = new org.hibernate.mapping.Bag(null, cls);
        collection.setLazy(lazy);
        property.setValue(collection);
        return property;
    }

    private static org.hibernate.mapping.Property createCollectionPropertyV5(
            final org.hibernate.mapping.PersistentClass cls,
            final org.hibernate.FetchMode fetchMode) {

        final org.hibernate.mapping.Property property = new org.hibernate.mapping.Property();
        property.setName("property");
        final org.hibernate.mapping.Collection collection = new org.hibernate.mapping.Bag(null, cls);
        collection.setFetchMode(fetchMode);
        property.setValue(collection);
        return property;
    }

    @Test
    public void testApplyChanges_ProxyInterface() throws Exception {
        net.sf.hibernate.mapping.PersistentClass cls = createPersistentClassV2();
        cls.setProxyInterface(TestEntityProxyV2.class);
        final org.hibernate.mapping.PersistentClass v5 = createPersistentClassV5();
        v5.setProxyInterfaceName(TestEntityProxyV5.class.getName());

        final PersistentClassChangeTracker tracker = new PersistentClassChangeTracker();
        cls = tracker.intercept(cls, () -> true);
        cls.setProxyInterface(TestEntity.class);
        tracker.applyChanges(createMetadata(v5));
        assertEquals("v5 proxy set to entity class", TestEntity.class.getName(), v5.getProxyInterfaceName());
    }

    /**
     * Verify that the proxyInterface property isn't modified on the v5 PersistentClass
     * if it hasn't been modified on the v2 PersistentClass. This is important, as it's not
     * desirable for the hibernate generated v2 proxy interface to be set on the v5 PersistentClass.
     */
    @Test
    public void testApplyChanges_ProxyInterface_NoChanges() throws Exception {
        net.sf.hibernate.mapping.PersistentClass cls = createPersistentClassV2();
        cls.setProxyInterface(TestEntityProxyV2.class);
        final org.hibernate.mapping.PersistentClass v5 = createPersistentClassV5();
        v5.setProxyInterfaceName(TestEntityProxyV5.class.getName());

        final PersistentClassChangeTracker tracker = new PersistentClassChangeTracker();
        cls = tracker.intercept(cls, () -> true);
        tracker.applyChanges(createMetadata(v5));
        assertEquals("v5 proxy unchanged", TestEntityProxyV5.class.getName(), v5.getProxyInterfaceName());
    }

    @Test
    public void testApplyChanges_ValueLazy() throws Exception {
        net.sf.hibernate.mapping.PersistentClass cls = createPersistentClassV2();
        cls.addProperty(createCollectionPropertyV2(cls, false));
        final org.hibernate.mapping.PersistentClass v5 = createPersistentClassV5();
        v5.addProperty(createCollectionPropertyV5(v5, false));

        final PersistentClassChangeTracker tracker = new PersistentClassChangeTracker();
        cls = tracker.intercept(cls, () -> true);
        ((net.sf.hibernate.mapping.Collection) cls.getProperty("property").getValue()).setLazy(true);
        tracker.applyChanges(createMetadata(v5));
        assertEquals(
                "Property set to lazy",
                true,
                ((org.hibernate.mapping.Collection) v5.getProperty("property").getValue()).isLazy());
    }

    @Test
    @SuppressWarnings("deprecation")
    public void testApplyChanges_PropertyFetchMode() throws Exception {
        net.sf.hibernate.mapping.PersistentClass cls = createPersistentClassV2();
        cls.addProperty(createCollectionPropertyV2(cls, net.sf.hibernate.loader.OuterJoinLoader.EAGER));
        final org.hibernate.mapping.PersistentClass v5 = createPersistentClassV5();
        v5.addProperty(createCollectionPropertyV5(v5, org.hibernate.FetchMode.EAGER));

        final PersistentClassChangeTracker tracker = new PersistentClassChangeTracker();
        cls = tracker.intercept(cls, () -> true);
        ((net.sf.hibernate.mapping.Collection) cls.getProperty("property").getValue()).setOuterJoinFetchSetting(
                net.sf.hibernate.loader.OuterJoinLoader.LAZY);
        tracker.applyChanges(createMetadata(v5));
        assertEquals(
                "Property set to FetchMode.LAZY",
                org.hibernate.FetchMode.LAZY,
                v5.getProperty("property").getValue().getFetchMode());
    }

    @Test
    public void testApplyChanges_ChangeUnexpectedClassProperty() throws Exception {
        net.sf.hibernate.mapping.PersistentClass cls = createPersistentClassV2();
        final PersistentClassChangeTracker tracker = new PersistentClassChangeTracker();
        cls = tracker.intercept(cls, () -> true);
        try {
            cls.setDiscriminatorValue("badness");
            fail("Expected exception");
        } catch (final RuntimeException ex) {
        }
    }

    @Test
    public void testApplyChanges_ChangeUnexpectedPropertyProperty() throws Exception {
        net.sf.hibernate.mapping.PersistentClass cls = createPersistentClassV2();
        cls.addProperty(createCollectionPropertyV2(cls, false));

        final PersistentClassChangeTracker tracker = new PersistentClassChangeTracker();
        cls = tracker.intercept(cls, () -> true);
        try {
            ((net.sf.hibernate.mapping.Collection) cls.getProperty("property").getValue()).setBatchSize(5);
            fail("Expected exception");
        } catch (final RuntimeException ex) {
        }
    }

    @Test
    public void testGetOrCreateProxy() throws Exception {
        testGetOrCreateProxy(new net.sf.hibernate.mapping.RootClass());
        testGetOrCreateProxy(new net.sf.hibernate.mapping.Subclass(null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.Property());
        testGetOrCreateProxy(new net.sf.hibernate.mapping.Any(null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.Array(null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.Bag(null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.Component((net.sf.hibernate.mapping.Table) null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.IdentifierBag(null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.List(null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.ManyToOne(null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.Map(null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.OneToMany(null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.OneToOne(null, null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.PrimitiveArray(null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.Set(null));
        testGetOrCreateProxy(new net.sf.hibernate.mapping.SimpleValue());
    }

    private void testGetOrCreateProxy(final Object obj) {
        final PersistentClassChangeTracker tracker = new PersistentClassChangeTracker();
        final MethodHandler handler = (objj, thisMethod, proceed, args) -> null;
        final Object proxy = tracker.getOrCreateProxy(obj, handler);
        assertEquals("Proxy created successfully", obj.getClass(), proxy.getClass().getSuperclass());

        for (final Method method : obj.getClass().getMethods()) {
            if (!Modifier.isStatic(method.getModifiers()) &&
                    Modifier.isFinal(method.getModifiers()) &&
                    method.getDeclaringClass() != Object.class &&
                    !method.getName().equals("isIndexed") &&
                    !method.getName().equals("isIdentified")) {
                fail("Method " + obj.getClass().getName() + "|" + method.getName() + " is final and can't be proxied");
            }
        }
    }

    private net.sf.hibernate.mapping.PersistentClass createPersistentClassV2() {
        final net.sf.hibernate.mapping.RootClass result = new net.sf.hibernate.mapping.RootClass();
        result.setMappedClass(Object.class);
        return result;
    }

    private org.hibernate.mapping.PersistentClass createPersistentClassV5() {
        final org.hibernate.mapping.RootClass result = new org.hibernate.mapping.RootClass(null);
        result.setEntityName(Object.class.getName());
        result.setClassName(Object.class.getName());
        return result;
    }

    private Metadata createMetadata(final org.hibernate.mapping.PersistentClass persistentClass) {
        final Metadata metadata = Mockito.mock(Metadata.class);
        Mockito.when(metadata.getEntityBinding(persistentClass.getEntityName())).thenReturn(persistentClass);
        return metadata;
    }

    public class TestEntity { }
    public class TestEntityProxyV2 { }
    public class TestEntityProxyV5 { }
}
