package com.atlassian.hibernate.adapter.reflection;

import net.sf.hibernate.Interceptor;
import net.sf.hibernate.cfg.Configuration;
import net.sf.hibernate.cfg.Settings;
import net.sf.hibernate.impl.SessionFactoryImpl;
import net.sf.hibernate.impl.SessionImpl;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.sql.Connection;

import static org.junit.Assert.assertEquals;

public class SessionImplV2ReflectionTest {
    @Test
    public void testSetTimestamp() throws Exception {
        final SessionFactoryImpl factory = new SessionFactoryImpl(new Configuration(), new Settings());
        final Constructor constructor = SessionImpl.class.getDeclaredConstructor(
                Connection.class, SessionFactoryImpl.class, boolean.class, long.class, Interceptor.class);
        constructor.setAccessible(true);

        final SessionImpl session = (SessionImpl) constructor.newInstance(null, factory, false, 0, null);
        SessionImplV2Reflection.setTimestamp(session, 10L);
        assertEquals("Private field set correctly", 10L, session.getTimestamp());
    }
}
