package com.atlassian.hibernate.adapter.bridge.session;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.HibernateBridgeMode;
import com.atlassian.hibernate.sample.model.AuctionItem;
import com.atlassian.hibernate.test.HibernateAdapterTestBase;
import com.atlassian.hibernate.util.TransactionHelper;
import org.h2.jdbc.JdbcConnection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(Parameterized.class)
public class TransactionBridgeTest extends HibernateAdapterTestBase {

    protected long auctionItemId;
    private final int sessionVersionForBegin;
    private final int sessionVersionForLoad;
    private final int sessionVersionForFlush;
    private List<Object> sessionsToClose;

    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][] {
                { HibernateBridgeMode.V2_WITH_SESSION_BRIDGE, 2, 2, 2 },
                { HibernateBridgeMode.V2_WITH_SESSION_BRIDGE, 2, 2, 5 },
                { HibernateBridgeMode.V2_WITH_SESSION_BRIDGE, 2, 5, 2 },
                { HibernateBridgeMode.V2_WITH_SESSION_BRIDGE, 2, 5, 5 },
                { HibernateBridgeMode.V2_WITH_SESSION_BRIDGE, 5, 2, 2 },
                { HibernateBridgeMode.V2_WITH_SESSION_BRIDGE, 5, 2, 5 },
                { HibernateBridgeMode.V2_WITH_SESSION_BRIDGE, 5, 5, 2 },
                { HibernateBridgeMode.V2_WITH_SESSION_BRIDGE, 5, 5, 5 },
                { HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE, 2, 2, 2 },
                { HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE, 2, 2, 5 },
                { HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE, 2, 5, 2 },
                { HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE, 2, 5, 5 },
                { HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE, 5, 2, 2 },
                { HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE, 5, 2, 5 },
                { HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE, 5, 5, 2 },
                { HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE, 5, 5, 5 }
        });
    }

    public TransactionBridgeTest(
            final HibernateBridgeMode bridgeModeToUse,
            final int sessionVersionForBegin,
            final int sessionVersionForLoad,
            final int sessionVersionForFlush) {

        bridgeMode = bridgeModeToUse;
        this.sessionVersionForBegin = sessionVersionForBegin;
        this.sessionVersionForLoad = sessionVersionForLoad;
        this.sessionVersionForFlush = sessionVersionForFlush;
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        auctionItemId = createTestAuctionItem();
        sessionsToClose = new ArrayList<>();
    }

    @Override
    public void tearDown() throws Exception {
        for (final Object session : sessionsToClose) {
            close(session);
        }
        resetAuctionItemCondition();
    }

    private <T> T closeOnTearDown(final T session) {
        sessionsToClose.add(session);
        return session;
    }

    @Test
    public void testBeginCommitTransaction_FromV2SessionFactory() throws Exception {
        testBeginCommitTransaction(closeOnTearDown(sessionFactoryV2.openSession()));
    }

    @Test
    public void testBeginCommitTransaction_FromV5SessionFactory() throws Exception {
        testBeginCommitTransaction(closeOnTearDown(sessionFactoryV5.openSession()));
    }

    private void testBeginCommitTransaction(final Object initialSession) throws Exception {
        final Object transaction = beginTransaction(initialSession, sessionVersionForBegin);
        try {
            // update AuctionItem
            final AuctionItem loaded =
                    get(initialSession, sessionVersionForLoad, AuctionItem.class, auctionItemId);
            loaded.setCondition(3);
            flush(initialSession, sessionVersionForFlush); // required to start h2 db transaction..

            // must be in a database transaction
            assertTrue("In database transaction", isInDbTransaction(getConnection(initialSession)));

            // load from the same db transaction / connection, but using other Session version
            final int otherSessionVersion = sessionVersionForLoad == 2 ? 5 : 2;
            final AuctionItem loadedFromOtherVersion =
                    get(initialSession, otherSessionVersion, AuctionItem.class, auctionItemId);
            assertEquals(
                    "Value read from other version of session, in same transaction",
                    3, loadedFromOtherVersion.getCondition());
        } finally {
            commit(transaction);
        }

        // check db updated
        assertEquals("Value updated in db", 3, loadConditionFromDb(auctionItemId));
    }

    @Test
    public void testBeginCommitNestedTransaction() throws Exception {
        final Object initialSession = closeOnTearDown(sessionFactoryV2.openSession());
        final Object transaction = beginTransaction(initialSession, sessionVersionForBegin);
        try {
            final Object nestedTransaction = beginTransaction(initialSession, sessionVersionForBegin);
            try {
                // update AuctionItem
                final AuctionItem loaded = get(initialSession, sessionVersionForLoad, AuctionItem.class, auctionItemId);
                loaded.setCondition(3);
                flush(initialSession, sessionVersionForFlush);

                // must still be in db transaction
                assertEquals("In database transaction", true, isInDbTransaction(getConnection(initialSession)));
            } finally {
                commit(nestedTransaction);
            }

            // db transaction committed - v2 commits the db transaction on nested commit
            assertEquals("Database transaction committed", false, isInDbTransaction(getConnection(initialSession)));

            // check db updated
            assertEquals("Value updated in db", 3, loadConditionFromDb(auctionItemId));

            // re-read the AuctionItem
            final AuctionItem loaded = get(initialSession, sessionVersionForLoad, AuctionItem.class, auctionItemId);
            loaded.setCondition(4);
            flush(initialSession, sessionVersionForFlush);

            // nested transaction commit should restart the db transaction, as per v2 behaviour
            assertEquals("In database transaction", true, isInDbTransaction(getConnection(initialSession)));
        } finally {
            commit(transaction);
        }
        assertEquals("Database transaction committed", false, isInDbTransaction(getConnection(initialSession)));
        assertEquals("Value updated in db", 4, loadConditionFromDb(auctionItemId));
    }

    @Test
    public void testNestedBeginOnV5TransactionInterfaceFails() throws Exception {
        final net.sf.hibernate.Session sessionV2 = closeOnTearDown(sessionFactoryV2.openSession());
        final org.hibernate.Session sessionV5 = HibernateBridge.get(sessionFactoryV2).getV5Session(sessionV2);

        sessionV5.beginTransaction();
        try {
            sessionV5.beginTransaction();
            fail("Expected HibernateException when attempting to begin nesting transaction");
        } catch (IllegalStateException ex) {
        }
    }

    @Test
    public void testSaveChangesInV2andV5SessionsInSameTransaction() throws Exception {
        final net.sf.hibernate.Session sessionV2 = sessionFactoryV2.openSession();
        final org.hibernate.Session sessionV5 = HibernateBridge.get(sessionFactoryV5).getV5Session(sessionV2);

        final net.sf.hibernate.Transaction transaction = sessionV2.beginTransaction();
        try {
            // make change in v2 session
            final AuctionItem item2 = (AuctionItem) sessionV2.get(AuctionItem.class, auctionItemId);
            item2.getBids().get(0).setAmount(2);
            sessionV5.update(item2.getBids().get(0));

            // make change in v5 session
            final AuctionItem item5 = (AuctionItem) sessionV5.get(AuctionItem.class, auctionItemId);
            item5.getBids().get(1).setAmount(5);
            sessionV5.update(item5.getBids().get(1));
        } finally {
            // commit both v2 and v5 transactions together
            transaction.commit();
        }

        // verify both changes went through
        doInV5Transaction(session -> {
            final AuctionItem loaded = sessionV5.get(AuctionItem.class, auctionItemId);
            assertEquals(2, (int) loaded.getBids().get(0).getAmount());
            assertEquals(5, (int) loaded.getBids().get(1).getAmount());
        });
    }

    private static Connection getConnection(final Object session) throws Exception {
        if (session instanceof net.sf.hibernate.Session) {
            return ((net.sf.hibernate.Session) session).connection();
        }
        if (session instanceof org.hibernate.Session) {
            return ((org.hibernate.engine.spi.SessionImplementor) session).connection();
        }
        throw new IllegalArgumentException();
    }

    private Object beginTransaction(final Object initialSession, final int sessionVersion) throws Exception {
        final Object session = castToSessionVersion(initialSession, sessionVersion);
        if (session instanceof net.sf.hibernate.Session) {
            return ((net.sf.hibernate.Session) session).beginTransaction();
        }
        if (session instanceof org.hibernate.Session) {
            // emulate v2 behaviour, this is what we'll refactor v2 beginTransaction calls to
            return TransactionHelper.beginTransactionCommitNested((org.hibernate.Session) session);
        }
        throw new IllegalArgumentException(session.getClass().getName());
    }

    private void commit(final Object transaction) throws Exception {
        if (transaction instanceof net.sf.hibernate.Transaction) {
            ((net.sf.hibernate.Transaction) transaction).commit();
            return;
        }
        if (transaction instanceof org.hibernate.Transaction) {
            ((org.hibernate.Transaction) transaction).commit();
            return;
        }
        throw new IllegalArgumentException(transaction.getClass().getName());
    }

    private void flush(final Object initialSession, final int sessionVersion) throws Exception {
        final Object session = castToSessionVersion(initialSession, sessionVersion);
        if (session instanceof net.sf.hibernate.Session) {
            ((net.sf.hibernate.Session) session).flush();
            return;
        }
        if (session instanceof org.hibernate.Session) {
            ((org.hibernate.Session) session).flush();
            return;
        }
        throw new IllegalArgumentException();
    }

    private void close(final Object session) throws Exception {
        if (session instanceof net.sf.hibernate.Session) {
            ((net.sf.hibernate.Session) session).close();
            return;
        }
        if (session instanceof org.hibernate.Session) {
            ((org.hibernate.Session) session).close();
            return;
        }
        throw new IllegalArgumentException();
    }

    @SuppressWarnings("unchecked")
    private <T> T get(
            final Object initialSession,
            final int sessionVersion,
            final Class<T> clazz,
            final Serializable id) throws Exception {

        final Object session = castToSessionVersion(initialSession, sessionVersion);
        if (session instanceof net.sf.hibernate.Session) {
            return (T) ((net.sf.hibernate.Session) session).get(clazz, id);
        }
        if (session instanceof org.hibernate.Session) {
            return ((org.hibernate.Session) session).get(clazz, id);
        }
        throw new IllegalArgumentException();
    }

    private static Object castToSessionVersion(final Object initialSession, final int sessionVersion) {
        if (initialSession instanceof net.sf.hibernate.Session) {
            final net.sf.hibernate.Session sessionV2 = (net.sf.hibernate.Session) initialSession;
            final HibernateBridge hibernateBridge = HibernateBridge.get(sessionV2.getSessionFactory());
            if (sessionVersion == 2) {
                return hibernateBridge.getV2Session(sessionV2);
            } else {
                return hibernateBridge.getV5Session(sessionV2);
            }
        }
        if (initialSession instanceof org.hibernate.Session) {
            final org.hibernate.Session sessionV5 = (org.hibernate.Session) initialSession;
            final HibernateBridge hibernateBridge = HibernateBridge.get(sessionV5.getSessionFactory());
            if (sessionVersion == 2) {
                return hibernateBridge.getV2Session(sessionV5);
            } else {
                return sessionV5;
            }
        }
        throw new IllegalArgumentException();
    }

    private int loadConditionFromDb(final long auctionItemId) throws Exception {
        final net.sf.hibernate.Session session = sessionFactoryV2.openSession();
        try {
            final Connection connection = session.connection();
            final Statement statement = connection.createStatement();
            statement.execute("select condition from AuctionItem where id=" + auctionItemId);
            final ResultSet resultSet = statement.getResultSet();
            resultSet.next();
            return resultSet.getInt(1);
        } finally {
            session.close();
        }
    }

    private void resetAuctionItemCondition() throws Exception {
        doInV2Transaction(session -> {
            final AuctionItem loaded = (AuctionItem) session.get(AuctionItem.class, auctionItemId);
            loaded.setCondition(2);
        });
        assertEquals("Value updated", 2, loadConditionFromDb(auctionItemId));
    }

    private static boolean isInDbTransaction(final Connection connection) {
        return ((JdbcConnection) connection).getSession().hasPendingTransaction();
    }
}
