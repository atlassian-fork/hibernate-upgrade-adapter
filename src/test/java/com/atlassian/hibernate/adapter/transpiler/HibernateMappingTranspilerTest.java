package com.atlassian.hibernate.adapter.transpiler;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

public class HibernateMappingTranspilerTest {
    @Test
    public void test() throws Exception {
        final String input = String.join("\n",
                "<?xml version='1.0'?>",
                "",
                "<!DOCTYPE hibernate-mapping PUBLIC",
                "    '-//Hibernate/Hibernate Mapping DTD 2.0//EN'",
                "    'http://svn.atlassian.com/svn/public/atlassian/vendor/hibernate-2.1.8/tags/hibernate-2.1.8-atlassian-7/src/net/sf/hibernate/hibernate-mapping-2.0.dtd'>",
                "",
                "<hibernate-mapping>",
                "    <!-- GENERIC CONTENT OBJECT -->",
                "    <class name='com.atlassian.confluence.core.ContentEntityObject' table='CONTENT'>",
                "        <cache strategy-class='com.atlassian.confluence.cache.hibernate.ConfluenceCacheStrategy'/>",
                "",
                "        <id name='id' column='CONTENTID' type='long' unsaved-value='0'>",
                "            <generator class='com.atlassian.hibernate.extras.ResettableTableHiLoGenerator'/>",
                "        </id>",
                "",
                "        <discriminator type='string'>",
                "            <!-- CONFDEV-33070: Re-define CONTENTTYPE with an index since Postgres 8.2 has no longer been used. -->",
                "            <column name='CONTENTTYPE' index='c_contenttype_idx' not-null='true' length='255' />",
                "        </discriminator>",
                "",
                "        <version name='hibernateVersion' type='int'>",
                "            <column name='HIBERNATEVERSION' not-null='true' default='0'/>",
                "        </version>",
                "",
                "        <property name='title' type='string' update='true' insert='true' column='TITLE' length='255' index='c_title_idx'/>",
                "",
                "        <bag name='bodyContents' lazy='true' inverse='true' cascade='all'  batch-size='10' optimistic-lock='true'>",
                "            <key column='CONTENTID'/>",
                "            <one-to-many class='com.atlassian.confluence.core.BodyContent'/>",
                "        </bag>",
                "",
                "        <bag name='outgoingLinks' lazy='true' inverse='true' cascade='all'>",
                "            <cache strategy-class='com.atlassian.confluence.cache.hibernate.ConfluenceCacheStrategy'/>",
                "            <key column='CONTENTID'/>",
                "            <one-to-many class='com.atlassian.confluence.links.OutgoingLink'/>",
                "        </bag>",
                "    </class>",
                "</hibernate-mapping>",
                "").replace("'", "\"");
        final InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        final HibernateMappingTranspilerResult mapping = new HibernateMappingTranspiler().transpile(stream);
        final InputStream outputStream = mapping.getTranspiledMappingFile();

        final String actual = IOUtils.toString(outputStream, StandardCharsets.UTF_8);
        final String expected = String.join("\n",
                "<?xml version='1.0' encoding='UTF-8'?>",
                "<!DOCTYPE hibernate-mapping PUBLIC '-//Hibernate/Hibernate Mapping DTD 3.0//EN' 'http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd'>",
                "<hibernate-mapping default-lazy='false'>",
                "    <!-- GENERIC CONTENT OBJECT -->",
                "    <class name='com.atlassian.confluence.core.ContentEntityObject' table='CONTENT'>",
                "        <cache usage='read-write'/>",
                "",
                "        <id column='CONTENTID' name='id' type='long' unsaved-value='0'>",
                "            <generator class='com.atlassian.hibernate.extras.ResettableTableHiLoGenerator'/>",
                "        </id>",
                "",
                "        <discriminator type='string'>",
                "            <!-- CONFDEV-33070: Re-define CONTENTTYPE with an index since Postgres 8.2 has no longer been used. -->",
                "            <column index='c_contenttype_idx' length='255' name='CONTENTTYPE' not-null='true'/>",
                "        </discriminator>",
                "",
                "        <version name='hibernateVersion' type='int'>",
                "            <column default='0' name='HIBERNATEVERSION' not-null='true'/>",
                "        </version>",
                "",
                "        <property column='TITLE' index='c_title_idx' insert='true' length='255' name='title' type='string' update='true'/>",
                "",
                "        <bag batch-size='10' cascade='all' inverse='true' lazy='true' name='bodyContents' optimistic-lock='true'>",
                "            <key column='CONTENTID'/>",
                "            <one-to-many class='com.atlassian.confluence.core.BodyContent'/>",
                "        </bag>",
                "",
                "        <bag cascade='all' inverse='true' lazy='true' name='outgoingLinks' optimistic-lock='false'>",
                "            <cache usage='read-write'/>",
                "            <key column='CONTENTID'/>",
                "            <one-to-many class='com.atlassian.confluence.links.OutgoingLink'/>",
                "        </bag>",
                "    </class>",
                "</hibernate-mapping>"
        ).replace("'", "\"");
        assertEquals(expected, actual);

        assertEquals(
            "com.atlassian.confluence.cache.hibernate.ConfluenceCacheStrategy",
            mapping.getCacheStrategyClassNames().get("com.atlassian.confluence.core.ContentEntityObject"));
        assertEquals(
            "com.atlassian.confluence.cache.hibernate.ConfluenceCacheStrategy",
            mapping.getCacheStrategyClassNames().get("com.atlassian.confluence.core.ContentEntityObject.outgoingLinks"));
    }
}
