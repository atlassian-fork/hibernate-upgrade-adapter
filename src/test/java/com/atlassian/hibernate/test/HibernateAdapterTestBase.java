package com.atlassian.hibernate.test;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.HibernateBridgeMode;
import com.atlassian.hibernate.sample.model.AuctionItem;
import com.atlassian.hibernate.sample.model.Bid;
import com.atlassian.hibernate.sample.model.BuyNow;
import com.atlassian.hibernate.sample.model.Name;
import com.atlassian.hibernate.sample.model.SessionFactoryFactory;
import com.atlassian.hibernate.sample.model.User;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Session;
import net.sf.hibernate.Transaction;
import org.hibernate.id.GUIDGenerator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class HibernateAdapterTestBase {
    protected static net.sf.hibernate.SessionFactory sessionFactoryV2;
    protected static org.hibernate.SessionFactory sessionFactoryV5;
    protected static HibernateBridgeMode bridgeMode;

    @BeforeClass
    public static void setUpClass() throws Exception {
        bridgeMode = HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE;
        final net.sf.hibernate.SessionFactory sessionFactory = SessionFactoryFactory.create(() -> bridgeMode);
        sessionFactoryV2 = HibernateBridge.get(sessionFactory).getV2orV5SessionFactory();
        sessionFactoryV5 = HibernateBridge.get(sessionFactory).getV5SessionFactory();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        sessionFactoryV2.close();
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    protected void doInV2Transaction(final SessionV2Consumer consumer) throws HibernateException {
        final Session session = sessionFactoryV2.openSession();
        final Transaction transaction = session.beginTransaction();
        try {
            consumer.accept(session);
        } finally {
            transaction.commit();
            session.close();
        }
    }

    protected void doInV5Transaction(final SessionV5Consumer consumer) throws HibernateException {
        final org.hibernate.Session session = sessionFactoryV5.openSession();
        final org.hibernate.Transaction transaction = session.beginTransaction();
        try {
            consumer.accept(session);
        } finally {
            transaction.commit();
            session.close();
        }
    }

    protected interface SessionV2Consumer {
        void accept(net.sf.hibernate.Session session) throws HibernateException;
    }

    protected interface SessionV5Consumer {
        void accept(org.hibernate.Session session) throws HibernateException;
    }

    @SuppressWarnings("unchecked")
    protected User createTestUser(final String userName, final Name name, final String email) {
        final User user = new User();
        user.setUserName(userName);
        user.setName(name);
        user.setEmail(email);

        user.setAuctions(new ArrayList());
        user.setBids(new ArrayList());
        return user;
    }

    protected long createTestAuctionItem() throws HibernateException {
        final AtomicLong id = new AtomicLong();
        doInV2Transaction(session -> {
            AuctionItem auctionItem = createTestAuctionItem(session);
            id.set(auctionItem.getId());
        });
        return id.get();
    }

    @SuppressWarnings("unchecked")
    protected AuctionItem createTestAuctionItem(final Session session) throws HibernateException {
        final User seller = createTestUser("oldirty", new Name("ol' dirty", null, "bastard"), "oldirty@hibernate.org");
        final User bidder1 = createTestUser("1E1", new Name("oney", '1', "one"), "oney@hibernate.org");
        final User bidder2 = createTestUser("izi", new Name("iz", null, "inizi"), "izi@hibernate.org");
        session.save(seller);
        session.save(bidder1);
        session.save(bidder2);

        final AuctionItem item = new AuctionItem("auction item" + UUID.randomUUID(), new Date(), seller, 2);

        final Bid bid = new Bid(item, 1, new Date(), bidder1);
        final Bid bid2 = new Bid(item, 1.5f, new Date(), bidder2);
        bidder1.getBids().add(bid);
        bidder2.getBids().add(bid2);
        item.getBids().add(bid);
        item.getBids().add(bid2);

        seller.getAuctions().add(item);

        final BuyNow buyNow = new BuyNow(item, 1, new Date(), bidder2);
        bidder2.getBids().add(buyNow);
        item.getBids().add(buyNow);

        session.save(item);
        return item;
    }
}
