package com.atlassian.hibernate.sample.model;

import com.atlassian.hibernate.adapter.ConfigurationV2Bridge;
import com.atlassian.hibernate.adapter.HibernateBridgeMode;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.cfg.Configuration;
import net.sf.hibernate.cfg.Environment;

import java.util.function.Supplier;

/**
 * Provides a SessionFactory with an in-memory h2 database for testing.
 */
public class SessionFactoryFactory {

    /**
     * Create the Configuration object.
     */
    public static ConfigurationV2Bridge createConfig() throws MappingException {
        ConfigurationV2Bridge config = new ConfigurationV2Bridge();

        config.setProperty(Environment.HBM2DDL_AUTO, "create");
        config.setProperty("hibernate.dialect", "net.sf.hibernate.dialect.H2Dialect");
        config.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
        config.setProperty("hibernate.connection.url", "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        config.setProperty("hibernate.connection.username", "sa");
        config.setProperty("hibernate.connection.password", "");

        config.setProperty("hibernate.cache.provider_class", "com.atlassian.hibernate.test.BridgableHashtableCacheProvider");
        config.setProperty("hibernate.atlassian.use.v2.postgres.schema.naming", "true");
        config.setProperty("hibernate.atlassian.disable.v2.hbm.validation", "true");
        config.setProperty("hibernate.cache.use_query_cache", "true");
        config.setProperty("hibernate.cache.use_second_level_cache", "true");

        config.addClass(AuctionItem.class);
        config.addClass(Bid.class);
        config.addClass(User.class);
        return config;
    }

    /**
     * Create the SessionFactory object.
     * @param bridgeMode The HibernateBridgeMode, which can be changed dynamically for testing
     */
    public static net.sf.hibernate.SessionFactory create(final Supplier<HibernateBridgeMode> bridgeMode)
            throws HibernateException {

        final Configuration config = createConfig();
        ((ConfigurationV2Bridge) config).setBridgeModeSupplier(bridgeMode);
        return config.buildSessionFactory();
    }
}
