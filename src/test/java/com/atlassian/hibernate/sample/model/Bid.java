package com.atlassian.hibernate.sample.model;

import java.util.Date;

public class Bid extends Persistent {
	private AuctionItem item;
	private float amount;
	private Date datetime;
	private User bidder;

	public Bid() { }

	public Bid(final AuctionItem item, final float amount, final Date datetime, final User bidder) {
		this.item = item;
		this.amount = amount;
		this.datetime = datetime;
		this.bidder = bidder;
	}

	public AuctionItem getItem() {
		return item;
	}

	public void setItem(final AuctionItem item) {
		this.item = item;
	}

	public float getAmount() {
		return amount;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setAmount(float f) {
		amount = f;
	}

	public void setDatetime(final Date date) {
		datetime = date;
	}

	public User getBidder() {
		return bidder;
	}

	public void setBidder(final User user) {
		bidder = user;
	}

	public String toString() {
		return bidder.getUserName() + " $" + amount;
	}
	
	public boolean isBuyNow() {
		return false;
	}
}
