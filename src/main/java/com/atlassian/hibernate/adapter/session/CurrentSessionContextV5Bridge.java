package com.atlassian.hibernate.adapter.session;

import com.atlassian.hibernate.adapter.HibernateBridge;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.context.spi.CurrentSessionContext;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.springframework.orm.hibernate.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * <p>
 * Implementation of Hibernate's CurrentSessionContext interface
 * that returns the v5 Session interface from the v2 Session interface
 * that's currently managed on the current thread by Spring.
 * </p>
 * <p>
 * This CurrentSessionContext implementation can be specified in custom
 * SessionFactory setup through the "hibernate.current_session_context_class"
 * property, with the fully qualified name of this class as the value.
 * </p>
 */
public class CurrentSessionContextV5Bridge implements CurrentSessionContext {

    private final SessionFactoryImplementor sessionFactory;
    private HibernateBridge hibernateBridge;

    public CurrentSessionContextV5Bridge(final SessionFactoryImplementor sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Session currentSession() throws HibernateException {
        return getHibernateBridge().getV5Session(getV2Session());
    }

    private net.sf.hibernate.Session getV2Session() {
        final Object obj = TransactionSynchronizationManager.getResource(getHibernateBridge().getSpringV2SessionFactory());
        if (obj instanceof net.sf.hibernate.Session) {
            return (net.sf.hibernate.Session) obj;
        } else if (obj instanceof SessionHolder) {
            final SessionHolder sessionHolder = (SessionHolder) obj;
            return sessionHolder.getSession();
        }
        throw new HibernateException("Failed to find " + net.sf.hibernate.Session.class.getName() + " from the current thread");
    }

    private HibernateBridge getHibernateBridge() {
        if (hibernateBridge == null) {
            hibernateBridge = HibernateBridge.get(sessionFactory);
        }
        return hibernateBridge;
    }
}
