package com.atlassian.hibernate.adapter.transpiler;

import java.util.Arrays;

/**
 * A String keyed map with efficient lookup by key from the substring of another string.
 * <p>
 * Suitable for maps with a small number of items, and a fair amount of uniqueness
 * for the first character of each key.
 */
class FastStringKeyMap<T> {

    private final boolean[] firstChars = new boolean[255];
    private Entry[] items = new Entry[0];

    private static boolean matches(final char[] key, final CharSequence keyString, final int start) {
        if (key.length > keyString.length() - start) {
            return false;
        }

        for (int i = 0; i < key.length; i++) {
            if (key[i] != keyString.charAt(i + start)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns the value to which the specified key is mapped,
     * or {@code null} if this map contains no mapping for the key.
     */
    @SuppressWarnings("unchecked")
    public T get(final String key) {
        final Entry entry = getEntry(key.toCharArray());
        return entry != null ? (T) entry.value : null;
    }

    /**
     * Returns the value to which a substring starting from 'start' is mapped,
     * or {@code null} if this map contains no mapping for such a key.
     */
    @SuppressWarnings("unchecked")
    public T get(final CharSequence keyString, final int start) {
        final char firstChar = keyString.charAt(start);
        if (firstChar <= 255 && !firstChars[firstChar]) {
            return null;
        }

        for (final Entry entry : items) {
            if (matches(entry.key, keyString, start)) {
                return (T) entry.value;
            }
        }
        return null;
    }

    /**
     * Associates the specified value with the specified key in this map.
     * If the map previously contained a mapping for the key, the old
     * value is replaced.
     */
    public void put(final String key, final T value) {
        final Entry entry = getEntry(key.toCharArray());
        if (entry != null) {
            entry.value = value;
            return;
        }

        final Entry[] array = Arrays.copyOf(items, items.length + 1);
        array[array.length - 1] = new Entry(key, value);
        items = array;

        firstChars[key.charAt(0)] = true;
    }

    private Entry getEntry(final char[] key) {
        for (final Entry entry : items) {
            if (Arrays.equals(entry.key, key)) {
                return entry;
            }
        }
        return null;
    }

    @SuppressWarnings("checkstyle:visibilitymodifier")
    private static class Entry {
        public char[] key;
        public Object value;

        public Entry(final String key, final Object value) {
            this.key = key.toCharArray();
            this.value = value;
        }
    }
}
