package com.atlassian.hibernate.adapter.proxy;

import javassist.util.proxy.MethodFilter;
import javassist.util.proxy.Proxy;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.internal.util.ReflectHelper;
import org.hibernate.internal.util.collections.ArrayHelper;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.pojo.javassist.JavassistProxyFactory;
import org.hibernate.type.CompositeType;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Set;

/**
 * An implementation of JavassistProxyFactory that produces proxies that also implement
 * the hibernate v2 net.sf.hibernate.proxy.HibernateProxy interface.
 *
 * @see BytecodeProviderImpl_ImplementV2Proxy
 */
@SuppressWarnings("checkstyle:typename")
class JavassistProxyFactory_ImplementV2Proxy extends JavassistProxyFactory {
    // skip finalize methods
    private static final MethodFilter FINALIZE_FILTER = m ->
            !(m.getParameterTypes().length == 0 && m.getName().equals("finalize"));

    private String entityName;
    private Class persistentClass;
    private Class[] interfaces;
    private Method getIdentifierMethod;
    private Method setIdentifierMethod;
    private CompositeType componentIdType;
    private boolean overridesEquals;

    private Class proxyClass;

    private static Class[] getInterfacesIncludingV2HibernateProxy(final Class[] interfaces) {
        final Class[] result = new Class[interfaces.length + 2];
        System.arraycopy(interfaces, 0, result, 0, interfaces.length);
        result[interfaces.length] = net.sf.hibernate.proxy.HibernateProxy.class;
        result[interfaces.length + 1] = net.sf.cglib.proxy.Factory.class;
        return result;
    }

    @Override
    public void postInstantiate(
            final String entityName,
            final Class persistentClass,
            final Set<Class> interfaces,
            final Method getIdentifierMethod,
            final Method setIdentifierMethod,
            final CompositeType componentIdType) throws HibernateException {
        super.postInstantiate(entityName, persistentClass, interfaces, getIdentifierMethod, setIdentifierMethod, componentIdType);

        this.entityName = entityName;
        this.persistentClass = persistentClass;
        this.interfaces = interfaces == null ? ArrayHelper.EMPTY_CLASS_ARRAY : interfaces.toArray(new Class[interfaces.size()]);
        this.getIdentifierMethod = getIdentifierMethod;
        this.setIdentifierMethod = setIdentifierMethod;
        this.componentIdType = componentIdType;
        this.overridesEquals = ReflectHelper.overridesEquals(persistentClass);

        this.proxyClass = buildJavassistProxyFactory().createClass();
    }

    private javassist.util.proxy.ProxyFactory buildJavassistProxyFactory() {
        final javassist.util.proxy.ProxyFactory factory = new javassist.util.proxy.ProxyFactory() {
            @Override
            protected ClassLoader getClassLoader() {
                return persistentClass.getClassLoader();
            }
        };

        factory.setSuperclass(interfaces.length == 1 ? persistentClass : null);
        factory.setInterfaces(getInterfacesIncludingV2HibernateProxy(interfaces));
        factory.setFilter(FINALIZE_FILTER);
        return factory;
    }

    @Override
    public HibernateProxy getProxy(
            final Serializable id,
            final SharedSessionContractImplementor session) throws HibernateException {
        final JavassistLazyInitializer_ImplementV2Proxy initializer = new JavassistLazyInitializer_ImplementV2Proxy(
                entityName,
                persistentClass,
                interfaces,
                id,
                getIdentifierMethod,
                setIdentifierMethod,
                componentIdType,
                session,
                overridesEquals);

        try {
            final HibernateProxy proxy = (HibernateProxy) proxyClass.newInstance();
            ((Proxy) proxy).setHandler(initializer);
            initializer.constructed();
            return proxy;
        } catch (final Throwable ex) {
            throw new HibernateException("Javassist Enhancement failed: " + entityName, ex);
        }
    }
}
