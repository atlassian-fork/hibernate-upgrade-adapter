package com.atlassian.hibernate.adapter.proxy;

import org.hibernate.bytecode.internal.javassist.ProxyFactoryFactoryImpl;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.proxy.ProxyFactory;

/**
 * An implementation of ProxyFactoryFactoryImpl that produces proxies that also implement
 * the hibernate v2 net.sf.hibernate.proxy.HibernateProxy interface.
 *
 * @see BytecodeProviderImpl_ImplementV2Proxy
 */
@SuppressWarnings("checkstyle:typename")
public class ProxyFactoryFactoryImpl_ImplementV2Proxy extends ProxyFactoryFactoryImpl {
    @Override
    public ProxyFactory buildProxyFactory(final SessionFactoryImplementor sessionFactory) {
        return new JavassistProxyFactory_ImplementV2Proxy();
    }
}
