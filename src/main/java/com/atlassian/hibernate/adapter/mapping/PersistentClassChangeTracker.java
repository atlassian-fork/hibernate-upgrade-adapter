package com.atlassian.hibernate.adapter.mapping;

import com.atlassian.hibernate.util.ThrowableUtil;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;
import net.sf.hibernate.loader.OuterJoinLoader;
import org.apache.commons.collections.IteratorUtils;
import org.hibernate.boot.Metadata;

import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Provides a way to track changes to a v2 PersistentClass object,
 * which may be later applied to a v5 PersistentClass object.
 * <p>
 * Only PersistentClass.proxyInterface, Collection.lazy and
 * Fetchable.outerJoinFetchSetting properties may be applied to the v5 objects.
 * <p>
 * Changes to other properties will cause an exception to be raised.
 */
public class PersistentClassChangeTracker {
    private final WeakHashMap<Object, WeakReference> proxyCache = new WeakHashMap<>();
    private final List<Consumer<Metadata>> changeQueue = new ArrayList<>();

    private static org.hibernate.FetchMode getV5FetchMode(final int outerJoinFetchSetting) {
        switch (outerJoinFetchSetting) {
            case OuterJoinLoader.AUTO:
                return org.hibernate.FetchMode.DEFAULT;
            case OuterJoinLoader.EAGER:
                return org.hibernate.FetchMode.JOIN;
            case OuterJoinLoader.LAZY:
                return org.hibernate.FetchMode.SELECT;
            default:
                throw new IllegalArgumentException("Unexpected outerJoinFetchSetting: " + outerJoinFetchSetting);
        }
    }

    private static boolean isMutationMethod(final Method method) {
        final String name = method.getName();
        return name.startsWith("add")
                || name.startsWith("set")
                || name.startsWith("create")
                || name.equals("readFrom")
                || name.equals("initializeFromCache");
    }

    /**
     * Apply changes to the v5 PersistentClass and Property objects.
     */
    public void applyChanges(final Metadata metadata) {
        for (final Consumer<Metadata> consumer : changeQueue) {
            consumer.accept(metadata);
        }
    }

    /**
     * Intercept a PersistentClass and it's Property objects.
     * An exception is thrown if any unsupported property mutations occur.
     */
    public net.sf.hibernate.mapping.PersistentClass intercept(
            final net.sf.hibernate.mapping.PersistentClass persistentClass,
            final Supplier<Boolean> throwOnUnsupportedMutation) {

        return getOrCreateProxy(persistentClass, (self, thisMethod, proceed, args) -> {
            // intercept Property objects
            if (thisMethod.getName().equals("getPropertyIterator")) {
                final Iterator i = (Iterator) thisMethod.invoke(persistentClass, args);
                return IteratorUtils.transformedIterator(
                        i,
                        property -> interceptProperty(
                                persistentClass, (net.sf.hibernate.mapping.Property) property, throwOnUnsupportedMutation));
            }
            if (thisMethod.getName().equals("getProperty")
                    || thisMethod.getName().equals("getIdentifierProperty")) {
                return interceptProperty(
                        persistentClass,
                        (net.sf.hibernate.mapping.Property) thisMethod.invoke(persistentClass, args),
                        throwOnUnsupportedMutation);
            }

            // remember setProxyInterface calls
            if (thisMethod.getName().equals("setProxyInterface")) {
                changeQueue.add(metadata ->
                        findV5PersistentClass(metadata, persistentClass)
                                .setProxyInterfaceName(((Class) args[0]).getName()));
                return thisMethod.invoke(persistentClass, args);
            }

            // throw on unsupported mutations
            if (isMutationMethod(thisMethod) && throwOnUnsupportedMutation.get()) {
                throwModificationError();
            }
            return thisMethod.invoke(persistentClass, args);
        });
    }

    private net.sf.hibernate.mapping.Property interceptProperty(
            final net.sf.hibernate.mapping.PersistentClass persistentClass,
            final net.sf.hibernate.mapping.Property property,
            final Supplier<Boolean> throwOnUnsupportedMutation) {

        return getOrCreateProxy(property, (self, thisMethod, proceed, args) -> {
            // intercept Value object
            if (thisMethod.getName().equals("getValue")) {
                return interceptValue(persistentClass, property, throwOnUnsupportedMutation);
            }

            // throw on unsupported mutations
            if (isMutationMethod(thisMethod) && throwOnUnsupportedMutation.get()) {
                throwModificationError();
            }
            return thisMethod.invoke(property, args);
        });
    }

    private net.sf.hibernate.mapping.Value interceptValue(
            final net.sf.hibernate.mapping.PersistentClass persistentClass,
            final net.sf.hibernate.mapping.Property property,
            final Supplier<Boolean> throwOnUnsupportedMutation) {

        return getOrCreateProxy(property.getValue(), (self, thisMethod, proceed, args) -> {
            // remember setLazy calls
            if (thisMethod.getName().equals("setLazy")) {
                changeQueue.add(metadata ->
                        ((org.hibernate.mapping.Collection) findV5Property(metadata, persistentClass, property).getValue())
                                .setLazy((boolean) args[0]));
                return thisMethod.invoke(property.getValue(), args);
            }

            // remember setOuterJoinFetchSetting calls
            if (thisMethod.getName().equals("setOuterJoinFetchSetting")) {
                changeQueue.add(metadata ->
                        ((org.hibernate.mapping.Fetchable) findV5Property(metadata, persistentClass, property).getValue())
                                .setFetchMode(getV5FetchMode((int) args[0])));
                return thisMethod.invoke(property.getValue(), args);
            }

            // throw on unsupported mutations
            if (isMutationMethod(thisMethod) && throwOnUnsupportedMutation.get()) {
                throwModificationError();
            }
            return thisMethod.invoke(property.getValue(), args);
        });
    }

    /**
     * Get or create a proxy for an object.
     * This method is designed to work with all instances and subclasses of
     * net.sf.hibernate.mapping PersistentClass, Property and Value objects.
     *
     * @param obj     The object to proxy
     * @param handler The method handler for all calls to the proxy
     * @return The proxy object
     */
    @SuppressWarnings("unchecked")
    <T> T getOrCreateProxy(final T obj, final MethodHandler handler) {
        WeakReference ref;
        synchronized (proxyCache) {
            ref = proxyCache.get(obj);
        }
        T proxy = ref != null ? (T) ref.get() : null;
        if (proxy != null) {
            return proxy;
        }

        final ProxyFactory factory = new ProxyFactory();
        factory.setSuperclass(obj.getClass());
        try {
            if (obj instanceof net.sf.hibernate.mapping.OneToOne) {
                proxy = (T) factory.create(
                        new Class[]{net.sf.hibernate.mapping.Table.class, net.sf.hibernate.mapping.SimpleValue.class},
                        new Object[]{null, null},
                        handler);
            } else if (obj instanceof net.sf.hibernate.mapping.Property
                    || obj instanceof net.sf.hibernate.mapping.RootClass) {
                proxy = (T) factory.create(new Class[0], new Object[0], handler);
            } else if (
                    obj instanceof net.sf.hibernate.mapping.Any
                            || obj instanceof net.sf.hibernate.mapping.ManyToOne
                            || obj instanceof net.sf.hibernate.mapping.SimpleValue) {
                proxy = (T) factory.create(new Class[]{net.sf.hibernate.mapping.Table.class}, new Object[]{null}, handler);
            } else {
                proxy = (T) factory.create(
                        new Class[]{net.sf.hibernate.mapping.PersistentClass.class},
                        new Object[]{null},
                        handler);
            }
            synchronized (proxyCache) {
                proxyCache.put(obj, new WeakReference<>(proxy));
            }
            return proxy;
        } catch (final ReflectiveOperationException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    private org.hibernate.mapping.PersistentClass findV5PersistentClass(
            final Metadata metadata, final net.sf.hibernate.mapping.PersistentClass persistentClass) {
        final org.hibernate.mapping.PersistentClass persistentClassV5 = metadata.getEntityBinding(persistentClass.getName());
        if (persistentClassV5 == null) {
            throw new RuntimeException("Failed to find v5 PersistentClass: " + persistentClass.getName());
        }
        return persistentClassV5;
    }

    private org.hibernate.mapping.Property findV5Property(
            final Metadata metadata,
            final net.sf.hibernate.mapping.PersistentClass persistentClass,
            final net.sf.hibernate.mapping.Property property) {

        final org.hibernate.mapping.PersistentClass persistentClassV5 = findV5PersistentClass(metadata, persistentClass);
        final org.hibernate.mapping.Property propertyV5 = persistentClassV5.getProperty(property.getName());
        if (propertyV5 == null) {
            throw new RuntimeException("Failed to find v5 Property: " + persistentClass.getClass().getName() + "." + property.getName());
        }
        return propertyV5;
    }

    private void throwModificationError() {
        throw new RuntimeException(
                "Modification of PersistentClass not supported for properties other than "
                + "PersistentClass.setProxyInterface, Collection.setLazy, Fetchable.setOuterJoinFetchSetting");
    }
}
