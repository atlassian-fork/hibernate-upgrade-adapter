package com.atlassian.hibernate.adapter.adapters.type.relation;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.type.ForeignKeyDirectionV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.JoinableV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.persister.Joinable;
import net.sf.hibernate.type.AssociationType;
import net.sf.hibernate.type.ForeignKeyDirection;
import org.apache.commons.lang3.NotImplementedException;

/**
 * An adapter bridging the AssociationType interface from hibernate v5 (onwards) to v2.
 */
public class AssociationTypeV2Adapter extends TypeV2Adapter implements AssociationType {
    private final org.hibernate.type.AssociationType type;

    protected AssociationTypeV2Adapter(final org.hibernate.type.AssociationType type) {
        super(type);
        this.type = type;
    }

    public static AssociationType adapt(final org.hibernate.type.AssociationType type) {
        if (type == null)
            return null;
        return new AssociationTypeV2Adapter(type);
    }

    @Override
    public ForeignKeyDirection getForeignKeyDirection() {
        return ForeignKeyDirectionV2Adapter.adapt(type.getForeignKeyDirection());
    }

    @Override
    public boolean usePrimaryKeyAsForeignKey() {
        throw new NotImplementedException("usePrimaryKeyAsForeignKey not implemented");
    }

    @Override
    public Joinable getJoinable(final SessionFactoryImplementor factory) throws MappingException {
        try {
            final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactoryV5 =
                    (org.hibernate.engine.spi.SessionFactoryImplementor) HibernateBridge.get(factory).getV5SessionFactory();
            return JoinableV2Adapter.adapt(type.getAssociatedJoinable(sessionFactoryV5), sessionFactoryV5);
        } catch (final org.hibernate.MappingException ex) {
            throw (MappingException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public String[] getReferencedColumns(final SessionFactoryImplementor factory) {
        throw new NotImplementedException("getReferencedColumns not implemented");
    }

    @Override
    @SuppressWarnings("deprecation")
    public Class getAssociatedClass(final SessionFactoryImplementor factory) throws MappingException {
        try {
            final org.hibernate.SessionFactory sessionFactory = HibernateBridge.get(factory).getV5SessionFactory();
            final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactoryImpl =
                    (org.hibernate.engine.spi.SessionFactoryImplementor) sessionFactory;

            final String entityName = type.getAssociatedEntityName(sessionFactoryImpl);
            return sessionFactoryImpl.getEntityPersister(entityName).getMappedClass();
        } catch (final org.hibernate.MappingException ex) {
            throw (MappingException) HibernateExceptionAdapter.adapt(ex);
        }
    }
}
