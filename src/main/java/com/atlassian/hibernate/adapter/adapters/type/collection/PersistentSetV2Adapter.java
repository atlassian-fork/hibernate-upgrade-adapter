package com.atlassian.hibernate.adapter.adapters.type.collection;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * An adapter bridging the PersistentSet class from hibernate v5 (onwards) to v2.
 */
@SuppressWarnings("unchecked")
class PersistentSetV2Adapter extends PersistentCollectionV2Adapter implements Set {
    private final Set set;

    PersistentSetV2Adapter(final org.hibernate.collection.internal.PersistentSet collection) {
        super(collection);
        this.set = collection;
    }

    @Override
    public int size() {
        return set.size();
    }

    @Override
    public boolean isEmpty() {
        return set.isEmpty();
    }

    @Override
    public boolean contains(final Object o) {
        return set.contains(o);
    }

    @Override
    public Iterator iterator() {
        return set.iterator();
    }

    @Override
    public Object[] toArray() {
        return set.toArray();
    }

    @Override
    public boolean add(final Object o) {
        return set.add(o);
    }

    @Override
    public boolean remove(final Object o) {
        return set.remove(o);
    }

    @Override
    public boolean addAll(final Collection c) {
        return set.addAll(c);
    }

    @Override
    public void clear() {
        set.clear();
    }

    @Override
    public boolean removeAll(final Collection c) {
        return set.removeAll(c);
    }

    @Override
    public boolean retainAll(final Collection c) {
        return set.retainAll(c);
    }

    @Override
    public boolean containsAll(final Collection c) {
        return set.containsAll(c);
    }

    @Override
    public Object[] toArray(final Object[] a) {
        return set.toArray(a);
    }
}
