package com.atlassian.hibernate.adapter.adapters.cache;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import net.sf.hibernate.cache.Cache;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.CacheDataDescription;
import org.hibernate.cache.spi.CollectionRegion;
import org.hibernate.cache.spi.EntityRegion;
import org.hibernate.cache.spi.NaturalIdRegion;
import org.hibernate.cache.spi.QueryResultsRegion;
import org.hibernate.cache.spi.RegionFactory;
import org.hibernate.cache.spi.TimestampsRegion;
import org.hibernate.cache.spi.access.AccessType;

import java.util.Map;
import java.util.Properties;

/**
 * The v5 RegionFactory to v2 CacheProvider bridge implementation.
 */
public class RegionFactoryCacheProviderBridge implements RegionFactory {

    public static final String V2_CACHE_PROVIDER_PROPERTY = "hibernate.atlassian.v2_cache_provider";
    public static final String V2_CACHE_STRATEGY_CLASS_PROPERTY = "hibernate.atlassian.v2_cache_strategy_class";
    public static final String V2_CACHE_STRATEGY_CLASS_MAP_PROPERTY = "hibernate.atlassian.v2_cache_strategy_class_map";

    private net.sf.hibernate.cache.CacheProvider cacheProvider;
    private final String cacheStrategyClass;
    private final Map<String, String> cacheStrategyClassMap;

    /**
     * This constructor is called using reflection from
     * org.hibernate.Configuration.buildSessionFactory().
     */
    @SuppressWarnings("unchecked")
    public RegionFactoryCacheProviderBridge(final Properties properties) {
        this(
                getCacheProvider(properties),
                (String) properties.get(V2_CACHE_STRATEGY_CLASS_PROPERTY),
                (Map<String, String>) properties.get(V2_CACHE_STRATEGY_CLASS_MAP_PROPERTY));
    }

    protected RegionFactoryCacheProviderBridge(
            final net.sf.hibernate.cache.CacheProvider cacheProvider,
            final String cacheStrategyClass,
            final Map<String, String> cacheStrategyClassMap) {

        this.cacheProvider = cacheProvider;
        this.cacheStrategyClass = cacheStrategyClass;
        this.cacheStrategyClassMap = cacheStrategyClassMap;
    }

    private static net.sf.hibernate.cache.CacheProvider getCacheProvider(final Properties properties) {

        // if specified by property V2_CACHE_PROVIDER_PROPERTY, use that
        final net.sf.hibernate.cache.CacheProvider cacheProvider =
                (net.sf.hibernate.cache.CacheProvider) properties.get(V2_CACHE_PROVIDER_PROPERTY);
        if (cacheProvider != null) {
            return cacheProvider;
        }

        // otherwise find it from hibernate properties
        final String providerClassName = net.sf.hibernate.util.PropertiesHelper.getString(
                net.sf.hibernate.cfg.Environment.CACHE_PROVIDER, properties, null);
        if (providerClassName == null) {
            throw new IllegalArgumentException("net.sf.hibernate.cache.CacheProvider is not configured");
        }
        try {
            return (net.sf.hibernate.cache.CacheProvider)
                    net.sf.hibernate.util.ReflectHelper.classForName(providerClassName).newInstance();
        } catch (final Exception ex) {
            throw new CacheException("could not instantiate CacheProvider [" + providerClassName + "]", ex);
        }
    }

    @Override
    @Deprecated
    @SuppressWarnings("unchecked")
    public void start(final SessionFactoryOptions settings, final Properties properties) throws CacheException {
        start(settings, (Map) properties);
    }

    @Override
    public void start(final SessionFactoryOptions settings, final Map<String, Object> configValues) throws CacheException {
        // if the CacheProvider came from hibernate 2, it will already be started
        if (configValues.containsKey(V2_CACHE_PROVIDER_PROPERTY)) {
            return;
        }

        try {
            final Properties properties = new Properties();
            properties.putAll(configValues);
            cacheProvider.start(properties);
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void stop() {
        cacheProvider.stop();
        cacheProvider = null;
    }

    @Override
    public boolean isMinimalPutsEnabledByDefault() {
        return false;
    }

    @Override
    public AccessType getDefaultAccessType() {
        return AccessType.READ_WRITE;
    }

    @Override
    public long nextTimestamp() {
        return cacheProvider.nextTimestamp();
    }

    @Override
    @Deprecated
    @SuppressWarnings("unchecked")
    public EntityRegion buildEntityRegion(
            final String regionName,
            final Properties properties,
            final CacheDataDescription metadata) throws CacheException {

        return buildEntityRegion(regionName, (Map) properties, metadata);
    }

    @Override
    public EntityRegion buildEntityRegion(
            final String regionName,
            final Map<String, Object> configValues,
            final CacheDataDescription metadata) throws CacheException {

        try {
            final Cache cache = buildCache(regionName, configValues);
            return new EntityRegionV5Adapter(regionName, cache, this::getCacheStrategyClass, metadata);
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    @Deprecated
    @SuppressWarnings("unchecked")
    public CollectionRegion buildCollectionRegion(
            final String regionName,
            final Properties properties,
            final CacheDataDescription metadata) throws CacheException {

        return buildCollectionRegion(regionName, (Map) properties, metadata);
    }

    @Override
    public CollectionRegion buildCollectionRegion(
            final String regionName,
            final Map<String, Object> configValues,
            final CacheDataDescription metadata) throws CacheException {

        try {
            final Cache cache = buildCache(regionName, configValues);
            return new CollectionRegionV5Adapter(regionName, cache, this::getCacheStrategyClass, metadata);
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    @Deprecated
    @SuppressWarnings("unchecked")
    public QueryResultsRegion buildQueryResultsRegion(final String regionName, final Properties properties)
            throws CacheException {

        return buildQueryResultsRegion(regionName, (Map) properties);
    }

    @Override
    public QueryResultsRegion buildQueryResultsRegion(final String qualifyRegionName, final Map<String, Object> configValues) {
        try {
            return new QueryResultsRegionV5Adapter(qualifyRegionName, buildCache(qualifyRegionName, configValues));
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    @Deprecated
    @SuppressWarnings("unchecked")
    public TimestampsRegion buildTimestampsRegion(final String regionName, final Properties properties)
            throws CacheException {

        return buildTimestampsRegion(regionName, (Map) properties);
    }

    @Override
    public TimestampsRegion buildTimestampsRegion(final String regionName, final Map<String, Object> configValues)
            throws CacheException {

        String regionNameToUse = regionName;
        if (regionNameToUse.equals(org.hibernate.cache.spi.UpdateTimestampsCache.class.getName())) {
            regionNameToUse = net.sf.hibernate.cache.UpdateTimestampsCache.class.getName();
        }

        try {
            return new TimestampsRegionV5Adapter(regionNameToUse, buildCache(regionNameToUse, configValues));
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    @Deprecated
    public NaturalIdRegion buildNaturalIdRegion(
            final String regionName,
            final Properties properties,
            final CacheDataDescription metadata) throws CacheException {

        throw new NotImplementedException("buildNaturalIdRegion not implemented");
    }

    @Override
    public NaturalIdRegion buildNaturalIdRegion(
            final String regionName,
            final Map<String, Object> configValues,
            final CacheDataDescription metadata) throws CacheException {

        throw new NotImplementedException("buildNaturalIdRegion not implemented");
    }

    private Cache buildCache(final String regionName, final Map<String, Object> configValues)
            throws net.sf.hibernate.cache.CacheException {

        final Properties properties = new Properties();
        properties.putAll(configValues);
        return cacheProvider.buildCache(regionName + "_v5", properties);
    }

    private String getCacheStrategyClass(final String entityRegion) {
        String result = cacheStrategyClassMap != null ? cacheStrategyClassMap.get(entityRegion) : null;
        if (result == null) {
            result = cacheStrategyClass;
        }
        return result;
    }
}
