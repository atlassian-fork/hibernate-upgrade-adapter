package com.atlassian.hibernate.adapter.adapters.persister;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.PropertyValueAdapter;
import com.atlassian.hibernate.adapter.adapters.cache.EntityRegionAccessStrategyV5Adapter;
import com.atlassian.hibernate.adapter.adapters.cache.RegionFactoryCacheProviderBridge;
import com.atlassian.hibernate.adapter.adapters.generator.IdentifierGeneratorV2Adapter;
import com.atlassian.hibernate.adapter.adapters.mapping.ClassMetadataV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.LockMode;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.cache.CacheConcurrencyStrategy;
import net.sf.hibernate.engine.Cascades;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.id.IdentifierGenerator;
import net.sf.hibernate.metadata.ClassMetadata;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.type.Type;
import net.sf.hibernate.type.VersionType;
import org.apache.commons.lang3.NotImplementedException;

import javax.persistence.PersistenceException;
import java.io.Serializable;

/**
 * An adapter bridging the ClassPersister interface for hibernate v5 (onwards) to v2.
 */
public class ClassPersisterV2Adapter implements ClassPersister {

    private final org.hibernate.persister.entity.EntityPersister persister;
    private final org.hibernate.SessionFactory sessionFactory;
    private final org.hibernate.Session session;

    protected ClassPersisterV2Adapter(
            final org.hibernate.persister.entity.EntityPersister persister,
            final org.hibernate.SessionFactory sessionFactory,
            final org.hibernate.Session session) {

        this.persister = persister;
        this.sessionFactory = sessionFactory;
        this.session = session;
    }

    public static net.sf.hibernate.persister.ClassPersister adapt(
            final org.hibernate.persister.entity.EntityPersister persister,
            final org.hibernate.SessionFactory sessionFactory,
            final org.hibernate.Session session) {

        if (persister == null)
            return null;

        if (persister instanceof org.hibernate.persister.entity.AbstractEntityPersister)
            return AbstractEntityPersisterV2Adapter.adaptAbstractEntityPersister(
                    (org.hibernate.persister.entity.AbstractEntityPersister) persister, sessionFactory, session);

        return adaptClassPersisterOnly(persister, sessionFactory, session);
    }

    public static net.sf.hibernate.persister.ClassPersister adaptClassPersisterOnly(
            final org.hibernate.persister.entity.EntityPersister persister,
            final org.hibernate.SessionFactory sessionFactory,
            final org.hibernate.Session session) {

        if (persister == null)
            return null;
        return new ClassPersisterV2Adapter(persister, sessionFactory, session);
    }

    @Override
    public void postInstantiate() throws MappingException {
        persister.postInstantiate();
    }

    @Override
    public Serializable getIdentifierSpace() {
        throw new NotImplementedException("getIdentifierSpace not implemented");
    }

    @Override
    public Serializable[] getPropertySpaces() {
        return persister.getPropertySpaces();
    }

    @Override
    public Class getMappedClass() {
        return persister.getMappedClass();
    }

    @Override
    public String getClassName() {
        return persister.getEntityName();
    }

    @Override
    public boolean implementsLifecycle() {
        throw new NotImplementedException("implementsLifecycle not implemented");
    }

    @Override
    public boolean implementsValidatable() {
        throw new NotImplementedException("implementsValidatable not implemented");
    }

    @Override
    public boolean hasProxy() {
        return persister.hasProxy();
    }

    @Override
    public Class getConcreteProxyClass() {
        return persister.getConcreteProxyClass();
    }

    @Override
    public Object createProxy(final Serializable id, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("createProxy not implemented");
    }

    @Override
    public boolean hasCollections() {
        return persister.hasCollections();
    }

    @Override
    public boolean hasCascades() {
        return persister.hasCascades();
    }

    @Override
    public boolean isMutable() {
        return persister.isMutable();
    }

    @Override
    public boolean isIdentifierAssignedByInsert() {
        return persister.isIdentifierAssignedByInsert();
    }

    @Override
    public boolean isUnsaved(final Object object) throws HibernateException {
        throw new NotImplementedException("isUnsaved not implemented");
    }

    @Override
    public void setPropertyValues(final Object object, final Object[] values) throws HibernateException {
        try {
            persister.setPropertyValues(object, PropertyValueAdapter.adaptToV5(values));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object[] getPropertyValues(final Object object) throws HibernateException {
        try {
            return PropertyValueAdapter.adaptToV2(persister.getPropertyValues(object));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void setPropertyValue(final Object object, final int i, final Object value) throws HibernateException {
        try {
            persister.setPropertyValue(object, i, value);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object getPropertyValue(final Object object, final int i) throws HibernateException {
        try {
            return persister.getPropertyValue(object, i);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object getPropertyValue(final Object object, final String propertyName) throws HibernateException {
        try {
            return persister.getPropertyValue(object, propertyName);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Type getPropertyType(final String propertyName) throws MappingException {
        try {
            return TypeV2Adapter.adapt(persister.getPropertyType(propertyName));
        } catch (final org.hibernate.MappingException ex) {
            throw (MappingException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public int[] findDirty(final Object[] x, final Object[] y, final Object owner, final SessionImplementor session)
            throws HibernateException {
        throw new NotImplementedException("findDirty not implemented");
    }

    @Override
    public int[] findModified(final Object[] old, final Object[] current, final Object object, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("findModified not implemented");
    }

    @Override
    public boolean hasIdentifierProperty() {
        return persister.hasIdentifierProperty();
    }

    @Override
    public boolean hasIdentifierPropertyOrEmbeddedCompositeIdentifier() {
        return hasIdentifierProperty() || persister.getEntityMetamodel().getIdentifierProperty().isEmbedded();
    }

    @Override
    public Serializable getIdentifier(final Object object) throws HibernateException {
        if (session == null) {
            throw new NotImplementedException("getIdentifier not available without a session");
        }
        try {
            return persister.getIdentifier(object, (org.hibernate.engine.spi.SessionImplementor) session);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void setIdentifier(final Object object, final Serializable id) throws HibernateException {
        if (session == null) {
            throw new NotImplementedException("setIdentifier not implemented without session");
        }
        try {
            persister.setIdentifier(object, id, (org.hibernate.engine.spi.SessionImplementor) session);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean isVersioned() {
        return persister.isVersioned();
    }

    @Override
    public VersionType getVersionType() {
        return (VersionType) TypeV2Adapter.adapt(persister.getVersionType());
    }

    @Override
    public int getVersionProperty() {
        return persister.getVersionProperty();
    }

    @Override
    public Object getVersion(final Object object) throws HibernateException {
        try {
            return persister.getVersion(object);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object instantiate(final Serializable id) throws HibernateException {
        if (session == null) {
            throw new NotImplementedException("instantiate not implemented without session");
        }
        try {
            return persister.instantiate(id, (org.hibernate.engine.spi.SessionImplementor) session);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public IdentifierGenerator getIdentifierGenerator() throws HibernateException {
        return IdentifierGeneratorV2Adapter.adapt(
                persister.getIdentifierGenerator(),
                (org.hibernate.engine.spi.SessionFactoryImplementor) sessionFactory);
    }

    @Override
    public Object load(final Serializable id, final Object optionalObject, final LockMode lockMode, final SessionImplementor session)
            throws HibernateException {
        throw new NotImplementedException("load not implemented");
    }

    @Override
    public void lock(final Serializable id, final Object version, final Object object, final LockMode lockMode, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("lock not implemented");
    }

    @Override
    public void insert(final Serializable id, final Object[] fields, final Object object, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("insert not implemented");
    }

    @Override
    public Serializable insert(final Object[] fields, final Object object, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("insert not implemented");
    }

    @Override
    public void delete(final Serializable id, final Object version, final Object object, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("delete not implemented");
    }

    @Override
    public void update(final Serializable id, final Object[] fields, final int[] dirtyFields, final Object[] oldFields, final Object oldVersion, final Object object, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("update not implemented");
    }

    @Override
    public Type[] getPropertyTypes() {
        return TypeV2Adapter.adapt(persister.getPropertyTypes());
    }

    @Override
    public String[] getPropertyNames() {
        return persister.getPropertyNames();
    }

    @Override
    public boolean[] getPropertyUpdateability() {
        return persister.getPropertyUpdateability();
    }

    @Override
    public boolean[] getPropertyNullability() {
        return persister.getPropertyNullability();
    }

    @Override
    public boolean[] getPropertyInsertability() {
        return persister.getPropertyInsertability();
    }

    @Override
    public Cascades.CascadeStyle[] getPropertyCascadeStyles() {
        throw new NotImplementedException("getPropertyCascadeStyles not implemented");
    }

    @Override
    public Type getIdentifierType() {
        return TypeV2Adapter.adapt(persister.getIdentifierType());
    }

    @Override
    public String getIdentifierPropertyName() {
        return persister.getIdentifierPropertyName();
    }

    @Override
    public boolean isCacheInvalidationRequired() {
        return persister.isCacheInvalidationRequired();
    }

    @Override
    public boolean hasCache() {
        return persister.hasCache();
    }

    @Override
    public CacheConcurrencyStrategy getCache() {
        final org.hibernate.cache.spi.access.EntityRegionAccessStrategy strategy =
                persister.getCacheAccessStrategy();

        if (strategy == null)
            return null;
        if (strategy instanceof EntityRegionAccessStrategyV5Adapter)
            return ((EntityRegionAccessStrategyV5Adapter) strategy).getCacheConcurrencyStrategy();

        throw new NotImplementedException(
                "getCache only available when using "
                + RegionFactoryCacheProviderBridge.class.getSimpleName());
    }

    @Override
    public ClassMetadata getClassMetadata() {
        return ClassMetadataV2Adapter.adapt(persister.getClassMetadata(), sessionFactory, null);
    }

    @Override
    public boolean isBatchLoadable() {
        return persister.isBatchLoadable();
    }

    @Override
    public Object[] getCurrentPersistentState(final Serializable id, final Object version, final SessionImplementor session)
            throws HibernateException {
        throw new NotImplementedException("getCurrentPersistentState not implemented");
    }

    @Override
    public Object getCurrentVersion(final Serializable id, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("getCurrentVersion not implemented");
    }
}
