package com.atlassian.hibernate.adapter.adapters.type.relation;

import com.atlassian.hibernate.adapter.type.V5TypeSupplier;
import com.atlassian.hibernate.util.ThrowableUtil;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.engine.Cascades;
import net.sf.hibernate.engine.Mapping;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.type.ComponentType;
import net.sf.hibernate.type.Type;
import org.apache.commons.lang3.NotImplementedException;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * An adapter bridging the ComponentType class from hibernate v5 (onwards) to v2.
 */
public class ComponentTypeV2Adapter extends ComponentType implements V5TypeSupplier {
    private final org.hibernate.type.ComponentType type;
    private final CompositeTypeV2Adapter impl;

    protected ComponentTypeV2Adapter(final org.hibernate.type.ComponentType type)
            throws net.sf.hibernate.MappingException {
        super(type.getReturnedClass(), new String[0], null, null, true, null, null, null, null);
        this.type = type;
        this.impl = new CompositeTypeV2Adapter(type);
    }

    public static ComponentType adapt(final org.hibernate.type.ComponentType type) {
        if (type == null)
            return null;

        try {
            return new ComponentTypeV2Adapter(type);
        } catch (final net.sf.hibernate.MappingException ex) {
            // this should never happen
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public org.hibernate.type.Type getV5Type() {
        return type;
    }

    //---------- Object Overrides ----------//

    @Override
    public boolean equals(final Object object) {
        return impl.equals(object);
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    @Override
    public String toString() {
        return type.toString();
    }

    //---------- ComponentType Overrides ----------//

    @Override
    public Object instantiate(final Object parent, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("instantiate(Object, SessionImplementor) not implemented");
    }

    //---------- CompositeType Overrides ----------//

    @Override
    public Type[] getSubtypes() {
        return impl.getSubtypes();
    }

    @Override
    public String[] getPropertyNames() {
        return impl.getPropertyNames();
    }

    @Override
    public Object[] getPropertyValues(final Object component, final SessionImplementor session) throws HibernateException {
        return impl.getPropertyValues(component, session);
    }

    @Override
    public Object[] getPropertyValues(final Object component) throws HibernateException {
        return impl.getPropertyValues(component);
    }

    @Override
    public void setPropertyValues(final Object component, final Object[] values) throws HibernateException {
        impl.setPropertyValues(component, values);
    }

    @Override
    public Object getPropertyValue(final Object component, final int i, final SessionImplementor session) throws HibernateException {
        return impl.getPropertyValue(component, i, session);
    }

    @Override
    public Cascades.CascadeStyle cascade(final int i) {
        return impl.cascade(i);
    }

    @Override
    public int enableJoinedFetch(final int i) {
        return impl.enableJoinedFetch(i);
    }

    //---------- Type Overrides ----------//

    @Override
    public boolean isAssociationType() {
        return impl.isAssociationType();
    }

    //public boolean isComponentType()

    @Override
    public boolean isEntityType() {
        return impl.isEntityType();
    }

    @Override
    public Class getReturnedClass() {
        return impl.getReturnedClass();
    }

    @Override
    public String getName() {
        return impl.getName();
    }

    @Override
    public boolean isMutable() {
        return impl.isMutable();
    }

    @Override
    public boolean isPersistentCollectionType() {
        return impl.isPersistentCollectionType();
    }

    @Override
    public boolean isObjectType() {
        return impl.isObjectType();
    }

    @Override
    public int[] sqlTypes(final Mapping mapping) throws MappingException {
        return impl.sqlTypes(mapping);
    }

    @Override
    public int getColumnSpan(final Mapping mapping) throws MappingException {
        return impl.getColumnSpan(mapping);
    }

    @Override
    public boolean equals(final Object x, final Object y) throws HibernateException {
        return impl.equals(x, y);
    }

    @Override
    public boolean isDirty(final Object old, final Object current, final SessionImplementor session) throws HibernateException {
        return impl.isDirty(old, current, session);
    }

    @Override
    public boolean isModified(final Object oldHydratedState, final Object currentState, final SessionImplementor session) throws HibernateException {
        return impl.isModified(oldHydratedState, currentState, session);
    }

    @Override
    public Object nullSafeGet(final ResultSet rs, final String[] names, final SessionImplementor session, final Object owner) throws HibernateException, SQLException {
        return impl.nullSafeGet(rs, names, session, owner);
    }

    @Override
    public Object nullSafeGet(final ResultSet rs, final String name, final SessionImplementor session, final Object owner) throws HibernateException, SQLException {
        return impl.nullSafeGet(rs, name, session, owner);
    }

    @Override
    public void nullSafeSet(final PreparedStatement st, final Object value, final int index, final SessionImplementor session) throws HibernateException, SQLException {
        impl.nullSafeSet(st, value, index, session);
    }

    @Override
    public String toString(final Object value, final SessionFactoryImplementor factory) throws HibernateException {
        return impl.toString(value, factory);
    }

    @Override
    public Object fromString(final String xml) {
        return impl.fromString(xml);
    }

    @Override
    public Object deepCopy(final Object value) throws HibernateException {
        return impl.deepCopy(value);
    }

    @Override
    public Serializable disassemble(final Object value, final SessionImplementor session) throws HibernateException {
        return impl.disassemble(value, session);
    }

    @Override
    public Object assemble(final Serializable cached, final SessionImplementor session, final Object owner) throws HibernateException {
        return impl.assemble(cached, session, owner);
    }

    @Override
    @Deprecated
    public boolean hasNiceEquals() {
        return impl.hasNiceEquals();
    }

    @Override
    public Object hydrate(final ResultSet rs, final String[] names, final SessionImplementor session, final Object owner) throws HibernateException, SQLException {
        return impl.hydrate(rs, names, session, owner);
    }

    @Override
    public Object resolveIdentifier(final Object value, final SessionImplementor session, final Object owner) throws HibernateException {
        return impl.resolveIdentifier(value, session, owner);
    }

    @Override
    public Object copy(final Object original, final Object target, final SessionImplementor session, final Object owner, final Map copiedAlready) throws HibernateException {
        return impl.copy(original, target, session, owner, copiedAlready);
    }
}
