package com.atlassian.hibernate.adapter.adapters.criterion;

import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import net.sf.hibernate.expression.Example;

/**
 * An adapter bridging the Example.PropertySelector interface for hibernate v5 (onwards) to v2.
 */
public final class ExamplePropertySelectorV2Adapter {

    private ExamplePropertySelectorV2Adapter() {
    }

    public static org.hibernate.criterion.Example.PropertySelector adapt(
            final Example.PropertySelector propertySelector) {

        if (propertySelector == null)
            return null;

        final String name = propertySelector.getClass().getSimpleName();
        if (name.equals("AllPropertySelector"))
            return org.hibernate.criterion.Example.AllPropertySelector.INSTANCE;
        if (name.equals("NotNullPropertySelector"))
            return org.hibernate.criterion.Example.NotNullPropertySelector.INSTANCE;
        if (name.equals("NotNullOrZeroPropertySelector"))
            return org.hibernate.criterion.Example.NotNullOrZeroPropertySelector.INSTANCE;

        return (propertyValue, propertyName, type) ->
                propertySelector.include(propertyValue, propertyName, TypeV2Adapter.adapt(type));
    }
}
