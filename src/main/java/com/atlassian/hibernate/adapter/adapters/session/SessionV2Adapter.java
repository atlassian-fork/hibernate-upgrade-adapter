package com.atlassian.hibernate.adapter.adapters.session;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.BatcherV2StubImpl;
import com.atlassian.hibernate.adapter.adapters.FlushModeAdapter;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.LockModeAdapter;
import com.atlassian.hibernate.adapter.adapters.ReplicationModeV2Adapter;
import com.atlassian.hibernate.adapter.adapters.TransactionV2Adapter;
import com.atlassian.hibernate.adapter.adapters.criteria.CriteriaV2Adapter;
import com.atlassian.hibernate.adapter.adapters.persister.ClassPersisterV2Adapter;
import com.atlassian.hibernate.adapter.adapters.query.QueryV2Adapter;
import com.atlassian.hibernate.adapter.adapters.query.QueryV2Supplier;
import com.atlassian.hibernate.adapter.adapters.type.TypeV5Adapter;
import com.atlassian.hibernate.adapter.transpiler.HibernateQueryTranspiler;
import com.atlassian.hibernate.util.SessionHelper;
import com.atlassian.hibernate.util.ThrowableUtil;
import com.atlassian.hibernate.util.TransactionHelper;
import net.sf.hibernate.Criteria;
import net.sf.hibernate.FlushMode;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.LockMode;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.Query;
import net.sf.hibernate.ReplicationMode;
import net.sf.hibernate.ScrollableResults;
import net.sf.hibernate.Session;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.Transaction;
import net.sf.hibernate.collection.ArrayHolder;
import net.sf.hibernate.collection.CollectionPersister;
import net.sf.hibernate.collection.PersistentCollection;
import net.sf.hibernate.engine.Batcher;
import net.sf.hibernate.engine.Key;
import net.sf.hibernate.engine.QueryParameters;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.type.Type;
import net.sf.hibernate.util.ArrayHelper;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.event.spi.EventSource;
import org.hibernate.proxy.HibernateProxy;

import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * An adapter bridging the Session interface for hibernate v5 (onwards) to v2.
 */
public class SessionV2Adapter implements Session, SessionImplementor {
    private static final Object[] NO_ARGS = ArrayHelper.EMPTY_STRING_ARRAY;
    private static final Type[] NO_TYPES = ArrayHelper.EMPTY_TYPE_ARRAY;

    private final org.hibernate.Session session;
    private final org.hibernate.engine.spi.SessionImplementor sessionImplementor;
    private final net.sf.hibernate.SessionFactory sessionFactory;

    protected SessionV2Adapter(
            final org.hibernate.Session session,
            final net.sf.hibernate.SessionFactory sessionFactory) {

        this.session = session;
        this.sessionImplementor = (org.hibernate.engine.spi.SessionImplementor) session;
        this.sessionFactory = sessionFactory;
    }

    public static Session adapt(
            final org.hibernate.Session session,
            final net.sf.hibernate.SessionFactory sessionFactory) {

        if (session == null)
            return null;
        return new SessionV2Adapter(session, sessionFactory);
    }

    private static void checkNotValidatable(final Object entity) {
        if (entity instanceof net.sf.hibernate.Validatable) {
            throw new IllegalArgumentException(
                    "Object implements net.sf.hibernate.Validatable, which isn't supported in this context");
        }
    }

    private net.sf.hibernate.Query adaptQuery(
            final org.hibernate.query.Query query,
            final QueryV2Supplier queryV2Supplier,
            final boolean areParametersOneBased) throws net.sf.hibernate.HibernateException {

        return adaptQuery(session, query, queryV2Supplier, areParametersOneBased);
    }

    protected net.sf.hibernate.Query adaptQuery(
            final org.hibernate.Session session,
            final org.hibernate.query.Query query,
            final QueryV2Supplier queryV2Supplier,
            final boolean areParametersOneBased) throws net.sf.hibernate.HibernateException {

        return QueryV2Adapter.adapt(session, query, null, areParametersOneBased);
    }

    private net.sf.hibernate.Session getSessionV2ForQueryComparison() {
        return HibernateBridge.get(sessionFactory).getV2Session(session);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object get(final Class clazz, final Serializable id) throws HibernateException {
        // this is how HibernateBridgeImpl gets to the v5 Session object, even if
        // the Session interface is intercepted by a java or spring proxy
        if (clazz == org.hibernate.Session.class) {
            return session;
        }

        try {
            return session.get(clazz, id);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    //---------- Session ----------//

    protected org.hibernate.Session getV5Session() {
        return session;
    }

    @Override
    public void flush() throws HibernateException {
        try {
            SessionHelper.flushAllowNoTransaction(session);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public FlushMode getFlushMode() {
        return FlushModeAdapter.adapt(session.getHibernateFlushMode());
    }

    @Override
    public void setFlushMode(final FlushMode flushMode) {
        session.setHibernateFlushMode(FlushModeAdapter.adapt(flushMode));
    }

    @Override
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public Connection connection() throws HibernateException {
        try {
            return sessionImplementor.connection();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Connection disconnect() throws HibernateException {
        try {
            return session.disconnect();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void reconnect() throws HibernateException {
        throw new NotImplementedException("reconnect not implemented");
    }

    @Override
    public void reconnect(final Connection connection) throws HibernateException {
        try {
            session.reconnect(connection);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Connection close() throws HibernateException {
        try {
            session.close();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return null;
    }

    @Override
    public void cancelQuery() throws HibernateException {
        throw new NotImplementedException("cancelQuery");
    }

    @Override
    public boolean isOpen() {
        return session.isOpen();
    }

    @Override
    public boolean isConnected() {
        return session.isConnected();
    }

    @Override
    public boolean isDirty() throws HibernateException {
        try {
            return session.isDirty();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Serializable getIdentifier(final Object object) throws HibernateException {
        try {
            return session.getIdentifier(object);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean contains(final Object object) {
        return session.contains(object);
    }

    @Override
    public void evict(final Object object) throws HibernateException {
        try {
            // emulate the v2 behaviour of ignoring non-entity objects
            if (object instanceof HibernateProxy || ((EventSource) session).getPersistenceContext().isEntryFor(object)) {
                session.evict(object);
            }
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object load(final Class theClass, final Serializable id, final LockMode lockMode) throws HibernateException {
        try {
            return session.load(theClass, id, LockModeAdapter.adapt(lockMode));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object load(final Class theClass, final Serializable id) throws HibernateException {
        try {
            return session.load(theClass, id);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void load(final Object object, final Serializable id) throws HibernateException {
        try {
            session.load(object, id);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void replicate(final Object object, final ReplicationMode replicationMode) throws HibernateException {
        checkNotValidatable(object);
        try {
            session.replicate(object, ReplicationModeV2Adapter.adapt(replicationMode));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Serializable save(final Object object) throws HibernateException {
        checkNotValidatable(object);
        try {
            return session.save(object);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void save(final Object object, final Serializable id) throws HibernateException {
        try {
            SessionHelper.save(session, object, id);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void saveOrUpdate(final Object object) throws HibernateException {
        try {
            session.saveOrUpdate(object);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void update(final Object object) throws HibernateException {
        checkNotValidatable(object);
        try {
            session.update(object);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void update(final Object object, final Serializable id) throws HibernateException {
        throw new NotImplementedException("update not implemented");
    }

    @Override
    public Object saveOrUpdateCopy(final Object object) throws HibernateException {
        throw new NotImplementedException("saveOrUpdateCopy not implemented");
    }

    @Override
    public Object saveOrUpdateCopy(final Object object, final Serializable id) throws HibernateException {
        throw new NotImplementedException("saveOrUpdateCopy not implemented");
    }

    @Override
    public void delete(final Object object) throws HibernateException {
        try {
            session.delete(object);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        } catch (final IllegalArgumentException ex) {
            if (ex.getCause() instanceof org.hibernate.ObjectDeletedException) {
                throw HibernateExceptionAdapter.adapt((org.hibernate.ObjectDeletedException) ex.getCause());
            }
            throw ex;
        }
    }

    @Override
    public List find(final String query) throws HibernateException {
        try {
            return session.createQuery(HibernateQueryTranspiler.transpileQuery(query)).list();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public List find(final String queryString, final Object value, final Type type) throws HibernateException {
        try {
            return SessionHelper.createQuery(
                    session,
                    HibernateQueryTranspiler.transpileQuery(queryString),
                    value,
                    TypeV5Adapter.adapt(session.getSessionFactory(), type)).list();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public List find(final String query, final Object[] values, final Type[] types) throws HibernateException {
        try {
            return SessionHelper.createQuery(
                    session,
                    HibernateQueryTranspiler.transpileQuery(query),
                    values,
                    TypeV5Adapter.adapt(session.getSessionFactory(), types)).list();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Iterator iterate(final String query) throws HibernateException {
        try {
            return createQuery(query).iterate();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Iterator iterate(final String query, final Object value, final Type type) throws HibernateException {
        try {
            return SessionHelper.createQuery(
                    session,
                    HibernateQueryTranspiler.transpileQuery(query),
                    value,
                    TypeV5Adapter.adapt(session.getSessionFactory(), type)).iterate();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Iterator iterate(final String query, final Object[] values, final Type[] types) throws HibernateException {
        try {
            return SessionHelper.createQuery(
                    session,
                    HibernateQueryTranspiler.transpileQuery(query),
                    values,
                    TypeV5Adapter.adapt(session.getSessionFactory(), types)).iterate();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Collection filter(final Object collection, final String filter) throws HibernateException {
        try {
            return session.createFilter(collection, filter).list();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Collection filter(final Object collection, final String filter, final Object value, final Type type) throws HibernateException {
        throw new NotImplementedException("filter not implemented");
    }

    @Override
    public Collection filter(final Object collection, final String filter, final Object[] values, final Type[] types) throws HibernateException {
        throw new NotImplementedException("filter not implemented");
    }

    @Override
    public int delete(final String query) throws HibernateException {
        return delete(query, NO_ARGS, NO_TYPES);
    }

    @Override
    public int delete(final String query, final Object value, final Type type) throws HibernateException {
        return delete(query, new Object[]{value}, new Type[]{type});
    }

    @Override
    public int delete(final String query, final Object[] values, final Type[] types) throws HibernateException {
        try {
            return SessionHelper.delete(
                    session,
                    HibernateQueryTranspiler.transpileQuery(query),
                    values,
                    TypeV5Adapter.adapt(session.getSessionFactory(), types));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        } catch (final IllegalArgumentException ex) {
            if (ex.getCause() instanceof org.hibernate.ObjectDeletedException) {
                throw HibernateExceptionAdapter.adapt((org.hibernate.ObjectDeletedException) ex.getCause());
            }
            throw ex;
        }
    }

    @Override
    public void lock(final Object object, final LockMode lockMode) throws HibernateException {
        session.lock(object, LockModeAdapter.adapt(lockMode));
    }

    @Override
    public void refresh(final Object object) throws HibernateException {
        try {
            session.refresh(object);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void refresh(final Object object, final LockMode lockMode) throws HibernateException {
        try {
            session.refresh(object, LockModeAdapter.adapt(lockMode));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public LockMode getCurrentLockMode(final Object object) throws HibernateException {
        try {
            return LockModeAdapter.adapt(session.getCurrentLockMode(object));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Transaction beginTransaction() throws HibernateException {
        try {
            return TransactionV2Adapter.adapt(TransactionHelper.beginTransactionCommitNested(session));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public Criteria createCriteria(final Class persistentClass) {
        return CriteriaV2Adapter.adapt(
                session.getSessionFactory(),
                session.createCriteria(persistentClass));
    }

    @Override
    public Query createQuery(final String queryString) throws HibernateException {
        try {
            final org.hibernate.query.Query query = session.createQuery(
                    HibernateQueryTranspiler.transpileQuery(queryString));
            return adaptQuery(
                    query,
                    () -> getSessionV2ForQueryComparison().createQuery(queryString),
                    false);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Query createFilter(final Object collection, final String queryString) throws HibernateException {
        try {
            return adaptQuery(
                    session.createFilter(collection, queryString),
                    () -> getSessionV2ForQueryComparison().createFilter(collection, queryString),
                    false);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Query getNamedQuery(final String queryName) throws HibernateException {
        try {
            return adaptQuery(
                    session.getNamedQuery(queryName),
                    () -> getSessionV2ForQueryComparison().getNamedQuery(queryName),
                    false);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Query createSQLQuery(final String sql, final String returnAlias, final Class returnClass) {
        final org.hibernate.query.NativeQuery query = session.createNativeQuery(sql).addEntity(returnAlias, returnClass);
        try {
            return adaptQuery(query, () -> getSessionV2ForQueryComparison().createSQLQuery(sql, returnAlias, returnClass), true);
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Query createSQLQuery(final String sql, final String[] returnAliases, final Class[] returnClasses) {
        final org.hibernate.query.NativeQuery query = SessionHelper.createNativeQuery(
                session, sql, returnAliases, returnClasses);
        try {
            return adaptQuery(query, () -> getSessionV2ForQueryComparison().createSQLQuery(sql, returnAliases, returnClasses), true);
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void clear() {
        session.clear();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object get(final Class clazz, final Serializable id, final LockMode lockMode) throws HibernateException {
        try {
            return session.get(clazz, id, LockModeAdapter.adapt(lockMode));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    //---------- SessionImplementor ----------//

    @Override
    public Serializable getLoadedCollectionKey(final PersistentCollection collection) {
        throw new NotImplementedException("getLoadedCollectionKey not implemented");
    }

    @Override
    public Serializable getSnapshot(final PersistentCollection collection) {
        throw new NotImplementedException("getSnapshot not implemented");
    }

    @Override
    public ArrayHolder getArrayHolder(final Object array) {
        throw new NotImplementedException("getArrayHolder not implemented");
    }

    @Override
    public void addArrayHolder(final ArrayHolder holder) {
        throw new NotImplementedException("addArrayHolder not implemented");
    }

    @Override
    public void initializeCollection(final PersistentCollection collection, final boolean writing) throws HibernateException {
        throw new NotImplementedException("getSnapshot not implemented");
    }

    @Override
    public boolean isInverseCollection(final PersistentCollection collection) {
        throw new NotImplementedException("isInverseCollection not implemented");
    }

    @Override
    public PersistentCollection getLoadingCollection(final CollectionPersister persister, final Serializable id, final Object resultSetId) throws HibernateException {
        throw new NotImplementedException("getLoadingCollection not implemented");
    }

    @Override
    public void endLoadingCollections(final CollectionPersister persister, final Object resultSetId) throws HibernateException {
        throw new NotImplementedException("endLoadingCollections not implemented");
    }

    @Override
    public void afterLoad() {
        throw new NotImplementedException("afterLoad not implemented");
    }

    @Override
    public void beforeLoad() {
        throw new NotImplementedException("beforeLoad not implemented");
    }

    @Override
    public void initializeNonLazyCollections() throws HibernateException {
        throw new NotImplementedException("initializeNonLazyCollections not implemented");
    }

    @Override
    public Object getCollection(final String role, final Serializable id, final Object owner) throws HibernateException {
        throw new NotImplementedException("getCollection not implemented");
    }

    @Override
    public Object internalLoad(final Class persistentClass, final Serializable id) throws HibernateException {
        throw new NotImplementedException("internalLoad not implemented");
    }

    @Override
    public Object internalLoadOneToOne(final Class persistentClass, final Serializable id) throws HibernateException {
        throw new NotImplementedException("internalLoadOneToOne not implemented");
    }

    @Override
    public Object immediateLoad(final Class persistentClass, final Serializable id) throws HibernateException {
        try {
            return sessionImplementor.immediateLoad(persistentClass.getName(), id);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object loadByUniqueKey(final Class persistentClass, final String uniqueKeyPropertyName, final Serializable id) throws HibernateException {
        throw new NotImplementedException("loadByUniqueKey not implemented");
    }

    @Override
    public long getTimestamp() {
        return sessionImplementor.getTimestamp();
    }

    @Override
    public SessionFactoryImplementor getFactory() {
        return (SessionFactoryImplementor) sessionFactory;
    }

    @Override
    public Batcher getBatcher() {
        return new BatcherV2StubImpl(sessionFactory);
    }

    @Override
    public void postInsert(final Object object) {
        throw new NotImplementedException("postInsert not implemented");
    }

    @Override
    public void postDelete(final Object object) {
        throw new NotImplementedException("postDelete not implemented");
    }

    @Override
    public void postUpdate(final Object object, final Object[] updatedState, final Object nextVersion) throws HibernateException {
        throw new NotImplementedException("postUpdate not implemented");
    }

    @Override
    public List find(final String query, final QueryParameters queryParameters) throws HibernateException {
        throw new NotImplementedException("find(String, QueryParameters) not implemented");
    }

    @Override
    public Iterator iterate(final String query, final QueryParameters queryParameters) throws HibernateException {
        throw new NotImplementedException("iterate(String, QueryParameters) not implemented");
    }

    @Override
    public ScrollableResults scroll(final String query, final QueryParameters queryParameters) throws HibernateException {
        throw new NotImplementedException("scroll(String, QueryParameters) not implemented");
    }

    @Override
    public List filter(final Object collection, final String filter, final QueryParameters queryParameters) throws HibernateException {
        throw new NotImplementedException("scroll(String, QueryParameters) not implemented");
    }

    @Override
    public Iterator iterateFilter(final Object collection, final String filter, final QueryParameters queryParameters) throws HibernateException {
        throw new NotImplementedException("iterateFilter not implemented");
    }

    @Override
    public ClassPersister getPersister(final Object object) throws MappingException {
        final String entityName = net.sf.hibernate.Hibernate.getClass(object).getName();
        return ClassPersisterV2Adapter.adapt(
                sessionImplementor.getEntityPersister(entityName, object),
                session.getSessionFactory(),
                session);
    }

    @Override
    public void addUninitializedEntity(final Key key, final Object object, final LockMode lockMode) {
        throw new NotImplementedException("addUninitializedEntity not implemented");
    }

    @Override
    public void postHydrate(final ClassPersister persister, final Serializable id, final Object[] values, final Object object, final LockMode lockMode) throws HibernateException {
        throw new NotImplementedException("postHydrate not implemented");
    }

    @Override
    public void initializeEntity(final Object object) throws HibernateException {
        throw new NotImplementedException("initializeEntity not implemented");
    }

    @Override
    public Object getEntity(final Key key) {
        throw new NotImplementedException("getEntity not implemented");
    }

    @Override
    public Object proxyFor(final ClassPersister persister, final Key key, final Object impl) throws HibernateException {
        throw new NotImplementedException("proxyFor(ClassPersister, Key, Object) not implemented");
    }

    @Override
    public Object proxyFor(final Object impl) throws HibernateException {
        throw new NotImplementedException("proxyFor(Object) not implemented");
    }

    @Override
    public void afterTransactionCompletion(final boolean successful) {
        // this is not required as we don't support JTA transactions
    }

    @Override
    public Serializable getEntityIdentifier(final Object obj) {
        throw new NotImplementedException("getEntityIdentifier not implemented");
    }

    @Override
    public Serializable getEntityIdentifierIfNotUnsaved(final Object object) throws HibernateException {
        throw new NotImplementedException("getEntityIdentifierIfNotUnsaved not implemented");
    }

    @Override
    public boolean isSaved(final Object object) throws HibernateException {
        throw new NotImplementedException("isSaved not implemented");
    }

    @Override
    public Object instantiate(final Class clazz, final Serializable id) throws HibernateException {
        throw new NotImplementedException("instantiate(Class, Serializable) not implemented");
    }

    @Override
    public void setLockMode(final Object entity, final LockMode lockMode) {
        throw new NotImplementedException("setLockMode not implemented");
    }

    @Override
    public Object getVersion(final Object entity) {
        throw new NotImplementedException("getVersion not implemented");
    }

    @Override
    public LockMode getLockMode(final Object object) {
        return LockModeAdapter.adapt(session.getCurrentLockMode(object));
    }

    @Override
    public Collection getOrphans(final PersistentCollection coll) throws HibernateException {
        throw new NotImplementedException("getOrphans not implemented");
    }

    @Override
    public Serializable[] getCollectionBatch(final CollectionPersister collectionPersister, final Serializable id, final int batchSize) {
        throw new NotImplementedException("getCollectionBatch not implemented");
    }

    @Override
    public Serializable[] getClassBatch(final Class clazz, final Serializable id, final int batchSize) {
        throw new NotImplementedException("getClassBatch not implemented");
    }

    @Override
    public void scheduleBatchLoad(final Class clazz, final Serializable id) throws MappingException {
        throw new NotImplementedException("scheduleBatchLoad not implemented");
    }

    @Override
    public List findBySQL(final String sqlQuery, final String[] aliases, final Class[] classes, final QueryParameters queryParameters, final Collection querySpaces) throws HibernateException {
        throw new NotImplementedException("findBySQL not implemented");
    }

    @Override
    public void addNonExist(final Key key) {
        throw new NotImplementedException("addNonExist not implemented");
    }

    @Override
    public Object copy(final Object object, final Map copiedAlready) throws HibernateException {
        throw new NotImplementedException("copy(Object, Map) not implemented");
    }

    @Override
    public Object getCollectionOwner(final Serializable key, final CollectionPersister collectionPersister) throws MappingException {
        throw new NotImplementedException("getCollectionOwner not implemented");
    }
}
