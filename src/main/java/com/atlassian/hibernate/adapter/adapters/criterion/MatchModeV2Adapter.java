package com.atlassian.hibernate.adapter.adapters.criterion;

import net.sf.hibernate.expression.MatchMode;

/**
 * An adapter bridging the MatchMode enum for hibernate v5 (onwards) to v2.
 */
public final class MatchModeV2Adapter {

    private MatchModeV2Adapter() {
    }

    public static org.hibernate.criterion.MatchMode adapt(final MatchMode value) {
        if (value == null)
            return null;

        if (value == MatchMode.ANYWHERE)
            return org.hibernate.criterion.MatchMode.ANYWHERE;
        if (value == MatchMode.END)
            return org.hibernate.criterion.MatchMode.END;
        if (value == MatchMode.EXACT)
            return org.hibernate.criterion.MatchMode.EXACT;
        if (value == MatchMode.START)
            return org.hibernate.criterion.MatchMode.START;

        throw new IllegalArgumentException("Unexpected MatchMode value: " + value);
    }
}
