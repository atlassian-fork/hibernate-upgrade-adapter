package com.atlassian.hibernate.adapter.adapters.query;

/**
 * An adapter bridging the ScrollMode enum for hibernate v2 to v5 (onwards).
 */
public final class ScrollModeV2Adapter {
    private ScrollModeV2Adapter() {
    }

    public static org.hibernate.ScrollMode adapt(final net.sf.hibernate.ScrollMode value) {
        if (value == null)
            return null;

        if (value == net.sf.hibernate.ScrollMode.FORWARD_ONLY)
            return org.hibernate.ScrollMode.FORWARD_ONLY;
        if (value == net.sf.hibernate.ScrollMode.SCROLL_SENSITIVE)
            return org.hibernate.ScrollMode.SCROLL_SENSITIVE;
        if (value == net.sf.hibernate.ScrollMode.SCROLL_INSENSITIVE)
            return org.hibernate.ScrollMode.SCROLL_INSENSITIVE;

        throw new IllegalArgumentException("Unexpected org.hibernate.ScrollMode value: " + value);
    }
}
