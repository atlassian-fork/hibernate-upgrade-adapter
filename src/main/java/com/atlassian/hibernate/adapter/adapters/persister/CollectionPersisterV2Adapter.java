package com.atlassian.hibernate.adapter.adapters.persister;

import com.atlassian.hibernate.adapter.adapters.cache.CollectionRegionAccessStrategyV5Adapter;
import com.atlassian.hibernate.adapter.adapters.cache.RegionFactoryCacheProviderBridge;
import com.atlassian.hibernate.adapter.adapters.generator.IdentifierGeneratorV2Adapter;
import com.atlassian.hibernate.adapter.adapters.mapping.CollectionMetadataV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.cache.CacheConcurrencyStrategy;
import net.sf.hibernate.collection.CollectionPersister;
import net.sf.hibernate.collection.PersistentCollection;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.id.IdentifierGenerator;
import net.sf.hibernate.metadata.CollectionMetadata;
import net.sf.hibernate.type.PersistentCollectionType;
import net.sf.hibernate.type.Type;
import org.apache.commons.lang3.NotImplementedException;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * An adapter bridging the CollectionPersister interface for hibernate v5 (onwards) to v2.
 */
public class CollectionPersisterV2Adapter implements CollectionPersister {
    private final org.hibernate.persister.collection.CollectionPersister persister;
    private final org.hibernate.SessionFactory sessionFactory;

    protected CollectionPersisterV2Adapter(
            final org.hibernate.persister.collection.CollectionPersister persister,
            final org.hibernate.SessionFactory sessionFactory) {

        this.persister = persister;
        this.sessionFactory = sessionFactory;
    }

    public static CollectionPersister adapt(
            final org.hibernate.persister.collection.CollectionPersister persister,
            final org.hibernate.SessionFactory sessionFactory) {

        if (persister == null)
            return null;

        if (persister instanceof org.hibernate.persister.collection.AbstractCollectionPersister)
            return AbstractCollectionPersisterV2Adapter.adaptCollectionPersister(
                    (org.hibernate.persister.collection.AbstractCollectionPersister) persister,
                    sessionFactory);

        return new CollectionPersisterV2Adapter(persister, sessionFactory);
    }

    public static CollectionPersister adaptCollectionPersisterOnly(
            final org.hibernate.persister.collection.CollectionPersister persister,
            final org.hibernate.SessionFactory sessionFactory) {

        if (persister == null)
            return null;
        return new CollectionPersisterV2Adapter(persister, sessionFactory);
    }

    @Override
    public void initialize(final Serializable key, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("initialize not implemented");
    }

    @Override
    public CacheConcurrencyStrategy getCache() {
        final org.hibernate.cache.spi.access.CollectionRegionAccessStrategy strategy =
                persister.getCacheAccessStrategy();

        if (strategy == null)
            return null;
        if (strategy instanceof CollectionRegionAccessStrategyV5Adapter)
            return ((CollectionRegionAccessStrategyV5Adapter) strategy).getCacheConcurrencyStrategy();

        throw new NotImplementedException(
                "getCache only available when using "
                + RegionFactoryCacheProviderBridge.class.getSimpleName());
    }

    @Override
    public boolean hasCache() {
        return persister.hasCache();
    }

    @Override
    public PersistentCollectionType getCollectionType() {
        return (PersistentCollectionType) TypeV2Adapter.adapt(persister.getCollectionType());
    }

    @Override
    public Type getKeyType() {
        return TypeV2Adapter.adapt(persister.getKeyType());
    }

    @Override
    public Type getIndexType() {
        return TypeV2Adapter.adapt(persister.getIndexType());
    }

    @Override
    public Type getElementType() {
        return TypeV2Adapter.adapt(persister.getElementType());
    }

    @Override
    public Class getElementClass() {
        return persister.getElementClass();
    }

    @Override
    public Object readKey(final ResultSet rs, final SessionImplementor session) throws HibernateException, SQLException {
        throw new NotImplementedException("readKey not implemented");
    }

    @Override
    public Object readElement(final ResultSet rs, final Object owner, final SessionImplementor session) throws HibernateException, SQLException {
        throw new NotImplementedException("readElement not implemented");
    }

    @Override
    public Object readIndex(final ResultSet rs, final SessionImplementor session) throws HibernateException, SQLException {
        throw new NotImplementedException("readIndex not implemented");
    }

    @Override
    public Object readIdentifier(final ResultSet rs, final SessionImplementor session) throws HibernateException, SQLException {
        throw new NotImplementedException("readIdentifier not implemented");
    }

    @Override
    public void writeKey(final PreparedStatement st, final Serializable key, final boolean writeOrder, final SessionImplementor session) throws HibernateException, SQLException {
        throw new NotImplementedException("writeKey not implemented");
    }

    @Override
    public void writeElement(final PreparedStatement st, final Object elt, final boolean writeOrder, final SessionImplementor session) throws HibernateException, SQLException {
        throw new NotImplementedException("writeElement not implemented");
    }

    @Override
    public void writeIndex(final PreparedStatement st, final Object idx, final boolean writeOrder, final SessionImplementor session) throws HibernateException, SQLException {
        throw new NotImplementedException("writeIndex not implemented");
    }

    @Override
    public void writeIdentifier(final PreparedStatement st, final Object idx, final boolean writeOrder, final SessionImplementor session) throws HibernateException, SQLException {
        throw new NotImplementedException("writeIdentifier not implemented");
    }

    @Override
    public boolean isPrimitiveArray() {
        return persister.isPrimitiveArray();
    }

    @Override
    public boolean isArray() {
        return persister.isArray();
    }

    @Override
    public boolean isOneToMany() {
        return persister.isOneToMany();
    }

    @Override
    public boolean hasIndex() {
        return persister.hasIndex();
    }

    @Override
    public boolean isLazy() {
        return persister.isLazy();
    }

    @Override
    public boolean isInverse() {
        return persister.isInverse();
    }

    @Override
    public void remove(final Serializable id, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("remove not implemented");
    }

    @Override
    public void recreate(final PersistentCollection collection, final Serializable key, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("recreate not implemented");
    }

    @Override
    public void deleteRows(final PersistentCollection collection, final Serializable key, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("deleteRows not implemented");
    }

    @Override
    public void updateRows(final PersistentCollection collection, final Serializable key, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("updateRows not implemented");
    }

    @Override
    public void insertRows(final PersistentCollection collection, final Serializable key, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("insertRows not implemented");
    }

    @Override
    public String getRole() {
        return persister.getRole();
    }

    @Override
    public Class getOwnerClass() {
        return persister.getOwnerEntityPersister().getMappedClass();
    }

    @Override
    public IdentifierGenerator getIdentifierGenerator() {
        return IdentifierGeneratorV2Adapter.adapt(
                persister.getIdentifierGenerator(),
                (org.hibernate.engine.spi.SessionFactoryImplementor) sessionFactory);
    }

    @Override
    public Type getIdentifierType() {
        return TypeV2Adapter.adapt(persister.getIdentifierType());
    }

    @Override
    public boolean hasOrphanDelete() {
        return persister.hasOrphanDelete();
    }

    @Override
    public boolean hasOrdering() {
        return persister.hasOrdering();
    }

    @Override
    public Serializable getCollectionSpace() {
        throw new NotImplementedException("getCollectionSpace not implemented");
    }

    @Override
    public CollectionMetadata getCollectionMetadata() {
        return CollectionMetadataV2Adapter.adapt(persister.getCollectionMetadata(), sessionFactory);
    }
}
