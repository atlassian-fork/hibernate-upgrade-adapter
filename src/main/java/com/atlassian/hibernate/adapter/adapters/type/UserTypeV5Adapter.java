package com.atlassian.hibernate.adapter.adapters.type;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.util.ThrowableUtil;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * An adapter bridging the UserType interface for hibernate v2 to v5 (onwards).
 */
public final class UserTypeV5Adapter implements UserType, Serializable {
    private transient net.sf.hibernate.UserType type;
    private final Object serializableTypeOrClassName;

    protected UserTypeV5Adapter(final net.sf.hibernate.UserType type) {
        this.type = type;
        this.serializableTypeOrClassName = type instanceof Serializable ? type : type.getClass();
    }

    public static UserType adapt(final net.sf.hibernate.UserType type) {
        if (type == null)
            return null;
        return new UserTypeV5Adapter(type);
    }

    @Override
    public boolean equals(final Object obj) {
        return
                this == obj
                        || (obj instanceof UserTypeV5Adapter && getV2UserType().equals(((UserTypeV5Adapter) obj).getV2UserType()));
    }

    @Override
    public int hashCode() {
        return getV2UserType().hashCode();
    }

    @SuppressWarnings("unchecked")
    public net.sf.hibernate.UserType getV2UserType() {
        if (type != null) {
            return type;
        }

        if (serializableTypeOrClassName instanceof net.sf.hibernate.UserType) {
            type = (net.sf.hibernate.UserType) serializableTypeOrClassName;
            return type;
        }

        try {
            type = (net.sf.hibernate.UserType) ((Class) serializableTypeOrClassName).getConstructor().newInstance();
        } catch (final ReflectiveOperationException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
        return type;
    }

    @Override
    public int[] sqlTypes() {
        return getV2UserType().sqlTypes();
    }

    @Override
    public Class returnedClass() {
        return getV2UserType().returnedClass();
    }

    @Override
    public boolean equals(final Object x, final Object y) throws HibernateException {
        try {
            return getV2UserType().equals(x, y);
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public int hashCode(final Object x) throws HibernateException {
        return x == null ? 0 : x.hashCode();
    }

    @Override
    public Object nullSafeGet(
            final ResultSet rs,
            final String[] names,
            final SharedSessionContractImplementor session,
            final Object owner) throws HibernateException, SQLException {

        try {
            return getV2UserType().nullSafeGet(rs, names, owner);
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void nullSafeSet(
            final PreparedStatement st,
            final Object value,
            final int index,
            final SharedSessionContractImplementor session) throws HibernateException, SQLException {

        // When calling Query.setParameter with a String instead of the object's property type,
        // Hibernate v5 passes the String here verbatim, which isn't what a v2 UserType implementation expects.
        // For example, JournalIdentifierUserType used from HibernateJournalDao.findEntries() has this problem.
        if (value instanceof String && !this.returnedClass().isInstance(value)) {
            st.setString(index, (String) value);
            return;
        }

        try {
            getV2UserType().nullSafeSet(st, value, index);
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object deepCopy(final Object value) throws HibernateException {
        try {
            return getV2UserType().deepCopy(value);
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean isMutable() {
        return getV2UserType().isMutable();
    }

    @Override
    public Serializable disassemble(final Object value) throws HibernateException {
        return value == null ? null : (Serializable) deepCopy(value);
    }

    @Override
    public Object assemble(final Serializable cached, final Object owner) throws HibernateException {
        return cached == null ? null : deepCopy(cached);
    }

    @Override
    public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
        return original;
    }
}
