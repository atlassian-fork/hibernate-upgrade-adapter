package com.atlassian.hibernate.adapter.adapters;

import org.hibernate.LockMode;

/**
 * An adapter bridging the Interceptor interface between hibernate v2 / v5 (onwards).
 */
public final class LockModeAdapter {
    private LockModeAdapter() { }

    @SuppressWarnings("deprecation")
    public static net.sf.hibernate.LockMode adapt(final org.hibernate.LockMode value) {
        if (value == org.hibernate.LockMode.NONE)
            return net.sf.hibernate.LockMode.NONE;
        if (value == org.hibernate.LockMode.READ)
            return net.sf.hibernate.LockMode.READ;
        if (value == org.hibernate.LockMode.UPGRADE || value == LockMode.PESSIMISTIC_WRITE)
            return net.sf.hibernate.LockMode.UPGRADE;
        if (value == org.hibernate.LockMode.UPGRADE_NOWAIT)
            return net.sf.hibernate.LockMode.UPGRADE_NOWAIT;
        if (value == org.hibernate.LockMode.WRITE)
            return net.sf.hibernate.LockMode.WRITE;
        throw new IllegalArgumentException("Unsupported org.hibernate.LockMode value: " + value);
    }

    public static org.hibernate.LockMode adapt(final net.sf.hibernate.LockMode value) {
        if (value == net.sf.hibernate.LockMode.NONE)
            return org.hibernate.LockMode.NONE;
        if (value == net.sf.hibernate.LockMode.READ)
            return org.hibernate.LockMode.READ;
        if (value == net.sf.hibernate.LockMode.UPGRADE)
            return org.hibernate.LockMode.PESSIMISTIC_WRITE;
        if (value == net.sf.hibernate.LockMode.UPGRADE_NOWAIT)
            return org.hibernate.LockMode.UPGRADE_NOWAIT;
        if (value == net.sf.hibernate.LockMode.WRITE)
            return org.hibernate.LockMode.WRITE;
        throw new IllegalArgumentException("Unsupported net.sf.hibernate.LockMode value: " + value);
    }
}
