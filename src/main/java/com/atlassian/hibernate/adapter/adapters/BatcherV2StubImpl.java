package com.atlassian.hibernate.adapter.adapters;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.ScrollMode;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.engine.Batcher;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.util.JDBCExceptionReporter;
import org.apache.commons.lang3.NotImplementedException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A net.sf.hibernate.engine.Batcher that supports openConnection / closeConnection operations.
 */
public class BatcherV2StubImpl implements Batcher {
    private final SessionFactoryImplementor sessionFactory;

    public BatcherV2StubImpl(final SessionFactory sessionFactory) {
        this.sessionFactory = (SessionFactoryImplementor) sessionFactory;
    }

    @Override
    public Connection openConnection() throws HibernateException {
        try {
            return sessionFactory.getConnectionProvider().getConnection();
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV2HibernateException(sessionFactory, ex, "Cannot open connection");
        }
    }

    @Override
    public void closeConnection(final Connection conn) throws HibernateException {
        try {
            if (!conn.isClosed()) {
                JDBCExceptionReporter.logWarnings(conn.getWarnings());
                conn.clearWarnings();
            }
            sessionFactory.getConnectionProvider().closeConnection(conn);
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV2HibernateException(sessionFactory, ex, "Cannot close connection");
        }
    }

    @Override
    public PreparedStatement prepareQueryStatement(final String sql, final boolean scrollable, final ScrollMode scrollMode)
            throws SQLException, HibernateException {
        throw new NotImplementedException("prepareQueryStatement not implemented");
    }

    @Override
    public void closeQueryStatement(final PreparedStatement ps, final ResultSet rs) throws SQLException {
        throw new NotImplementedException("closeQueryStatement not implemented");
    }

    @Override
    public PreparedStatement prepareStatement(final String sql, final boolean useGetGeneratedKeys)
            throws SQLException, HibernateException {
        throw new NotImplementedException("prepareStatement not implemented");
    }

    @Override
    public PreparedStatement prepareStatement(final String sql) throws SQLException, HibernateException {
        throw new NotImplementedException("prepareStatement not implemented");
    }

    @Override
    public void closeStatement(final PreparedStatement ps) throws SQLException {
        throw new NotImplementedException("closeStatement not implemented");
    }

    @Override
    public PreparedStatement prepareBatchStatement(final String sql) throws SQLException, HibernateException {
        throw new NotImplementedException("prepareBatchStatement not implemented");
    }

    @Override
    public void addToBatch(final int expectedRowCount) throws SQLException, HibernateException {
        throw new NotImplementedException("addToBatch not implemented");
    }

    @Override
    public void executeBatch() throws HibernateException {
        throw new NotImplementedException("executeBatch not implemented");
    }

    @Override
    public void closeStatements() {
        throw new NotImplementedException("closeStatements not implemented");
    }

    @Override
    public ResultSet getResultSet(final PreparedStatement ps) throws SQLException {
        throw new NotImplementedException("getResultSet not implemented");
    }

    @Override
    public void abortBatch(final SQLException sqle) {
        throw new NotImplementedException("abortBatch not implemented");
    }

    @Override
    public void cancelLastQuery() throws HibernateException {
        throw new NotImplementedException("cancelLastQuery not implemented");
    }
}
