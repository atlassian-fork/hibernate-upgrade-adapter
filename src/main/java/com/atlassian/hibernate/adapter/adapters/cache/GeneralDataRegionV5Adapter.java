package com.atlassian.hibernate.adapter.adapters.cache;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.GeneralDataRegion;
import org.hibernate.engine.spi.SharedSessionContractImplementor;

/**
 * An adapter bridging the GeneralDataRegion interface for hibernate v2 to v5 (onwards).
 */
public abstract class GeneralDataRegionV5Adapter extends RegionV5Adapter implements GeneralDataRegion {

    protected GeneralDataRegionV5Adapter(
            final String regionName,
            final net.sf.hibernate.cache.Cache underlyingCache) {

        super(regionName, underlyingCache);
    }

    @Override
    public Object get(final SharedSessionContractImplementor session, final Object key) throws CacheException {
        try {
            return getUnderlyingCache().get(key);
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void put(final SharedSessionContractImplementor session, final Object key, final Object value) throws CacheException {
        try {
            getUnderlyingCache().put(key, value);
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void evict(final Object key) throws CacheException {
        try {
            getUnderlyingCache().remove(key);
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void evictAll() throws CacheException {
        try {
            getUnderlyingCache().clear();
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }
}
