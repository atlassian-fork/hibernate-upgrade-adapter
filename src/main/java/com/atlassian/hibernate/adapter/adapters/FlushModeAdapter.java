package com.atlassian.hibernate.adapter.adapters;

/**
 * An adapter bridging the FlushMode enum between hibernate v2 / v5 (onwards).
 */
public final class FlushModeAdapter {
    private FlushModeAdapter() {
    }

    public static net.sf.hibernate.FlushMode adapt(final org.hibernate.FlushMode value) {
        if (value == null)
            return null;
        if (value == org.hibernate.FlushMode.MANUAL)
            return net.sf.hibernate.FlushMode.NEVER;
        if (value == org.hibernate.FlushMode.COMMIT)
            return net.sf.hibernate.FlushMode.COMMIT;
        if (value == org.hibernate.FlushMode.AUTO)
            return net.sf.hibernate.FlushMode.AUTO;
        if (value == org.hibernate.FlushMode.ALWAYS)
            return net.sf.hibernate.FlushMode.AUTO;
        throw new IllegalArgumentException("Unsupported org.hibernate.FlushMode value: " + value);
    }

    public static org.hibernate.FlushMode adapt(final net.sf.hibernate.FlushMode value) {
        if (value == null)
            return null;
        if (value == net.sf.hibernate.FlushMode.NEVER)
            return org.hibernate.FlushMode.MANUAL;
        if (value == net.sf.hibernate.FlushMode.COMMIT)
            return org.hibernate.FlushMode.COMMIT;
        if (value == net.sf.hibernate.FlushMode.AUTO)
            return org.hibernate.FlushMode.AUTO;
        throw new IllegalArgumentException("Unsupported net.sf.hibernate.FlushMode value: " + value);
    }
}
