package com.atlassian.hibernate.adapter.adapters.type;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.mapping.MappingV5Adapter;
import com.atlassian.hibernate.adapter.adapters.type.collection.PersistentCollectionTypeV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.relation.ComponentTypeV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.relation.CompositeCustomTypeV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.relation.CustomTypeV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.relation.ManyToOneTypeV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.relation.OneToOneTypeV2Adapter;
import com.atlassian.hibernate.adapter.type.PersistentEnumUserType;
import com.atlassian.hibernate.adapter.type.V5TypeSupplier;
import com.atlassian.hibernate.adapter.util.ArrayUtil;
import com.atlassian.hibernate.util.ThrowableUtil;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.engine.Mapping;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.engine.SessionImplementor;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.type.CollectionType;
import org.hibernate.type.ComponentType;

import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * An adapter bridging the Type interface from hibernate v5 (onwards) to v2.
 */
public class TypeV2Adapter implements net.sf.hibernate.type.Type, V5TypeSupplier {
    private static final WeakHashMap<org.hibernate.type.Type[], net.sf.hibernate.type.Type[]> TYPE_ARRAY_CACHE = new WeakHashMap<>();
    private static final WeakHashMap<org.hibernate.type.Type, WeakReference<net.sf.hibernate.type.Type>> TYPE_CACHE = new WeakHashMap<>();

    private final org.hibernate.type.Type type;

    public TypeV2Adapter(final org.hibernate.type.Type type) {
        this.type = type;
    }

    public static net.sf.hibernate.type.Type[] adapt(final org.hibernate.type.Type[] types) {
        if (types == null) {
            return null;
        }

        synchronized (TYPE_ARRAY_CACHE) {
            net.sf.hibernate.type.Type[] result = TYPE_ARRAY_CACHE.get(types);
            if (result != null) {
                return result;
            }

            result = adaptImpl(types);
            TYPE_ARRAY_CACHE.put(types, result);
            return result;
        }
    }

    private static net.sf.hibernate.type.Type[] adaptImpl(final org.hibernate.type.Type[] types) {
        if (types == null)
            return null;
        return ArrayUtil.transformArray(types, new net.sf.hibernate.type.Type[types.length], TypeV2Adapter::adapt);
    }

    public static net.sf.hibernate.type.Type adapt(final org.hibernate.type.Type type) {
        if (type == null) {
            return null;
        }

        synchronized (TYPE_CACHE) {
            WeakReference<net.sf.hibernate.type.Type> ref = TYPE_CACHE.get(type);
            if (ref != null) {
                Object result = ref.get();
                if (result != null) {
                    return (net.sf.hibernate.type.Type) result;
                }
            }

            net.sf.hibernate.type.Type result = adaptImpl(type);
            TYPE_CACHE.put(type, new WeakReference<>(result));
            return result;
        }
    }

    private static net.sf.hibernate.type.Type adaptImpl(final org.hibernate.type.Type type) {
        if (type instanceof org.hibernate.type.StringType)
            return net.sf.hibernate.Hibernate.STRING;
        if (type instanceof org.hibernate.type.TextType)
            return net.sf.hibernate.Hibernate.TEXT;
        if (type instanceof org.hibernate.type.IntegerType)
            return net.sf.hibernate.Hibernate.INTEGER;
        if (type instanceof org.hibernate.type.LongType)
            return net.sf.hibernate.Hibernate.LONG;
        if (type instanceof org.hibernate.type.BigDecimalType)
            return net.sf.hibernate.Hibernate.BIG_DECIMAL;
        if (type instanceof org.hibernate.type.BinaryType)
            return net.sf.hibernate.Hibernate.BINARY;
        if (type instanceof org.hibernate.type.BlobType)
            return net.sf.hibernate.Hibernate.BLOB;
        if (type instanceof org.hibernate.type.ByteType)
            return net.sf.hibernate.Hibernate.BYTE;
        if (type instanceof org.hibernate.type.CalendarDateType)
            return net.sf.hibernate.Hibernate.CALENDAR_DATE;
        if (type instanceof org.hibernate.type.CalendarType)
            return net.sf.hibernate.Hibernate.CALENDAR;
        if (type instanceof org.hibernate.type.CharacterType)
            return net.sf.hibernate.Hibernate.CHARACTER;
        if (type instanceof org.hibernate.type.ClassType)
            return net.sf.hibernate.Hibernate.CLASS;
        if (type instanceof org.hibernate.type.ClobType)
            return net.sf.hibernate.Hibernate.CLOB;
        if (type instanceof org.hibernate.type.CurrencyType)
            return net.sf.hibernate.Hibernate.CURRENCY;
        if (type instanceof org.hibernate.type.DateType)
            return net.sf.hibernate.Hibernate.DATE;
        if (type instanceof org.hibernate.type.DoubleType)
            return net.sf.hibernate.Hibernate.DOUBLE;
        if (type instanceof org.hibernate.type.FloatType)
            return net.sf.hibernate.Hibernate.FLOAT;
        if (type instanceof org.hibernate.type.LocaleType)
            return net.sf.hibernate.Hibernate.LOCALE;
        if (type instanceof org.hibernate.type.ObjectType)
            return net.sf.hibernate.Hibernate.OBJECT;
        if (type instanceof org.hibernate.type.SerializableType)
            return net.sf.hibernate.Hibernate.SERIALIZABLE;
        if (type instanceof org.hibernate.type.ShortType)
            return net.sf.hibernate.Hibernate.SHORT;
        if (type instanceof org.hibernate.type.TimeType)
            return net.sf.hibernate.Hibernate.TIME;
        if (type instanceof org.hibernate.type.TimeZoneType)
            return net.sf.hibernate.Hibernate.TIMEZONE;
        if (type instanceof org.hibernate.type.DbTimestampType)
            return new TypeV2Adapter(type);
        if (type instanceof org.hibernate.type.TimestampType)
            return net.sf.hibernate.Hibernate.TIMESTAMP;
        if (type instanceof org.hibernate.type.TrueFalseType)
            return net.sf.hibernate.Hibernate.TRUE_FALSE;
        if (type instanceof org.hibernate.type.YesNoType)
            return net.sf.hibernate.Hibernate.YES_NO;
        if (type instanceof org.hibernate.type.NumericBooleanType)
            return net.sf.hibernate.Hibernate.BOOLEAN;
        if (type instanceof org.hibernate.type.BooleanType)
            return net.sf.hibernate.Hibernate.BOOLEAN;

        if (type instanceof org.hibernate.type.OneToOneType)
            return OneToOneTypeV2Adapter.adapt((org.hibernate.type.OneToOneType) type);
        if (type instanceof org.hibernate.type.ManyToOneType)
            return ManyToOneTypeV2Adapter.adapt((org.hibernate.type.ManyToOneType) type);
        if (type instanceof org.hibernate.type.CollectionType)
            return PersistentCollectionTypeV2Adapter.adapt(
                    (org.hibernate.type.CollectionType) type);
        if (type instanceof org.hibernate.type.CustomType
                && ((org.hibernate.type.CustomType) type).getUserType() instanceof PersistentEnumUserType)
            return createPersistentEnumType((PersistentEnumUserType) ((org.hibernate.type.CustomType) type).getUserType());

        if (type instanceof org.hibernate.type.ComponentType)
            return ComponentTypeV2Adapter.adapt((org.hibernate.type.ComponentType) type);
        if (type instanceof org.hibernate.type.CompositeCustomType)
            return CompositeCustomTypeV2Adapter.adapt((org.hibernate.type.CompositeCustomType) type);
        if (type instanceof org.hibernate.type.CustomType)
            return CustomTypeV2Adapter.adapt((org.hibernate.type.CustomType) type);
        if (type instanceof org.hibernate.type.MetaType)
            return MetaTypeV2Adapter.adapt((org.hibernate.type.MetaType) type);

        if (type instanceof org.hibernate.type.LiteralType)
            return new LiteralTypeV2Adapter((org.hibernate.type.LiteralType) type);
        return new TypeV2Adapter(type);
    }

    @SuppressWarnings("deprecation")
    private static net.sf.hibernate.type.PersistentEnumType createPersistentEnumType(final PersistentEnumUserType type) {
        try {
            return new net.sf.hibernate.type.PersistentEnumType(type.returnedClass());
        } catch (final MappingException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public org.hibernate.type.Type getV5Type() {
        return type;
    }

    @Override
    public boolean equals(final Object object) {
        return
                object instanceof V5TypeSupplier
                && type.equals(((V5TypeSupplier) object).getV5Type());
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    @Override
    public String toString() {
        return type.toString();
    }

    @Override
    public boolean isAssociationType() {
        return type.isAssociationType();
    }

    @Override
    public boolean isComponentType() {
        return type.isComponentType();
    }

    @Override
    public boolean isEntityType() {
        return type.isEntityType();
    }

    @Override
    public Class getReturnedClass() {
        return type.getReturnedClass();
    }

    @Override
    public String getName() {
        if (type instanceof ComponentType || type instanceof CollectionType) {
            return type.getReturnedClass().getName();
        }
        return type.getName();
    }

    @Override
    public boolean isMutable() {
        return type.isMutable();
    }

    @Override
    public boolean isPersistentCollectionType() {
        return type.isCollectionType();
    }

    @Override
    public boolean isObjectType() {
        throw new NotImplementedException("isObjectType not implemented");
    }

    @Override
    public int[] sqlTypes(final Mapping mapping) throws MappingException {
        return type.sqlTypes(MappingV5Adapter.adapt(mapping));
    }

    @Override
    public int getColumnSpan(final Mapping mapping) {
        throw new NotImplementedException("sqlTypes not implemented");
    }

    @Override
    public boolean equals(final Object x, final Object y) throws HibernateException {
        try {
            return type.isEqual(x, y);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean isDirty(final Object old, final Object current, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("sqlTypes not implemented");
    }

    @Override
    public boolean isModified(final Object oldHydratedState, final Object currentState, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("sqlTypes not implemented");
    }

    @Override
    public Object nullSafeGet(final ResultSet rs, final String[] names, final SessionImplementor session, final Object owner) throws HibernateException, SQLException {
        throw new NotImplementedException("sqlTypes not implemented");
    }

    @Override
    public Object nullSafeGet(final ResultSet rs, final String name, final SessionImplementor session, final Object owner) throws HibernateException, SQLException {
        throw new NotImplementedException("nullSafeGet(ResultSet, String, SessionImplementor, Object) not implemented");
    }

    @Override
    public void nullSafeSet(final PreparedStatement st, final Object value, final int index, final SessionImplementor session) throws HibernateException, SQLException {
        throw new NotImplementedException("nullSafeGet(PreparedStatement, Object, int, SessionImplementor) not implemented");
    }

    @Override
    @SuppressWarnings("unchecked")
    public String toString(final Object value, final SessionFactoryImplementor factory) {
        if (type instanceof org.hibernate.type.StringRepresentableType)
            return ((org.hibernate.type.StringRepresentableType) type).toString(value);
        throw new UnsupportedOperationException("toString not implemented for a " + type.getName());
    }

    @Override
    public Object fromString(final String xml) {
        if (type instanceof org.hibernate.type.StringRepresentableType)
            return ((org.hibernate.type.StringRepresentableType) type).fromStringValue(xml);
        throw new UnsupportedOperationException("fromString not implemented for a " + type.getName());
    }

    @Override
    public Object deepCopy(final Object value) {
        throw new NotImplementedException("deepCopy not implemented");
    }

    @Override
    public Serializable disassemble(final Object value, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("disassemble not implemented");
    }

    @Override
    public Object assemble(final Serializable cached, final SessionImplementor session, final Object owner) throws HibernateException {
        throw new NotImplementedException("assemble not implemented");
    }

    @Override
    @Deprecated
    public boolean hasNiceEquals() {
        throw new NotImplementedException("hasNiceEquals not implemented");
    }

    @Override
    public Object hydrate(final ResultSet rs, final String[] names, final SessionImplementor session, final Object owner) throws HibernateException, SQLException {
        throw new NotImplementedException("hydrate not implemented");
    }

    @Override
    public Object resolveIdentifier(final Object value, final SessionImplementor session, final Object owner) throws HibernateException {
        throw new NotImplementedException("resolveIdentifier not implemented");
    }

    @Override
    public Object copy(final Object original, final Object target, final SessionImplementor session, final Object owner, final Map copiedAlready) throws HibernateException {
        throw new NotImplementedException("copy not implemented");
    }
}
