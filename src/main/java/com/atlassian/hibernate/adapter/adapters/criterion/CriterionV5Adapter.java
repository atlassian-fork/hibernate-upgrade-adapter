package com.atlassian.hibernate.adapter.adapters.criterion;

import com.atlassian.hibernate.adapter.adapters.type.TypeV5Adapter;
import com.atlassian.hibernate.adapter.reflection.JunctionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.BetweenExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.ExampleV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.IlikeExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.InExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.LogicalExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.NotExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.NotNullExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.NullExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.PropertyExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.SQLCriterionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.SimpleExpressionV2Reflection;
import com.atlassian.hibernate.adapter.util.ArrayUtil;
import net.sf.hibernate.engine.TypedValue;
import net.sf.hibernate.expression.AndExpression;
import net.sf.hibernate.expression.BetweenExpression;
import net.sf.hibernate.expression.Conjunction;
import net.sf.hibernate.expression.Criterion;
import net.sf.hibernate.expression.Disjunction;
import net.sf.hibernate.expression.EqExpression;
import net.sf.hibernate.expression.EqPropertyExpression;
import net.sf.hibernate.expression.Example;
import net.sf.hibernate.expression.GeExpression;
import net.sf.hibernate.expression.GtExpression;
import net.sf.hibernate.expression.IlikeExpression;
import net.sf.hibernate.expression.InExpression;
import net.sf.hibernate.expression.Junction;
import net.sf.hibernate.expression.LeExpression;
import net.sf.hibernate.expression.LePropertyExpression;
import net.sf.hibernate.expression.LikeExpression;
import net.sf.hibernate.expression.LogicalExpression;
import net.sf.hibernate.expression.LtExpression;
import net.sf.hibernate.expression.LtPropertyExpression;
import net.sf.hibernate.expression.NotExpression;
import net.sf.hibernate.expression.NotNullExpression;
import net.sf.hibernate.expression.NullExpression;
import net.sf.hibernate.expression.OrExpression;
import net.sf.hibernate.expression.PropertyExpression;
import net.sf.hibernate.expression.SQLCriterion;
import net.sf.hibernate.expression.SimpleExpression;

import java.util.List;

/**
 * An adapter bridging the Criterion interface for hibernate v2 to v5 (onwards).
 */
public final class CriterionV5Adapter {
    private CriterionV5Adapter() {
    }

    public static org.hibernate.criterion.Criterion adapt(
            final org.hibernate.SessionFactory sessionFactory,
            final Criterion criterion) {

        if (criterion == null)
            return null;

        if (criterion instanceof LogicalExpression)
            return adaptLogicalExpr(sessionFactory, (LogicalExpression) criterion);
        if (criterion instanceof SimpleExpression)
            return adaptSimpleExpr((SimpleExpression) criterion);
        if (criterion instanceof PropertyExpression)
            return adaptPropertyExpr((PropertyExpression) criterion);
        if (criterion instanceof Junction)
            return adaptJunction(sessionFactory, (Junction) criterion);
        if (criterion instanceof SQLCriterion)
            return adaptSQLCriterion(sessionFactory, (SQLCriterion) criterion);
        if (criterion instanceof Example)
            return adaptExample((Example) criterion);

        if (criterion instanceof BetweenExpression) {
            final BetweenExpression expr = (BetweenExpression) criterion;
            return org.hibernate.criterion.Restrictions.between(
                    BetweenExpressionV2Reflection.getPropertyName(expr),
                    BetweenExpressionV2Reflection.getLo(expr),
                    BetweenExpressionV2Reflection.getHi(expr));
        }
        if (criterion instanceof IlikeExpression) {
            final IlikeExpression expr = (IlikeExpression) criterion;
            return org.hibernate.criterion.Restrictions.ilike(
                    IlikeExpressionV2Reflection.getPropertyName(expr),
                    IlikeExpressionV2Reflection.getValue(expr));
        }
        if (criterion instanceof InExpression) {
            final InExpression expr = (InExpression) criterion;
            return org.hibernate.criterion.Restrictions.in(
                    InExpressionV2Reflection.getPropertyName(expr),
                    InExpressionV2Reflection.getValues(expr));
        }
        if (criterion instanceof NotExpression) {
            return org.hibernate.criterion.Restrictions.not(
                    adapt(sessionFactory, NotExpressionV2Reflection.getCriterion((NotExpression) criterion)));
        }
        if (criterion instanceof NullExpression) {
            final NullExpression expr = (NullExpression) criterion;
            return org.hibernate.criterion.Restrictions.isNull(
                    NullExpressionV2Reflection.getPropertyName(expr));
        }
        if (criterion instanceof NotNullExpression) {
            final NotNullExpression expr = (NotNullExpression) criterion;
            return org.hibernate.criterion.Restrictions.isNotNull(
                    NotNullExpressionV2Reflection.getPropertyName(expr));
        }
        throw new IllegalArgumentException("Unexpected Criterion type: " + criterion.getClass().getName());
    }

    private static org.hibernate.criterion.Criterion adaptLogicalExpr(
            final org.hibernate.SessionFactory sessionFactory, final LogicalExpression expr) {

        final org.hibernate.criterion.Criterion lhs =
                adapt(sessionFactory, LogicalExpressionV2Reflection.getLhs(expr));
        final org.hibernate.criterion.Criterion rhs =
                adapt(sessionFactory, LogicalExpressionV2Reflection.getRhs(expr));

        if (expr instanceof AndExpression)
            return org.hibernate.criterion.Restrictions.and(lhs, rhs);
        if (expr instanceof OrExpression)
            return org.hibernate.criterion.Restrictions.or(lhs, rhs);

        throw new IllegalArgumentException("Unexpected LogicalExpression criterion: " + expr.getClass().getName());
    }

    private static org.hibernate.criterion.Criterion adaptSimpleExpr(final SimpleExpression criterion) {
        final String propertyName = SimpleExpressionV2Reflection.getPropertyName(criterion);
        final Object value = SimpleExpressionV2Reflection.getValue(criterion);
        final boolean ignoreCase = SimpleExpressionV2Reflection.getIgnoreCase(criterion);

        if (criterion instanceof EqExpression)
            return setIgnoreCase(org.hibernate.criterion.Restrictions.eq(propertyName, value), ignoreCase);
        if (criterion instanceof LtExpression)
            return setIgnoreCase(org.hibernate.criterion.Restrictions.lt(propertyName, value), ignoreCase);
        if (criterion instanceof LeExpression)
            return setIgnoreCase(org.hibernate.criterion.Restrictions.le(propertyName, value), ignoreCase);
        if (criterion instanceof GtExpression)
            return setIgnoreCase(org.hibernate.criterion.Restrictions.gt(propertyName, value), ignoreCase);
        if (criterion instanceof GeExpression)
            return setIgnoreCase(org.hibernate.criterion.Restrictions.ge(propertyName, value), ignoreCase);
        if (criterion instanceof LikeExpression)
            return setIgnoreCase(org.hibernate.criterion.Restrictions.like(propertyName, value), ignoreCase);

        throw new IllegalArgumentException("Unexpected SimpleExpression criterion: " + criterion.getClass().getName());
    }

    private static org.hibernate.criterion.Criterion adaptPropertyExpr(final PropertyExpression criterion) {
        final String propertyName = PropertyExpressionV2Reflection.getPropertyName(criterion);
        final String otherPropertyName = PropertyExpressionV2Reflection.getOtherPropertyName(criterion);

        if (criterion instanceof EqPropertyExpression)
            return org.hibernate.criterion.Restrictions.eqProperty(propertyName, otherPropertyName);
        if (criterion instanceof LtPropertyExpression)
            return org.hibernate.criterion.Restrictions.ltProperty(propertyName, otherPropertyName);
        if (criterion instanceof LePropertyExpression)
            return org.hibernate.criterion.Restrictions.leProperty(propertyName, otherPropertyName);

        throw new IllegalArgumentException("Unexpected PropertyExpression criterion: " + criterion.getClass().getName());
    }

    private static org.hibernate.criterion.Criterion adaptJunction(
            final org.hibernate.SessionFactory sessionFactory,
            final Junction expr) {

        final org.hibernate.criterion.Criterion[] criteria = toCriterionArray(
                sessionFactory, JunctionV2Reflection.getCriteria(expr));

        if (expr instanceof Conjunction)
            return org.hibernate.criterion.Restrictions.conjunction(criteria);
        if (expr instanceof Disjunction)
            return org.hibernate.criterion.Restrictions.disjunction(criteria);

        throw new IllegalArgumentException("Unexpected Junction criterion: " + expr.getClass().getName());
    }

    private static org.hibernate.criterion.Criterion adaptSQLCriterion(
            final org.hibernate.SessionFactory sessionFactory, final SQLCriterion criterion) {
        final String sql = SQLCriterionV2Reflection.getSql(criterion);
        final TypedValue[] typedValues = SQLCriterionV2Reflection.getTypedValues(criterion);
        final Object[] values = ArrayUtil.transformArray(
                typedValues,
                new Object[typedValues.length],
                TypedValue::getValue);
        final org.hibernate.type.Type[] types = ArrayUtil.transformArray(
                typedValues,
                new org.hibernate.type.Type[typedValues.length],
                typedValue -> TypeV5Adapter.adapt(sessionFactory, typedValue.getType()));
        return org.hibernate.criterion.Restrictions.sqlRestriction(sql, values, types);
    }

    private static org.hibernate.criterion.Example adaptExample(final Example expr) {
        final Object entity = ExampleV2Reflection.getEntity(expr);
        final org.hibernate.criterion.Example.PropertySelector selector =
                ExamplePropertySelectorV2Adapter.adapt(ExampleV2Reflection.getSelector(expr));

        final org.hibernate.criterion.Example example = org.hibernate.criterion.Example.create(entity);
        example.setPropertySelector(selector);
        for (final Object propertyName : ExampleV2Reflection.getExcludedProperties(expr)) {
            example.excludeProperty((String) propertyName);
        }
        if (ExampleV2Reflection.getIsLikeEnabled(expr)) {
            example.enableLike(MatchModeV2Adapter.adapt(ExampleV2Reflection.getMatchMode(expr)));
        }
        return example;
    }

    private static org.hibernate.criterion.Criterion[] toCriterionArray(
            final org.hibernate.SessionFactory sessionFactory,
            final List<Criterion> list) {

        final org.hibernate.criterion.Criterion[] result =
                new org.hibernate.criterion.Criterion[list.size()];

        for (int i = 0; i < list.size(); i++) {
            result[i] = adapt(sessionFactory, list.get(i));
        }
        return result;
    }

    private static org.hibernate.criterion.SimpleExpression setIgnoreCase(
            final org.hibernate.criterion.SimpleExpression expr,
            final boolean ignoreCase) {

        return ignoreCase ? expr.ignoreCase() : expr;
    }
}
