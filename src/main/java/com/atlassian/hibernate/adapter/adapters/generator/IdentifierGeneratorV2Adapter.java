package com.atlassian.hibernate.adapter.adapters.generator;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.id.IdentifierGenerator;
import net.sf.hibernate.id.IdentifierGeneratorFactory;

import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.sql.SQLException;

/**
 * An adapter bridging the IdentifierGenerator interface for hibernate v5 (onwards) to v2.
 */
public class IdentifierGeneratorV2Adapter implements IdentifierGenerator {
    private final org.hibernate.id.IdentifierGenerator generator;

    protected IdentifierGeneratorV2Adapter(final org.hibernate.id.IdentifierGenerator generator) {
        this.generator = generator;
    }

    public static IdentifierGenerator adapt(
            final org.hibernate.id.IdentifierGenerator generator,
            final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactory) {

        if (generator == null)
            return null;
        if (generator instanceof IdentifierGenerator)
            return (IdentifierGenerator) generator;

        if (generator instanceof IdentifierGeneratorV5Adapter)
            return ((IdentifierGeneratorV5Adapter) generator).getV2IdentifierGenerator();
        // 'instanceof Assigned' is used in confluence code
        if (generator instanceof org.hibernate.id.Assigned)
            return new net.sf.hibernate.id.Assigned();
        if (generator instanceof org.hibernate.id.PersistentIdentifierGenerator)
            return new PersistentIdentifierGeneratorV2Adapter(
                    (org.hibernate.id.PersistentIdentifierGenerator) generator,
                    sessionFactory);

        return new IdentifierGeneratorV2Adapter(generator);
    }

    @Override
    public Serializable generate(final SessionImplementor session, final Object object)
            throws SQLException, HibernateException {
        try {
            final org.hibernate.Session sessionV5 = HibernateBridge.get(session.getFactory()).getV5Session(session);
            final Serializable result = generator.generate((org.hibernate.engine.spi.SessionImplementor) sessionV5, object);

            if (result == org.hibernate.id.IdentifierGeneratorHelper.POST_INSERT_INDICATOR)
                return IdentifierGeneratorFactory.IDENTITY_COLUMN_INDICATOR;
            if (result == org.hibernate.id.IdentifierGeneratorHelper.SHORT_CIRCUIT_INDICATOR)
                return IdentifierGeneratorFactory.SHORT_CIRCUIT_INDICATOR;
            return result;
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }
}
