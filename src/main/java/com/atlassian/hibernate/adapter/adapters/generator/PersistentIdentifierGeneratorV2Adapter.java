package com.atlassian.hibernate.adapter.adapters.generator;

import com.atlassian.hibernate.adapter.adapters.DialectAdapter;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV5Adapter;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.dialect.Dialect;
import net.sf.hibernate.id.Configurable;
import net.sf.hibernate.id.PersistentIdentifierGenerator;
import net.sf.hibernate.type.Type;
import org.apache.commons.lang3.NotImplementedException;

import javax.persistence.PersistenceException;
import java.util.Properties;

/**
 * An adapter bridging the PersistentIdentifierGenerator / Configurable interfaces
 * for hibernate v5 (onwards) to v2.
 */
public class PersistentIdentifierGeneratorV2Adapter
        extends IdentifierGeneratorV2Adapter
        implements PersistentIdentifierGenerator, Configurable {

    private final org.hibernate.id.PersistentIdentifierGenerator generator;
    private final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactory;

    PersistentIdentifierGeneratorV2Adapter(
            final org.hibernate.id.PersistentIdentifierGenerator generator,
            final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactory) {

        super(generator);
        this.generator = generator;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void configure(final Type type, final Properties params, final Dialect d) throws MappingException {
        if (!(generator instanceof org.hibernate.id.Configurable)) {
            throw new NotImplementedException("org.hibernate.id.Configurable not implemented on " + generator.getClass().getName());
        }
        try {
            ((org.hibernate.id.Configurable) generator).configure(
                    TypeV5Adapter.adapt(sessionFactory, type),
                    params,
                    sessionFactory.getServiceRegistry());
        } catch (final PersistenceException ex) {
            throw (MappingException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public String[] sqlCreateStrings(final Dialect dialect) throws HibernateException {
        try {
            return generator.sqlCreateStrings(DialectAdapter.adapt(dialect));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public String sqlDropString(final Dialect dialect) throws HibernateException {
        try {
            final String[] array = generator.sqlDropStrings(DialectAdapter.adapt(dialect));
            return array != null && array.length != 0 ? array[0] : null;
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object generatorKey() {
        return generator.generatorKey();
    }
}
