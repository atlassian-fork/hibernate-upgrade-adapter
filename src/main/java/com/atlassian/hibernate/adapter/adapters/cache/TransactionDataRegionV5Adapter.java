package com.atlassian.hibernate.adapter.adapters.cache;

import net.sf.hibernate.cache.CacheConcurrencyStrategy;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.CacheDataDescription;
import org.hibernate.cache.spi.TransactionalDataRegion;
import org.hibernate.cache.spi.access.AccessType;

import java.util.function.Function;

/**
 * An adapter bridging the TransactionalDataRegion interface for hibernate v2 to v5 (onwards).
 */
public abstract class TransactionDataRegionV5Adapter
        extends RegionV5Adapter
        implements TransactionalDataRegion {

    private final CacheDataDescription metadata;
    private final Function<String, String> cacheStrategyClassSupplier;

    protected TransactionDataRegionV5Adapter(
            final String regionName,
            final net.sf.hibernate.cache.Cache underlyingCache,
            final CacheDataDescription metadata,
            final Function<String, String> cacheStrategyClassSupplier) {

        super(regionName, underlyingCache);
        this.metadata = metadata;
        this.cacheStrategyClassSupplier = cacheStrategyClassSupplier;
    }

    @Override
    public boolean isTransactionAware() {
        return false;
    }

    @Override
    public CacheDataDescription getCacheDataDescription() {
        return metadata;
    }

    protected net.sf.hibernate.cache.CacheConcurrencyStrategy buildCacheConcurrencyStategy(final AccessType accessType)
            throws CacheException {

        final String cacheStrategyClassName = cacheStrategyClassSupplier.apply(getName());
        if (cacheStrategyClassName != null) {
            final net.sf.hibernate.cache.CacheConcurrencyStrategy strategy =
                    buildCacheConcurrencyStategy(cacheStrategyClassName);
            strategy.setCache(getUnderlyingCache());
            return strategy;
        }

        final net.sf.hibernate.cache.CacheConcurrencyStrategy strategy;
        switch (accessType) {
            case READ_ONLY:
                strategy = new net.sf.hibernate.cache.ReadOnlyCache();
                break;
            case READ_WRITE:
                strategy = new net.sf.hibernate.cache.ReadWriteCache();
                break;
            case NONSTRICT_READ_WRITE:
                strategy = new net.sf.hibernate.cache.NonstrictReadWriteCache();
                break;
            case TRANSACTIONAL:
                strategy = new net.sf.hibernate.cache.TransactionalCache();
                break;
            default:
                throw new IllegalArgumentException("Unexpected AccessType ''" + accessType + "'");
        }

        strategy.setCache(getUnderlyingCache());
        return strategy;
    }

    protected CacheConcurrencyStrategy buildCacheConcurrencyStategy(
            final String cacheStrategyClassName) throws CacheException {

        try {
            return (net.sf.hibernate.cache.CacheConcurrencyStrategy)
                    net.sf.hibernate.util.ReflectHelper.classForName(cacheStrategyClassName).newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            throw new CacheException("Failed to construct CacheConcurrencyStrategy '" + cacheStrategyClassName + "'", ex);
        }
    }
}
