package com.atlassian.hibernate.adapter.adapters.interceptor;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV5Adapter;
import net.sf.hibernate.CallbackException;
import net.sf.hibernate.Interceptor;
import net.sf.hibernate.type.Type;

import java.io.Serializable;
import java.util.Iterator;

/**
 * An adapter bridging the Interceptor interface from hibernate v5 (onwards) to v2.
 */
public class InterceptorV2Adapter implements Interceptor {
    private final org.hibernate.SessionFactory sessionFactory;
    private final org.hibernate.Interceptor interceptor;

    protected InterceptorV2Adapter(
            final org.hibernate.SessionFactory sessionFactory,
            final org.hibernate.Interceptor interceptor) {

        this.sessionFactory = sessionFactory;
        this.interceptor = interceptor;
    }

    public static Interceptor adapt(
            final org.hibernate.SessionFactory sessionFactory,
            final org.hibernate.Interceptor interceptor) {

        if (interceptor == null)
            return null;
        if (interceptor instanceof InterceptorV5Adapter) {
            return ((InterceptorV5Adapter) interceptor).getInterceptor();
        }
        return new InterceptorV2Adapter(sessionFactory, interceptor);
    }

    org.hibernate.Interceptor getInterceptor() {
        return interceptor;
    }

    @Override
    public boolean onLoad(final Object entity, final Serializable id, final Object[] state, final String[] propertyNames, final Type[] types)
            throws CallbackException {

        try {
            return interceptor.onLoad(entity, id, state, propertyNames, TypeV5Adapter.adapt(sessionFactory, types));
        } catch (final org.hibernate.CallbackException ex) {
            throw (net.sf.hibernate.CallbackException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean onFlushDirty(final Object entity, final Serializable id, final Object[] currentState, final Object[] previousState, final String[] propertyNames, final Type[] types)
            throws CallbackException {

        try {
            final org.hibernate.type.Type[] typesV5 = TypeV5Adapter.adapt(sessionFactory, types);
            return interceptor.onFlushDirty(entity, id, currentState, previousState, propertyNames, typesV5);
        } catch (final org.hibernate.CallbackException ex) {
            throw (net.sf.hibernate.CallbackException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean onSave(final Object entity, final Serializable id, final Object[] state, final String[] propertyNames, final Type[] types)
            throws CallbackException {

        try {
            final org.hibernate.type.Type[] typesV5 = TypeV5Adapter.adapt(sessionFactory, types);
            return interceptor.onSave(entity, id, state, propertyNames, typesV5);
        } catch (final org.hibernate.CallbackException ex) {
            throw (net.sf.hibernate.CallbackException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void onDelete(final Object entity, final Serializable id, final Object[] state, final String[] propertyNames, final Type[] types)
            throws CallbackException {

        try {
            interceptor.onDelete(entity, id, state, propertyNames, TypeV5Adapter.adapt(sessionFactory, types));
        } catch (final org.hibernate.CallbackException ex) {
            throw (net.sf.hibernate.CallbackException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void preFlush(final Iterator entities) throws CallbackException {
        try {
            interceptor.preFlush(entities);
        } catch (final org.hibernate.CallbackException ex) {
            throw (net.sf.hibernate.CallbackException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void postFlush(final Iterator entities) throws CallbackException {
        try {
            interceptor.postFlush(entities);
        } catch (final org.hibernate.CallbackException ex) {
            throw (net.sf.hibernate.CallbackException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Boolean isUnsaved(final Object entity) {
        return null;
    }

    @Override
    public int[] findDirty(final Object entity, final Serializable id, final Object[] currentState, final Object[] previousState, final String[] propertyNames, final Type[] types) {
        final org.hibernate.type.Type[] typesV5 = TypeV5Adapter.adapt(sessionFactory, types);
        return interceptor.findDirty(
                entity, id, currentState, previousState, propertyNames, typesV5);
    }

    @Override
    public Object instantiate(final Class clazz, final Serializable id) throws CallbackException {
        return null;
    }
}
