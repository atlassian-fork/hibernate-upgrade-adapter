package com.atlassian.hibernate.adapter.adapters.type;

import com.atlassian.hibernate.adapter.adapters.persister.AbstractEntityPersisterV2Adapter;
import com.atlassian.hibernate.adapter.adapters.persister.AbstractCollectionPersisterV2Adapter;
import net.sf.hibernate.persister.Joinable;
import org.apache.commons.lang3.NotImplementedException;

/**
 * An adapter bridging the Joinable interface from hibernate v5 (onwards) to v2.
 */
public class JoinableV2Adapter implements Joinable {
    private final org.hibernate.persister.entity.Joinable joinable;

    protected JoinableV2Adapter(final org.hibernate.persister.entity.Joinable joinable) {
        this.joinable = joinable;
    }

    public static Joinable adapt(
            final org.hibernate.persister.entity.Joinable joinable,
            final org.hibernate.SessionFactory sessionFactory) {

        if (joinable == null)
            return null;

        if (joinable instanceof org.hibernate.persister.entity.AbstractEntityPersister)
            return AbstractEntityPersisterV2Adapter.adaptAbstractEntityPersister(
                    (org.hibernate.persister.entity.AbstractEntityPersister) joinable, sessionFactory, null);
        if (joinable instanceof org.hibernate.persister.collection.AbstractCollectionPersister)
            return AbstractCollectionPersisterV2Adapter.adaptCollectionPersister(
                    (org.hibernate.persister.collection.AbstractCollectionPersister) joinable, sessionFactory);

        return new JoinableV2Adapter(joinable);
    }

    public static Joinable adaptJoinableOnly(final org.hibernate.persister.entity.Joinable joinable) {
        if (joinable == null)
            return null;
        return new JoinableV2Adapter(joinable);
    }

    @Override
    public String getName() {
        return joinable.getName();
    }

    @Override
    public String getTableName() {
        return joinable.getTableName();
    }

    @Override
    public String selectFragment(final String alias, final String suffix, final boolean includeCollectionColumns) {
        throw new NotImplementedException("selectFragment not implemented");
    }

    @Override
    public String whereJoinFragment(final String alias, final boolean innerJoin, final boolean includeSubclasses) {
        throw new NotImplementedException("whereJoinFragment not implemented");
    }

    @Override
    public String fromJoinFragment(final String alias, final boolean innerJoin, final boolean includeSubclasses) {
        throw new NotImplementedException("fromJoinFragment not implemented");
    }

    @Override
    public String[] getJoinKeyColumnNames() {
        return joinable.getKeyColumnNames();
    }

    @Override
    public boolean isCollection() {
        return joinable.isCollection();
    }

    @Override
    public boolean isManyToMany() {
        return
                joinable instanceof org.hibernate.persister.collection.CollectionPersister
                && ((org.hibernate.persister.collection.CollectionPersister) joinable).isManyToMany();
    }

    @Override
    public boolean consumesAlias() {
        return joinable.consumesEntityAlias();
    }
}
