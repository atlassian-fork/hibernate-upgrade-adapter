package com.atlassian.hibernate.adapter.adapters.query;

import com.atlassian.hibernate.adapter.adapters.LockModeAdapter;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV5Adapter;
import com.atlassian.hibernate.util.SessionHelper;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.LockMode;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.Query;
import net.sf.hibernate.ScrollMode;
import net.sf.hibernate.ScrollableResults;
import net.sf.hibernate.type.Type;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.DateType;

import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * An adapter bridging the Query interface for hibernate v5 (onwards) to v2.
 */
public class QueryV2Adapter implements Query {
    private static final Log LOG = LogFactory.getLog(QueryV2Adapter.class);

    private final org.hibernate.Session session;
    private final org.hibernate.query.Query query;
    private final net.sf.hibernate.Query queryV2ForCompare;
    private final boolean areParametersOneBased;

    protected QueryV2Adapter(
            final org.hibernate.Session session,
            final org.hibernate.query.Query query,
            final net.sf.hibernate.Query queryV2ForCompare,
            final boolean areParametersOneBased) {

        this.session = session;
        this.query = query;
        this.queryV2ForCompare = queryV2ForCompare;
        this.areParametersOneBased = areParametersOneBased;
    }

    public static Query adapt(
            final org.hibernate.Session session,
            final org.hibernate.query.Query query,
            final net.sf.hibernate.Query queryV2ForCompare,
            final boolean areParametersOneBased) {

        if (query == null)
            return null;
        return new QueryV2Adapter(session, query, queryV2ForCompare, areParametersOneBased);
    }

    public org.hibernate.query.Query getV5Query() {
        return query;
    }

    private int adjustParameterPosition(final int position) {
        return areParametersOneBased ? position + 1 : position;
    }

    private void compareV2V5QueryList(final List list) throws HibernateException {
        if (queryV2ForCompare == null) {
            return;
        }

        List listV5 = list;
        List listV2;
        try {
            listV2 = queryV2ForCompare.list();
            if (listV5.size() == listV2.size()) {
                return;
            }

            // try disabling the cache
            final boolean cacheable = query.isCacheable();
            setCacheable(false);
            try {
                listV2 = queryV2ForCompare.list();
                listV5 = query.list();
                if (listV5.size() == listV2.size()) {
                    return;
                }

                // try flushing everything
                SessionHelper.flushAllowNoTransaction(session);

                listV2 = queryV2ForCompare.list();
                listV5 = query.list();
                if (listV5.size() == listV2.size()) {
                    return;
                }
            } finally {
                setCacheable(cacheable);
            }
        } catch (final PersistenceException | net.sf.hibernate.HibernateException ex) {
            throw new net.sf.hibernate.HibernateException(
                    "An exception occurred while running v2 query for hibernate v2 v5 Query.list().size() comparison", ex);
        }

        // fail
        final String message =
                "Query.list().size() mismatch between hibernate v2 and v5 for query ("
                + listV2.size() + " != " + listV5.size() + "): "
                + getQueryString();
        LOG.error(message);
    }

    private void compareV2V5UniqueResult(final Object resultV5) throws HibernateException {
        if (queryV2ForCompare == null) {
            return;
        }

        try {
            final Object resultV2 = queryV2ForCompare.uniqueResult();
            if ((resultV5 == null) == (resultV2 == null)) {
                return;
            }

            // try disabling the cache
            boolean cacheable = query.isCacheable();
            setCacheable(false);
            try {
                if ((query.uniqueResult() == null) == (queryV2ForCompare.uniqueResult() == null)) {
                    return;
                }

                // try flushing everything
                SessionHelper.flushAllowNoTransaction(session);

                if ((query.uniqueResult() == null) == (queryV2ForCompare.uniqueResult() == null)) {
                    return;
                }
            } finally {
                setCacheable(cacheable);
            }
        } catch (final HibernateException ex) {
            throw new HibernateException(
                    "An exception occurred while running v2 query for hibernate v2 v5 'Query.uniqueResult() != null' comparison", ex);
        }

        // fail
        final String message = "'Query.uniqueResult() != null' mismatch between hibernate v2 and v5 for query: " + getQueryString();
        LOG.error(message);
        throw new org.hibernate.HibernateException(message);
    }

    @Override
    public String getQueryString() {
        return query.getQueryString();
    }

    @Override
    @Deprecated
    public Type[] getReturnTypes() throws HibernateException {
        try {
            return TypeV2Adapter.adapt(query.getReturnTypes());
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public String[] getNamedParameters() throws HibernateException {
        try {
            return query.getNamedParameters();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Iterator iterate() throws HibernateException {
        try {
            return query.iterate();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public ScrollableResults scroll() throws HibernateException {
        try {
            return new ScrollableResultsV2Adapter(query.scroll());
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public ScrollableResults scroll(final ScrollMode scrollMode) throws HibernateException {
        try {
            return new ScrollableResultsV2Adapter(query.scroll(ScrollModeV2Adapter.adapt(scrollMode)));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public List list() throws HibernateException {
        try {
            final List listV5 = query.list();
            compareV2V5QueryList(listV5);
            return listV5;
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object uniqueResult() throws HibernateException {
        try {
            final Object resultV5 = query.uniqueResult();
            compareV2V5UniqueResult(resultV5);
            return resultV5;
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Query setMaxResults(final int maxResults) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setMaxResults(maxResults);
        }
        query.setMaxResults(maxResults);
        return this;
    }

    @Override
    public Query setFirstResult(final int firstResult) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setFirstResult(firstResult);
        }
        query.setFirstResult(firstResult);
        return this;
    }

    @Override
    public Query setCacheable(final boolean cacheable) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setCacheable(cacheable);
        }
        query.setCacheable(cacheable);
        return this;
    }

    @Override
    public Query setCacheRegion(final String cacheRegion) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setCacheRegion(cacheRegion);
        }
        query.setCacheRegion(cacheRegion);
        return this;
    }

    @Override
    public Query setForceCacheRefresh(final boolean forceCacheRefresh) {
        throw new NotImplementedException("setForceCacheRefresh not implemented");
    }

    @Override
    public Query setTimeout(final int timeout) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setTimeout(timeout);
        }
        query.setTimeout(timeout);
        return this;
    }

    @Override
    public Query setFetchSize(final int fetchSize) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setFetchSize(fetchSize);
        }
        query.setFetchSize(fetchSize);
        return this;
    }

    @Override
    public void setLockMode(final String alias, final LockMode lockMode) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setLockMode(alias, lockMode);
        }
        query.setLockMode(alias, LockModeAdapter.adapt(lockMode));
    }

    @Override
    public Query setParameter(final int position, final Object val, final Type type) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setParameter(position, val, type);
        }
        query.setParameter(adjustParameterPosition(position), val, TypeV5Adapter.adapt(session.getSessionFactory(), type));
        return this;
    }

    @Override
    public Query setParameter(final String name, final Object val, final Type type) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setParameter(name, val, type);
        }
        query.setParameter(name, val, TypeV5Adapter.adapt(session.getSessionFactory(), type));
        return this;
    }

    @Override
    public Query setParameter(final int position, final Object val) throws HibernateException {
        try {
            if (queryV2ForCompare != null) {
                queryV2ForCompare.setParameter(position, val);
            }
            query.setParameter(adjustParameterPosition(position), val);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return this;
    }

    @Override
    public Query setParameter(final String name, final Object val) throws HibernateException {
        try {
            if (queryV2ForCompare != null) {
                queryV2ForCompare.setParameter(name, val);
            }
            query.setParameter(name, val);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return this;
    }

    @Override
    public Query setParameterList(final String name, final Collection vals, final Type type) throws HibernateException {
        try {
            if (queryV2ForCompare != null) {
                queryV2ForCompare.setParameterList(name, vals, type);
            }
            query.setParameterList(name, vals, TypeV5Adapter.adapt(session.getSessionFactory(), type));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return this;
    }

    @Override
    public Query setParameterList(final String name, final Collection vals) throws HibernateException {
        try {
            if (queryV2ForCompare != null) {
                queryV2ForCompare.setParameterList(name, vals);
            }
            query.setParameterList(name, vals);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return this;
    }

    @Override
    public Query setParameterList(final String name, final Object[] vals, final Type type) throws HibernateException {
        try {
            if (queryV2ForCompare != null) {
                queryV2ForCompare.setParameterList(name, vals, type);
            }
            query.setParameterList(name, vals, TypeV5Adapter.adapt(session.getSessionFactory(), type));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return this;
    }

    @Override
    public Query setParameterList(final String name, final Object[] vals) throws HibernateException {
        try {
            if (queryV2ForCompare != null) {
                queryV2ForCompare.setParameterList(name, vals);
            }
            query.setParameterList(name, vals);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return this;
    }

    @Override
    public Query setProperties(final Object bean) throws HibernateException {
        try {
            if (queryV2ForCompare != null) {
                queryV2ForCompare.setProperties(bean);
            }
            query.setProperties(bean);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setString(final int position, final String val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setString(position, val);
        }
        query.setString(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setCharacter(final int position, final char val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setCharacter(position, val);
        }
        query.setCharacter(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setBoolean(final int position, final boolean val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setBoolean(position, val);
        }
        query.setBoolean(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setByte(final int position, final byte val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setByte(position, val);
        }
        query.setByte(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setShort(final int position, final short val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setShort(position, val);
        }
        query.setShort(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setInteger(final int position, final int val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setInteger(position, val);
        }
        query.setInteger(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setLong(final int position, final long val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setLong(position, val);
        }
        query.setLong(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setFloat(final int position, final float val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setFloat(position, val);
        }
        query.setFloat(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setDouble(final int position, final double val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setDouble(position, val);
        }
        query.setDouble(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setBinary(final int position, final byte[] val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setBinary(position, val);
        }
        query.setBinary(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setText(final int position, final String val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setText(position, val);
        }
        query.setText(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setSerializable(final int position, final Serializable val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setSerializable(position, val);
        }
        query.setSerializable(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setLocale(final int position, final Locale locale) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setLocale(position, locale);
        }
        query.setLocale(adjustParameterPosition(position), locale);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setBigDecimal(final int position, final BigDecimal number) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setBigDecimal(position, number);
        }
        query.setBigDecimal(adjustParameterPosition(position), number);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setDate(final int position, final Date date) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setDate(position, date);
        }
        query.setDate(adjustParameterPosition(position), date);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setTime(final int position, final Date date) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setTime(position, date);
        }
        query.setTime(adjustParameterPosition(position), date);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setTimestamp(final int position, final Date date) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setTimestamp(position, date);
        }
        query.setTimestamp(adjustParameterPosition(position), date);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setCalendar(final int position, final Calendar calendar) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setCalendar(position, calendar);
        }
        query.setCalendar(adjustParameterPosition(position), calendar);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setCalendarDate(final int position, final Calendar calendar) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setCalendarDate(position, calendar);
        }
        query.setCalendarDate(adjustParameterPosition(position), calendar);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setString(final String name, final String val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setString(name, val);
        }
        query.setString(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setCharacter(final String name, final char val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setCharacter(name, val);
        }
        query.setCharacter(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setBoolean(final String name, final boolean val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setBoolean(name, val);
        }
        query.setBoolean(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setByte(final String name, final byte val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setByte(name, val);
        }
        query.setByte(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setShort(final String name, final short val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setShort(name, val);
        }
        query.setShort(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setInteger(final String name, final int val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setInteger(name, val);
        }
        query.setInteger(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setLong(final String name, final long val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setLong(name, val);
        }
        query.setLong(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setFloat(final String name, final float val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setFloat(name, val);
        }
        query.setFloat(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setDouble(final String name, final double val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setDouble(name, val);
        }
        query.setDouble(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setBinary(final String name, final byte[] val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setBinary(name, val);
        }
        query.setBinary(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setText(final String name, final String val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setText(name, val);
        }
        query.setText(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setSerializable(final String name, final Serializable val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setSerializable(name, val);
        }
        query.setSerializable(name, val);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setLocale(final String name, final Locale locale) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setLocale(name, locale);
        }
        query.setLocale(name, locale);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setBigDecimal(final String name, final BigDecimal number) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setBigDecimal(name, number);
        }
        query.setBigDecimal(name, number);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setDate(final String name, final Date date) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setDate(name, date);
        }
        query.setDate(name, date);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setTime(final String name, final Date date) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setTime(name, date);
        }
        query.setTime(name, date);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setTimestamp(final String name, final Date date) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setTimestamp(name, date);
        }
        query.setTimestamp(name, date);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setCalendar(final String name, final Calendar calendar) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setCalendar(name, calendar);
        }
        query.setCalendar(name, calendar);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setCalendarDate(final String name, final Calendar calendar) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setCalendarDate(name, calendar);
        }
        Date date = calendar != null ? new Date(calendar.getTime().getTime()) : null;
        query.setParameter(name, date, DateType.INSTANCE);
        return this;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setEntity(final int position, final Object val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setEntity(position, val);
        }
        query.setEntity(adjustParameterPosition(position), val);
        return this;
    }

    @Override
    @Deprecated
    public Query setEnum(final int position, final Object val) throws MappingException {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setEnum(position, val);
        }
        return setParameter(adjustParameterPosition(position), val, new net.sf.hibernate.type.PersistentEnumType(val.getClass()));
    }

    @Override
    @Deprecated
    public Query setEnum(final String name, final Object val) throws MappingException {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setEnum(name, val);
        }
        return setParameter(name, val, new net.sf.hibernate.type.PersistentEnumType(val.getClass()));
    }

    @Override
    @SuppressWarnings("deprecation")
    public Query setEntity(final String name, final Object val) {
        if (queryV2ForCompare != null) {
            queryV2ForCompare.setEntity(name, val);
        }
        query.setEntity(name, val);
        return this;
    }
}
