package com.atlassian.hibernate.adapter.adapters.type;

import net.sf.hibernate.type.ForeignKeyDirection;
import org.apache.commons.lang3.NotImplementedException;

/**
 * An adapter bridging the ForeignKeyDirection object from hibernate v5 (onwards) to v2.
 */
public final class ForeignKeyDirectionV2Adapter {
    private ForeignKeyDirectionV2Adapter() {
    }

    public static ForeignKeyDirection adapt(final org.hibernate.type.ForeignKeyDirection foreignKeyDirection) {
        if (foreignKeyDirection == null)
            return null;
        if (foreignKeyDirection == org.hibernate.type.ForeignKeyDirection.FROM_PARENT)
            return ForeignKeyDirection.FOREIGN_KEY_FROM_PARENT;
        if (foreignKeyDirection == org.hibernate.type.ForeignKeyDirection.TO_PARENT)
            return ForeignKeyDirection.FOREIGN_KEY_TO_PARENT;
        throw new NotImplementedException("Unexpected org.hibernate.type.ForeignKeyDirection: " + foreignKeyDirection);
    }
}
