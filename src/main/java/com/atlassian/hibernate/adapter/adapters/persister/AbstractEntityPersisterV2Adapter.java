package com.atlassian.hibernate.adapter.adapters.persister;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.mapping.ClassMetadataV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import com.atlassian.hibernate.util.ThrowableUtil;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.LockMode;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.cache.CacheConcurrencyStrategy;
import net.sf.hibernate.engine.Cascades;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.id.IdentifierGenerator;
import net.sf.hibernate.mapping.RootClass;
import net.sf.hibernate.mapping.SimpleValue;
import net.sf.hibernate.metadata.ClassMetadata;
import net.sf.hibernate.persister.AbstractEntityPersister;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.type.Type;
import net.sf.hibernate.type.VersionType;
import org.apache.commons.lang3.NotImplementedException;

import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.sql.SQLException;

/**
 * An adapter bridging the AbstractEntityPersister class for hibernate v5 (onwards) to v2.
 */
public class AbstractEntityPersisterV2Adapter
        extends AbstractEntityPersister
        implements ClassPersister, ClassMetadata {

    private final org.hibernate.persister.entity.AbstractEntityPersister persister;
    private final ClassPersister classPersisterImpl;
    private final ClassMetadata classMetadataImpl;

    protected AbstractEntityPersisterV2Adapter(
            final org.hibernate.persister.entity.AbstractEntityPersister persister,
            final org.hibernate.SessionFactory sessionFactory,
            final org.hibernate.Session session) throws HibernateException {

        // all public methods are overridden, so running the super constructor
        // really happens only because it can't be avoided
        super(new RootClass() {
            @Override
            public Class getMappedClass() {
                return persister.getMappedClass();
            }

            @Override
            public String getName() {
                return persister.getMappedClass().getName();
            }

            @Override
            public SimpleValue getIdentifier() {
                return new SimpleValue();
            }
        }, getV2orV5SessionFactory(sessionFactory));

        this.persister = persister;
        this.classPersisterImpl = ClassPersisterV2Adapter.adaptClassPersisterOnly(persister, sessionFactory, session);
        this.classMetadataImpl = ClassMetadataV2Adapter.adaptClassMetadataOnly(persister, sessionFactory);
    }

    public static AbstractEntityPersister adaptAbstractEntityPersister(
            final org.hibernate.persister.entity.AbstractEntityPersister persister,
            final org.hibernate.SessionFactory sessionFactory,
            final org.hibernate.Session session) {

        if (persister instanceof org.hibernate.persister.entity.SingleTableEntityPersister) {
            return com.atlassian.hibernate.adapter.adapters.persister.EntityPersisterV2Adapter.adaptEntityPersister(
                    (org.hibernate.persister.entity.SingleTableEntityPersister) persister,
                    sessionFactory,
                    session);
        }
        try {
            return new AbstractEntityPersisterV2Adapter(persister, sessionFactory, session);
        } catch (final HibernateException ex) {
            // this should never happen
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    private static net.sf.hibernate.engine.SessionFactoryImplementor getV2orV5SessionFactory(
            final org.hibernate.SessionFactory sessionFactory) {

        return (net.sf.hibernate.engine.SessionFactoryImplementor)
                HibernateBridge.get(sessionFactory).getV2orV5SessionFactory();
    }

    //---------- AbstractEntityPersister Overrides ----------//

    @Override
    public String toString() {
        return persister.toString();
    }

    @Override
    public String identifierSelectFragment(final String name, final String suffix) {
        return persister.identifierSelectFragment(name, suffix);
    }

    @Override
    public String[] getIdentifierColumnNames() {
        return persister.getIdentifierColumnNames();
    }

    @Override
    public boolean isInherited() {
        return persister.isInherited();
    }

    @Override
    public boolean isPolymorphic() {
        return persister.isPolymorphic();
    }

    @Override
    public boolean hasSubclasses() {
        return persister.hasSubclasses();
    }

    @Override
    @SuppressWarnings("deprecation")
    public Class getMappedSuperclass() {
        final String entityName = persister.getMappedSuperclass();
        return
                entityName != null
                ? persister.getEntityMetamodel().getSessionFactory().getEntityPersister(entityName).getMappedClass()
                : null;
    }

    @Override
    public boolean isExplicitPolymorphism() {
        return persister.isExplicitPolymorphism();
    }

    @Override
    public Type getType() {
        return TypeV2Adapter.adapt(persister.getType());
    }

    @Override
    public String[] getSubclassPropertyColumnAliases(final String propertyName, final String suffix) {
        return persister.getSubclassPropertyColumnAliases(propertyName, suffix);
    }

    @Override
    public String[] getJoinKeyColumnNames() {
        return persister.getKeyColumnNames();
    }

    @Override
    public String getName() {
        return persister.getName();
    }

    @Override
    public String selectFragment(final String alias, final String suffix) {
        return persister.selectFragment(alias, suffix);
    }

    @Override
    public String[] getIdentifierAliases(final String suffix) {
        return persister.getIdentifierAliases(suffix);
    }

    @Override
    public String[] getPropertyAliases(final String suffix, final int i) {
        return persister.getPropertyAliases(suffix, i);
    }

    @Override
    public String getDiscriminatorAlias(final String suffix) {
        return persister.getDiscriminatorAlias(suffix);
    }

    @Override
    public Object loadByUniqueKey(final String propertyName, final Serializable uniqueKey, final SessionImplementor session)
            throws HibernateException, SQLException {

        try {
            final org.hibernate.Session sessionV5 = HibernateBridge.get(session.getFactory()).getV5Session(session);
            return persister.loadByUniqueKey(propertyName, uniqueKey, (org.hibernate.engine.spi.SessionImplementor) sessionV5);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public String[] getUniqueKeyColumnNames(final String propertyName) {
        throw new NotImplementedException("getUniqueKeyColumnNames");
    }

    @Override
    public boolean isCollection() {
        return persister.isCollection();
    }

    @Override
    public boolean consumesAlias() {
        return persister.consumesEntityAlias();
    }

    @Override
    public boolean isManyToMany() {
        throw new NotImplementedException("isManyToMany not implemented");
    }

    @Override
    public boolean isBatchable() {
        return persister.isBatchable();
    }

    @Override
    public String getVersionColumnName() {
        return persister.getVersionColumnName();
    }

    @Override
    public boolean[] getNotNullInsertableColumns(final Object[] fields) {
        throw new NotImplementedException("getNotNullInsertableColumns not implemented");
    }

    //---------- ClassPersister / ClassMetadata ----------//

    @Override
    public void postInstantiate() throws MappingException {
        classPersisterImpl.postInstantiate();
    }

    @Override
    public Serializable getIdentifierSpace() {
        return classPersisterImpl.getIdentifierSpace();
    }

    @Override
    public Serializable[] getPropertySpaces() {
        return classPersisterImpl.getPropertySpaces();
    }

//    @Override
//    public String getClassName()
//    {
//        return getMappedClass();
//    }

    @Override
    public Class getConcreteProxyClass() {
        return classPersisterImpl.getConcreteProxyClass();
    }

    @Override
    public Object createProxy(final Serializable id, final SessionImplementor session) throws HibernateException {
        return classPersisterImpl.createProxy(id, session);
    }

    @Override
    public boolean hasCollections() {
        return classPersisterImpl.hasCollections();
    }

    @Override
    public boolean hasCascades() {
        return classPersisterImpl.hasCascades();
    }

    @Override
    public boolean isIdentifierAssignedByInsert() {
        return classPersisterImpl.isIdentifierAssignedByInsert();
    }

    @Override
    public boolean isUnsaved(final Object object) throws HibernateException {
        return classPersisterImpl.isUnsaved(object);
    }

    @Override
    public void setPropertyValue(final Object object, final int i, final Object value) throws HibernateException {
        classPersisterImpl.setPropertyValue(object, i, value);
    }

    @Override
    public Object getPropertyValue(final Object object, final int i) throws HibernateException {
        return classPersisterImpl.getPropertyValue(object, i);
    }

    @Override
    public int[] findDirty(final Object[] x, final Object[] y, final Object owner, final SessionImplementor session)
            throws HibernateException {
        return classPersisterImpl.findDirty(x, y, owner, session);
    }

    @Override
    public int[] findModified(final Object[] old, final Object[] current, final Object object, final SessionImplementor session)
            throws HibernateException {
        return classPersisterImpl.findModified(old, current, object, session);
    }

    @Override
    public boolean hasIdentifierPropertyOrEmbeddedCompositeIdentifier() {
        return classPersisterImpl.hasIdentifierPropertyOrEmbeddedCompositeIdentifier();
    }

    @Override
    public VersionType getVersionType() {
        return classPersisterImpl.getVersionType();
    }

    @Override
    public IdentifierGenerator getIdentifierGenerator()
            throws HibernateException {
        return classPersisterImpl.getIdentifierGenerator();
    }

    @Override
    public Object load(final Serializable id, final Object optionalObject, final LockMode lockMode, final SessionImplementor session)
            throws HibernateException {
        return classPersisterImpl.load(id, optionalObject, lockMode, session);
    }

    @Override
    public void lock(final Serializable id, final Object version, final Object object, final LockMode lockMode, final SessionImplementor session)
            throws HibernateException {
        classPersisterImpl.lock(id, version, object, lockMode, session);
    }

    @Override
    public void insert(final Serializable id, final Object[] fields, final Object object, final SessionImplementor session)
            throws HibernateException {
        classPersisterImpl.insert(id, fields, object, session);
    }

    @Override
    public Serializable insert(final Object[] fields, final Object object, final SessionImplementor session)
            throws HibernateException {
        return classPersisterImpl.insert(fields, object, session);
    }

    @Override
    public void delete(final Serializable id, final Object version, final Object object, final SessionImplementor session)
            throws HibernateException {
        classPersisterImpl.delete(id, version, object, session);
    }

    @Override
    public void update(final Serializable id, final Object[] fields, final int[] dirtyFields, final Object[] oldFields, final Object oldVersion, final Object object, final SessionImplementor session)
            throws HibernateException {
        classPersisterImpl.update(id, fields, dirtyFields, oldFields, oldVersion, object, session);
    }

    @Override
    public boolean[] getPropertyUpdateability() {
        return classPersisterImpl.getPropertyUpdateability();
    }

    @Override
    public boolean[] getPropertyInsertability() {
        return classPersisterImpl.getPropertyInsertability();
    }

    @Override
    public Cascades.CascadeStyle[] getPropertyCascadeStyles() {
        return classPersisterImpl.getPropertyCascadeStyles();
    }

    @Override
    public boolean isCacheInvalidationRequired() {
        return classPersisterImpl.isCacheInvalidationRequired();
    }

    @Override
    public boolean hasCache() {
        return classPersisterImpl.hasCache();
    }

    @Override
    public CacheConcurrencyStrategy getCache() {
        return classPersisterImpl.getCache();
    }

    @Override
    public ClassMetadata getClassMetadata() {
        return classPersisterImpl.getClassMetadata();
    }

    @Override
    public boolean isBatchLoadable() {
        return classPersisterImpl.isBatchLoadable();
    }

    @Override
    public Object[] getCurrentPersistentState(final Serializable id, final Object version, final SessionImplementor session) throws HibernateException {
        return classPersisterImpl.getCurrentPersistentState(id, version, session);
    }

    @Override
    public Object getCurrentVersion(final Serializable id, final SessionImplementor session) throws HibernateException {
        return classPersisterImpl.getCurrentVersion(id, session);
    }

//    @Override
//    public Class getMappedClass()
//    {
//        return classMetadataImpl.getMappedClass();
//    }

    @Override
    public Object instantiate(final Serializable id) throws HibernateException {
        return classMetadataImpl.instantiate(id);
    }

    @Override
    public String getIdentifierPropertyName() {
        return classMetadataImpl.getIdentifierPropertyName();
    }

    @Override
    public String[] getPropertyNames() {
        return classMetadataImpl.getPropertyNames();
    }

    @Override
    public Type getIdentifierType() {
        return classMetadataImpl.getIdentifierType();
    }

    @Override
    public Type[] getPropertyTypes() {
        return classMetadataImpl.getPropertyTypes();
    }

    @Override
    public Type getPropertyType(final String propertyName) throws MappingException {
        try {
            return classMetadataImpl.getPropertyType(propertyName);
        } catch (final MappingException ex) {
            throw ex;
        } catch (final HibernateException ex) {
            throw new MappingException(ex.getMessage(), ex.getCause());
        }
    }

    @Override
    public Object getPropertyValue(final Object object, final String propertyName) throws HibernateException {
        return classMetadataImpl.getPropertyValue(object, propertyName);
    }

    @Override
    public Object[] getPropertyValues(final Object entity) throws HibernateException {
        return classMetadataImpl.getPropertyValues(entity);
    }

    @Override
    public void setPropertyValues(final Object object, final Object[] values) throws HibernateException {
        classMetadataImpl.setPropertyValues(object, values);
    }

    @Override
    public Serializable getIdentifier(final Object entity) throws HibernateException {
        return classMetadataImpl.getIdentifier(entity);
    }

    @Override
    public void setIdentifier(final Object object, final Serializable id) throws HibernateException {
        classMetadataImpl.setIdentifier(object, id);
    }

    @Override
    public boolean implementsLifecycle() {
        return classMetadataImpl.implementsLifecycle();
    }

    @Override
    public boolean implementsValidatable() {
        return classMetadataImpl.implementsValidatable();
    }

    @Override
    public boolean hasProxy() {
        return classMetadataImpl.hasProxy();
    }

    @Override
    public boolean isMutable() {
        return classMetadataImpl.isMutable();
    }

    @Override
    public boolean isVersioned() {
        return classMetadataImpl.isVersioned();
    }

    @Override
    public Object getVersion(final Object object) throws HibernateException {
        return classMetadataImpl.getVersion(object);
    }

    @Override
    public int getVersionProperty() {
        return classMetadataImpl.getVersionProperty();
    }

    @Override
    public boolean[] getPropertyNullability() {
        return classMetadataImpl.getPropertyNullability();
    }

    @Override
    public boolean hasIdentifierProperty() {
        return classMetadataImpl.hasIdentifierProperty();
    }

    @Override
    public void setPropertyValue(final Object object, final String propertyName, final Object value) throws HibernateException {
        classMetadataImpl.setPropertyValue(object, propertyName, value);
    }

    //---------- Other Interfaces ----------//

    @Override
    protected String[] getActualPropertyColumnNames(final int i) {
        throw new NotImplementedException("getActualPropertyColumnNames not implemented");
    }

    @Override
    protected String getFormulaTemplate(final int i) {
        throw new NotImplementedException("getFormulaTemplate not implemented");
    }

    @Override
    protected String generateLockString() {
        throw new NotImplementedException("generateLockString not implemented");
    }

    @Override
    protected String getDiscriminatorAlias() {
        throw new NotImplementedException("getDiscriminatorAlias not implemented");
    }

    @Override
    protected String getVersionedTableName() {
        throw new NotImplementedException("getVersionedTableName not implemented");
    }

    @Override
    protected String getConcreteSelectString() {
        throw new NotImplementedException("getConcreteSelectString not implemented");
    }

    @Override
    protected String getVersionSelectString() {
        throw new NotImplementedException("getVersionSelectString not implemented");
    }

    @Override
    protected String[] getPropertyColumnNames(final int i) {
        throw new NotImplementedException("getPropertyColumnNames not implemented");
    }

    @Override
    public String getDiscriminatorColumnName() {
        return persister.getDiscriminatorColumnName();
    }

    @Override
    public int countSubclassProperties() {
        return persister.countSubclassProperties();
    }

    @Override
    public int enableJoinedFetch(final int i) {
        throw new NotImplementedException("enableJoinedFetch not implemented");
    }

    @Override
    public boolean isDefinedOnSubclass(final int i) {
        return persister.isDefinedOnSubclass(i);
    }

    @Override
    public Type getSubclassPropertyType(final int i) {
        return TypeV2Adapter.adapt(persister.getSubclassPropertyType(i));
    }

    @Override
    public String getSubclassPropertyName(final int i) {
        return persister.getSubclassPropertyName(i);
    }

    @Override
    public String[] getSubclassPropertyColumnNames(final int i) {
        return persister.getSubclassPropertyColumnNames(i);
    }

    @Override
    public String getSubclassPropertyTableName(final int i) {
        return persister.getSubclassPropertyTableName(i);
    }

    @Override
    public String[] toColumns(final String name, final int i) {
        return persister.toColumns(name, i);
    }

    @Override
    public String fromTableFragment(final String alias) {
        return persister.fromTableFragment(alias);
    }

    @Override
    public Object getDiscriminatorSQLValue() {
        return persister.getDiscriminatorSQLValue();
    }

    @Override
    public String queryWhereFragment(final String alias, final boolean innerJoin, final boolean includeSubclasses) throws MappingException {
        throw new NotImplementedException("queryWhereFragment not implemented");
    }

    @Override
    public String propertySelectFragment(final String alias, final String suffix) {
        throw new NotImplementedException("propertySelectFragment not implemented");
    }

    @Override
    public String getTableName() {
        return persister.getTableName();
    }

    @Override
    public String whereJoinFragment(final String alias, final boolean innerJoin, final boolean includeSubclasses) {
        return persister.whereJoinFragment(alias, innerJoin, includeSubclasses);
    }

    @Override
    public String fromJoinFragment(final String alias, final boolean innerJoin, final boolean includeSubclasses) {
        return persister.fromJoinFragment(alias, innerJoin, includeSubclasses);
    }

    @Override
    public Type getDiscriminatorType() {
        return TypeV2Adapter.adapt(persister.getDiscriminatorType());
    }

    @Override
    public Class getSubclassForDiscriminatorValue(final Object value) {
        throw new NotImplementedException("getSubclassForDiscriminatorValue not implemented");
    }
}
