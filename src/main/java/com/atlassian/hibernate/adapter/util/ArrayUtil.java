package com.atlassian.hibernate.adapter.util;

import java.util.function.Function;

public final class ArrayUtil {
    private ArrayUtil() { }

    public static <TInput, TOutput> TOutput[] transformArray(
            final TInput[] array, final TOutput[] output, final Function<TInput, TOutput> function) {
        for (int i = 0; i < array.length; i++) {
            output[i] = function.apply(array[i]);
        }
        return output;
    }
}
