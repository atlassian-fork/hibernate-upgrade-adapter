package com.atlassian.hibernate.adapter.util;

import com.atlassian.hibernate.adapter.HibernateBridge;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.LockMode;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.cache.CacheConcurrencyStrategy;
import net.sf.hibernate.engine.Cascades;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.id.IdentifierGenerator;
import net.sf.hibernate.metadata.ClassMetadata;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.type.Type;
import net.sf.hibernate.type.VersionType;

import java.io.Serializable;

/**
 * An object that implements hibernate v2 ClassPersister, for the sole purpose of storing a
 * HibernateBridge object, accessible via SessionFactory.getPersister(HibernateBridge.class.getName()).
 * <p>
 * This is required because SessionFactory may be intercepted by Spring, and so it's not possible
 * simply implement HibernateBridge or cast to one of the adapter implementations.
 */
public class HibernateBridgeHolder implements ClassPersister {
    private final HibernateBridge hibernateBridge;

    public HibernateBridgeHolder(final HibernateBridge hibernateBridge) {
        this.hibernateBridge = hibernateBridge;
    }

    @Override
    public Object getVersion(final Object object) throws HibernateException {
        return hibernateBridge;
    }

    //---------- Unsupported Operations ----------//

    @Override
    public void postInstantiate() throws MappingException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Serializable getIdentifierSpace() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Serializable[] getPropertySpaces() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Class getMappedClass() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getClassName() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean implementsLifecycle() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean implementsValidatable() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasProxy() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Class getConcreteProxyClass() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object createProxy(final Serializable id, final SessionImplementor session) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasCollections() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasCascades() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isMutable() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isIdentifierAssignedByInsert() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isUnsaved(final Object object) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setPropertyValues(final Object object, final Object[] values) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] getPropertyValues(final Object object) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setPropertyValue(final Object object, final int i, final Object value) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object getPropertyValue(final Object object, final int i) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object getPropertyValue(final Object object, final String propertyName) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Type getPropertyType(final String propertyName) throws MappingException {
        throw new UnsupportedOperationException();
    }

    @Override
    public int[] findDirty(final Object[] x, final Object[] y, final Object owner, final SessionImplementor session) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public int[] findModified(final Object[] old, final Object[] current, final Object object, final SessionImplementor session) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasIdentifierProperty() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasIdentifierPropertyOrEmbeddedCompositeIdentifier() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Serializable getIdentifier(final Object object) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setIdentifier(final Object object, final Serializable id) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isVersioned() {
        throw new UnsupportedOperationException();
    }

    @Override
    public VersionType getVersionType() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getVersionProperty() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object instantiate(final Serializable id) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public IdentifierGenerator getIdentifierGenerator() throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object load(final Serializable id, final Object optionalObject, final LockMode lockMode, final SessionImplementor session) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void lock(final Serializable id, final Object version, final Object object, final LockMode lockMode, final SessionImplementor session) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void insert(final Serializable id, final Object[] fields, final Object object, final SessionImplementor session) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Serializable insert(final Object[] fields, final Object object, final SessionImplementor session) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(final Serializable id, final Object version, final Object object, final SessionImplementor session) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(final Serializable id, final Object[] fields, final int[] dirtyFields, final Object[] oldFields, final Object oldVersion, final Object object, final SessionImplementor session) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Type[] getPropertyTypes() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String[] getPropertyNames() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean[] getPropertyUpdateability() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean[] getPropertyNullability() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean[] getPropertyInsertability() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Cascades.CascadeStyle[] getPropertyCascadeStyles() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Type getIdentifierType() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getIdentifierPropertyName() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isCacheInvalidationRequired() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasCache() {
        throw new UnsupportedOperationException();
    }

    @Override
    public CacheConcurrencyStrategy getCache() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ClassMetadata getClassMetadata() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isBatchLoadable() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] getCurrentPersistentState(final Serializable id, final Object version, final SessionImplementor session) throws HibernateException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object getCurrentVersion(final Serializable id, final SessionImplementor session) throws HibernateException {
        throw new UnsupportedOperationException();
    }
}
