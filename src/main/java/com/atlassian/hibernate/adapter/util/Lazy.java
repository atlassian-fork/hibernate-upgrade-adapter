package com.atlassian.hibernate.adapter.util;

import java.util.function.Supplier;

public final class Lazy<T> {
    private volatile T value;
    private final Supplier<T> supplier;

    public Lazy(final Supplier<T> supplier) {
        this.supplier = supplier;
    }

    public T getNoAutoCreate() {
        return value;
    }

    public T get() {
        T result = value;
        if (result == null) {
            synchronized (this) {
                result = value;
                if (result == null) {
                    result = supplier.get();
                    value = result;
                }
            }
        }
        return result;
    }
}
