package com.atlassian.hibernate.adapter.reflection.criterion;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.PropertyExpression;

import java.util.function.Function;

public final class PropertyExpressionV2Reflection {
    private PropertyExpressionV2Reflection() { }

    private static final Function<Object, Object> PROPERTY_NAME_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(PropertyExpression.class, "propertyName");
    private static final Function<Object, Object> OTHER_PROPERTY_NAME_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(PropertyExpression.class, "otherPropertyName");

    public static String getPropertyName(final PropertyExpression obj) {
        return (String) PROPERTY_NAME_FIELD.apply(obj);
    }

    public static String getOtherPropertyName(final PropertyExpression obj) {
        return (String) OTHER_PROPERTY_NAME_FIELD.apply(obj);
    }
}
