package com.atlassian.hibernate.adapter.reflection;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.impl.FilterImpl;

import java.util.function.Function;

public final class FilterImplV2Reflection {
    private FilterImplV2Reflection() { }

    private static final Function<Object, Object> COLLECTION_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(FilterImpl.class, "collection");

    public static Object getCollection(final FilterImpl obj) {
        return COLLECTION_FIELD.apply(obj);
    }
}
