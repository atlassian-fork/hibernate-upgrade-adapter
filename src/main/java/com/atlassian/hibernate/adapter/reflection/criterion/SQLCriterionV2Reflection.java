package com.atlassian.hibernate.adapter.reflection.criterion;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.engine.TypedValue;
import net.sf.hibernate.expression.SQLCriterion;

import java.util.function.Function;

public final class SQLCriterionV2Reflection {
    private SQLCriterionV2Reflection() { }

    private static final Function<Object, Object> SQL_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(SQLCriterion.class, "sql");
    private static final Function<Object, Object> TYPED_VALUES_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(SQLCriterion.class, "typedValues");

    public static String getSql(final SQLCriterion obj) {
        return (String) SQL_FIELD.apply(obj);
    }

    public static TypedValue[] getTypedValues(final SQLCriterion obj) {
        return (TypedValue[]) TYPED_VALUES_FIELD.apply(obj);
    }
}
