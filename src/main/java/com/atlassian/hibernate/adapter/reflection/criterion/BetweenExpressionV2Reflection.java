package com.atlassian.hibernate.adapter.reflection.criterion;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.BetweenExpression;

import java.util.function.Function;

public final class BetweenExpressionV2Reflection {
    private BetweenExpressionV2Reflection() { }

    private static final Function<Object, Object> PROPERTY_NAME_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(BetweenExpression.class, "propertyName");
    private static final Function<Object, Object> LO_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(BetweenExpression.class, "lo");
    private static final Function<Object, Object> HI_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(BetweenExpression.class, "hi");

    public static String getPropertyName(final BetweenExpression obj) {
        return (String) PROPERTY_NAME_FIELD.apply(obj);
    }

    public static Object getLo(final BetweenExpression obj) {
        return LO_FIELD.apply(obj);
    }

    public static Object getHi(final BetweenExpression obj) {
        return HI_FIELD.apply(obj);
    }
}
