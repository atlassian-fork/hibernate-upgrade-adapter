package com.atlassian.hibernate.adapter.reflection.criterion;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.Example;
import net.sf.hibernate.expression.MatchMode;

import java.util.Set;
import java.util.function.Function;

public final class ExampleV2Reflection {
    private ExampleV2Reflection() { }

    private static final Function<Object, Object> ENTITY_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(Example.class, "entity");
    private static final Function<Object, Object> EXCLUDED_PROPERTIES_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(Example.class, "excludedProperties");
    private static final Function<Object, Object> SELECTOR_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(Example.class, "selector");
    private static final Function<Object, Object> IS_LIKE_ENABLED_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(Example.class, "isLikeEnabled");
    private static final Function<Object, Object> MATCH_MODE_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(Example.class, "matchMode");

    public static Object getEntity(final Example obj) {
        return ENTITY_FIELD.apply(obj);
    }

    public static Set getExcludedProperties(final Example obj) {
        return (Set) EXCLUDED_PROPERTIES_FIELD.apply(obj);
    }

    public static Example.PropertySelector getSelector(final Example obj) {
        return (Example.PropertySelector) SELECTOR_FIELD.apply(obj);
    }

    public static boolean getIsLikeEnabled(final Example obj) {
        return (Boolean) IS_LIKE_ENABLED_FIELD.apply(obj);
    }

    public static MatchMode getMatchMode(final Example obj) {
        return (MatchMode) MATCH_MODE_FIELD.apply(obj);
    }
}
