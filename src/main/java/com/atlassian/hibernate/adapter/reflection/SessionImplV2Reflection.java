package com.atlassian.hibernate.adapter.reflection;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.impl.SessionImpl;

import java.util.function.BiConsumer;

public final class SessionImplV2Reflection {
    private SessionImplV2Reflection() { }

    private static final BiConsumer<Object, Object> TIMESTAMP_FIELD_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(SessionImpl.class, "timestamp");

    public static void setTimestamp(final SessionImpl obj, final long value) {
        TIMESTAMP_FIELD_SETTER.accept(obj, value);
    }
}
