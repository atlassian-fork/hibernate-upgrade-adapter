package com.atlassian.hibernate.adapter.reflection;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.UserType;
import net.sf.hibernate.type.CustomType;

import java.util.function.Function;

public final class CustomTypeV2Reflection {
    private CustomTypeV2Reflection() { }

    private static final Function<Object, Object> USER_TYPE_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(CustomType.class, "userType");

    public static UserType getUserType(final CustomType obj) {
        return (UserType) USER_TYPE_FIELD.apply(obj);
    }
}
