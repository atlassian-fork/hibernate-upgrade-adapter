package com.atlassian.hibernate.adapter.reflection.criterion;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.NullExpression;

import java.util.function.Function;

public final class NullExpressionV2Reflection {
    private NullExpressionV2Reflection() { }

    private static final Function<Object, Object> PROPERTY_NAME_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(NullExpression.class, "propertyName");

    public static String getPropertyName(final NullExpression obj) {
        return (String) PROPERTY_NAME_FIELD.apply(obj);
    }
}
