package com.atlassian.hibernate.adapter.reflection.criterion;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.NotNullExpression;

import java.util.function.Function;

public final class NotNullExpressionV2Reflection {
    private NotNullExpressionV2Reflection() { }

    private static final Function<Object, Object> PROPERTY_NAME_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(NotNullExpression.class, "propertyName");

    public static String getPropertyName(final NotNullExpression obj) {
        return (String) PROPERTY_NAME_FIELD.apply(obj);
    }
}
