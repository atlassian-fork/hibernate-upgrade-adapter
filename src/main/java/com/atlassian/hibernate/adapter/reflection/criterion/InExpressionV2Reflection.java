package com.atlassian.hibernate.adapter.reflection.criterion;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.InExpression;

import java.util.function.Function;

public final class InExpressionV2Reflection {
    private InExpressionV2Reflection() { }

    private static final Function<Object, Object> PROPERTY_NAME_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(InExpression.class, "propertyName");
    private static final Function<Object, Object> VALUES_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(InExpression.class, "values");

    public static String getPropertyName(final InExpression obj) {
        return (String) PROPERTY_NAME_FIELD.apply(obj);
    }

    public static Object[] getValues(final InExpression obj) {
        return (Object[]) VALUES_FIELD.apply(obj);
    }
}
