package com.atlassian.hibernate.adapter.reflection;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.Interceptor;
import net.sf.hibernate.cfg.Settings;
import net.sf.hibernate.impl.SessionFactoryImpl;

import java.util.Map;
import java.util.function.Function;

public final class SessionFactoryImplV2Reflection {
    private SessionFactoryImplV2Reflection() { }

    private static final Function<Object, Object> SETTINGS_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(SessionFactoryImpl.class, "settings");
    private static final Function<Object, Object> INTERCEPTOR_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(SessionFactoryImpl.class, "interceptor");
    private static final Function<Object, Object> CLASS_PROPERTIES_BY_NAME_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(SessionFactoryImpl.class, "classPersistersByName");

    public static Settings getSettings(final SessionFactoryImpl obj) {
        return (Settings) SETTINGS_FIELD.apply(obj);
    }

    public static Interceptor getInterceptor(final SessionFactoryImpl obj) {
        return (Interceptor) INTERCEPTOR_FIELD.apply(obj);
    }

    public static Map getClassPersistersByName(final SessionFactoryImpl obj) {
        return (Map) CLASS_PROPERTIES_BY_NAME_FIELD.apply(obj);
    }
}
