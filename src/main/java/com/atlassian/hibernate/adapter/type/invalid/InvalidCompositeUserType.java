package com.atlassian.hibernate.adapter.type.invalid;

import net.sf.hibernate.CompositeUserType;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.type.Type;
import org.apache.commons.lang3.NotImplementedException;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Used by CompositeCustomTypeV2Adapter where a CompositeUserType must be instantiated in it's constructor.
 */
public class InvalidCompositeUserType implements CompositeUserType {

    private static NotImplementedException notImplemented() {
        return new NotImplementedException("Operation not valid on InvalidCompositeUserType");
    }

    @Override
    public String[] getPropertyNames() {
        throw notImplemented();
    }

    @Override
    public Type[] getPropertyTypes() {
        throw notImplemented();
    }

    @Override
    public Object getPropertyValue(final Object component, final int property) throws HibernateException {
        throw notImplemented();
    }

    @Override
    public void setPropertyValue(final Object component, final int property, final Object value) throws HibernateException {
        throw notImplemented();
    }

    @Override
    public Class returnedClass() {
        throw notImplemented();
    }

    @Override
    public boolean equals(final Object x, final Object y) throws HibernateException {
        throw notImplemented();
    }

    @Override
    public Object nullSafeGet(final ResultSet rs, final String[] names, final SessionImplementor session, final Object owner) throws HibernateException, SQLException {
        throw notImplemented();
    }

    @Override
    public void nullSafeSet(final PreparedStatement st, final Object value, final int index, final SessionImplementor session) throws HibernateException, SQLException {
        throw notImplemented();
    }

    @Override
    public Object deepCopy(final Object value) throws HibernateException {
        throw notImplemented();
    }

    @Override
    public boolean isMutable() {
        throw notImplemented();
    }

    @Override
    public Serializable disassemble(final Object value, final SessionImplementor session) throws HibernateException {
        throw notImplemented();
    }

    @Override
    public Object assemble(final Serializable cached, final SessionImplementor session, final Object owner) throws HibernateException {
        throw notImplemented();
    }
}
