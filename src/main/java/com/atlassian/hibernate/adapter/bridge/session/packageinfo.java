/**
 * Classes in this package support session v2 / v side-by-side mode, and are not strictly required
 * for hibernate 5 migration.
 *
 * Classes in the com.atlassian.hibernate.adapter.bridge.session package implement
 * SessionBridge functionality that allow both v2 and v5 Session objects to be used
 * within the same transaction, supporting the HibernateBridgeMode bridge modes that
 * are marked 'Session v2 / v5 side-by-side modes'.
 */
package com.atlassian.hibernate.adapter.bridge.session;
