package com.atlassian.hibernate.adapter.bridge.session;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.query.QueryV2Adapter;
import com.atlassian.hibernate.adapter.adapters.query.QueryV2Supplier;
import com.atlassian.hibernate.adapter.adapters.FlushModeAdapter;
import com.atlassian.hibernate.adapter.adapters.session.SessionV2Adapter;
import com.atlassian.hibernate.util.SessionHelper;
import net.sf.hibernate.FlushMode;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.Transaction;
import org.hibernate.Session;

import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * An adapter bridging the Session interface for hibernate v5 (onwards) to v2, and
 * also delegating to SessionBridge / SessionFactoryBridge to provide alternate Session object versions.
 */
class SessionV2BridgeAdapter extends SessionV2Adapter {
    private final SessionBridge sessionBridge;
    private final boolean compareQueryResults;

    public SessionV2BridgeAdapter(
            final SessionBridge sessionBridge,
            final SessionFactory sessionFactory,
            final boolean compareQueryResults) {

        super(sessionBridge.getRealV5Session(), sessionFactory);
        this.sessionBridge = sessionBridge;
        this.compareQueryResults = compareQueryResults;
    }

    public org.hibernate.Session getV5Session() {
        return super.getV5Session();
    }

    @Override
    public Object get(final Class clazz, final Serializable id) throws HibernateException {
        // this is how SessionFactoryBridge gets to the v5 Session object, even if
        // the Session interface is intercepted by a java or spring proxy
        if (clazz == SessionBridge.class) {
            return sessionBridge;
        }
        return super.get(clazz, id);
    }

    @Override
    public SessionFactory getSessionFactory() {
        return sessionBridge.getHibernateBridge().getV2orV5SessionFactory();
    }

    @Override
    public void save(final Object object, final Serializable id) throws HibernateException {
        try {
            final Session realSession = SessionBridgeAssociations.getSessionBridge(getV5Session()).getRealV5Session();
            SessionHelper.save(realSession, object, id);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Connection connection() throws HibernateException {
        try {
            return sessionBridge.connection();
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV2HibernateException(getSessionFactory(), ex, ex.getMessage());
        }
    }

    @Override
    public boolean isOpen() {
        return sessionBridge.isOpen();
    }

    @Override
    public boolean isConnected() {
        return sessionBridge.isConnected();
    }

    @Override
    public Connection close() throws HibernateException {
        try {
            return sessionBridge.close();
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV2HibernateException(getSessionFactory(), ex, "Cannot close connection");
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Connection disconnect() throws HibernateException {
        try {
            return sessionBridge.disconnect();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void reconnect() throws HibernateException {
        try {
            sessionBridge.reconnect();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void reconnect(final Connection connection) throws HibernateException {
        try {
            sessionBridge.reconnect(connection);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void flush() throws HibernateException {
        try {
            sessionBridge.flush();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    //---------- get/setFlushMode ----------//

    @Override
    public FlushMode getFlushMode() {
        return FlushModeAdapter.adapt(sessionBridge.getHibernateFlushMode());
    }

    @Override
    public void setFlushMode(final FlushMode flushMode) {
        sessionBridge.setHibernateFlushMode(FlushModeAdapter.adapt(flushMode));
    }

    //---------- beginTransaction ----------//

    @Override
    public Transaction beginTransaction() throws HibernateException {
        return sessionBridge.beginTransactionV2();
    }

    @Override
    protected net.sf.hibernate.Query adaptQuery(
            final org.hibernate.Session session,
            final org.hibernate.query.Query query,
            final QueryV2Supplier queryV2Supplier,
            final boolean areParametersOneBased) throws net.sf.hibernate.HibernateException {

        try {
            return QueryV2Adapter.adapt(session, query, compareQueryResults ? queryV2Supplier.get() : null, areParametersOneBased);
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw new org.hibernate.HibernateException(
                    "Exception occurred while constructing v2 query for hibernate v2 v5 list().size() comparison", ex);
        }
    }
}
