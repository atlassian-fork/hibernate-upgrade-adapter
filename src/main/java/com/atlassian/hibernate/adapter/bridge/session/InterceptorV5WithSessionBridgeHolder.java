package com.atlassian.hibernate.adapter.bridge.session;

import com.atlassian.hibernate.adapter.adapters.interceptor.InterceptorV5Adapter;
import com.atlassian.hibernate.adapter.util.DelegatingInteceptorV5;
import org.hibernate.Interceptor;

/**
 * A hibernate Interceptor interface delegator that also holds a reference to a SessionBridge object.
 * This is what HibernateBridge uses to get to the SessionBridge, even if the Session interface
 * is wrapped in a java or spring proxy.
 */
public interface InterceptorV5WithSessionBridgeHolder extends Interceptor {
    SessionBridge getSessionBridge();

    /**
     * Create a SessionBridge holder with a v5 Interceptor implementation adapted around a v2 Interceptor.
     */
    static InterceptorV5WithSessionBridgeHolder create(final SessionBridge sessionBridge, final net.sf.hibernate.Interceptor interceptor) {
        if (interceptor == null) {
            throw new IllegalArgumentException("interceptor cannot be null");
        }
        return new InterceptorV5AdapterAndHolder(sessionBridge, interceptor);
    }

    /**
     * Create a SessionBridge holder wrapping a v5 Interceptor object.
     */
    static InterceptorV5WithSessionBridgeHolder create(final SessionBridge sessionBridge, final org.hibernate.Interceptor interceptor) {
        if (interceptor == null) {
            throw new IllegalArgumentException("interceptor cannot be null");
        }
        return new HolderV5(sessionBridge, interceptor);
    }

    class InterceptorV5AdapterAndHolder extends InterceptorV5Adapter implements InterceptorV5WithSessionBridgeHolder {
        private final SessionBridge sessionBridge;

        public InterceptorV5AdapterAndHolder(final SessionBridge sessionBridge, final net.sf.hibernate.Interceptor interceptor) {
            super(interceptor);
            this.sessionBridge = sessionBridge;
        }

        public SessionBridge getSessionBridge() {
            return sessionBridge;
        }
    }

    class HolderV5 extends DelegatingInteceptorV5 implements InterceptorV5WithSessionBridgeHolder {
        private final SessionBridge sessionBridge;

        public HolderV5(final SessionBridge sessionBridge, final org.hibernate.Interceptor interceptor) {
            super(interceptor);
            this.sessionBridge = sessionBridge;
        }

        public SessionBridge getSessionBridge() {
            return sessionBridge;
        }
    }
}
