package com.atlassian.hibernate.adapter.bridge.session;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.util.DelegatingSessionV5;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SharedSessionBuilder;
import org.hibernate.Transaction;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.jpa.internal.util.FlushModeTypeHelper;

import javax.persistence.FlushModeType;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * The v5 Session that has v5 interface specific behaviour and delegates it's implementation to
 * SessionBridge / SessionFactoryBridge.
 */
class SessionV5BridgeProxy extends DelegatingSessionV5 {
    private final SessionBridge sessionBridge;

    public SessionV5BridgeProxy(final SessionBridge sessionBridge) {
        this.sessionBridge = sessionBridge;
    }

    @Override
    public SessionFactoryImplementor getSessionFactory() {
        return (SessionFactoryImplementor) sessionBridge.getHibernateBridge().getV5SessionFactory();
    }

    @Override
    public Connection connection() throws HibernateException {
        try {
            return sessionBridge.connection();
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV5HibernateException(ex, "Cannot open connection");
        }
    }

    @Override
    public boolean isOpen() {
        return sessionBridge.isOpen();
    }

    @Override
    public boolean isConnected() {
        return sessionBridge.isConnected();
    }

    @Override
    public void close() throws HibernateException {
        try {
            sessionBridge.close();
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV5HibernateException(ex, "Cannot close connection");
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Connection disconnect() throws HibernateException {
        try {
            return sessionBridge.disconnect();
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void reconnect(final Connection connection) throws HibernateException {
        try {
            sessionBridge.reconnect(connection);
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void flush() throws HibernateException {
        try {
            sessionBridge.flush();
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    //---------- Lifecycle ----------//

    @Override
    protected Session createSession() throws HibernateException {
        return sessionBridge.createV5Session();
    }

    @Override
    public boolean hasSession() {
        return super.hasSession();
    }

    @Override
    public Session getWrappedSession() {
        return super.getWrappedSession();
    }

    @Override
    public Session getSessionNoCreate() {
        return super.getSessionNoCreate();
    }

    @Override
    public SharedSessionBuilder sessionWithOptions() {
        throw new NotImplementedException("sessionWithOptions() not implemented");
    }

    //---------- get/setFlushMode ----------//

    @Override
    public FlushModeType getFlushMode() {
        return FlushModeTypeHelper.getFlushModeType(sessionBridge.getHibernateFlushMode());
    }

    @Override
    public void setFlushMode(final FlushModeType flushModeType) {
        sessionBridge.setHibernateFlushMode(FlushModeTypeHelper.getFlushMode(flushModeType));
    }

    @Override
    @Deprecated
    public void setFlushMode(final FlushMode flushMode) {
        sessionBridge.setHibernateFlushMode(flushMode);
    }

    @Override
    public FlushMode getHibernateFlushMode() {
        return sessionBridge.getHibernateFlushMode();
    }

    @Override
    public void setHibernateFlushMode(final FlushMode flushMode) {
        sessionBridge.setHibernateFlushMode(flushMode);
    }

    public void setRealHibernateFlushMode(final FlushMode flushMode) {
        getSessionNoCreate().setHibernateFlushMode(flushMode);
    }

    //---------- beginTransaction ----------//

    @Override
    public Transaction beginTransaction() throws HibernateException {
        return sessionBridge.beginTransactionV5();
    }
}
