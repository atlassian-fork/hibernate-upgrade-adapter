package com.atlassian.hibernate.adapter.bridge.session;

import com.atlassian.hibernate.adapter.HibernateBridgeMode;
import com.atlassian.hibernate.adapter.adapters.interceptor.InterceptorV2Adapter;
import com.atlassian.hibernate.adapter.bridge.factory.SessionFactoryBridge;
import com.atlassian.hibernate.adapter.reflection.SessionFactoryImplV2Reflection;
import com.atlassian.hibernate.adapter.reflection.SessionImplV2Reflection;
import com.atlassian.hibernate.adapter.session.SessionV2Supplier;
import com.atlassian.hibernate.adapter.session.SessionV5Supplier;

import java.sql.Connection;

/**
 * A factory for SessionBridge objects, for when using a Session bridging mode.
 */
public class SessionBridgeFactory {
    private final SessionFactoryBridge sessionFactoryBridge;

    public SessionBridgeFactory(final SessionFactoryBridge sessionFactoryBridge) {
        this.sessionFactoryBridge = sessionFactoryBridge;
    }

    public SessionBridge openSession(final HibernateBridgeMode bridgeMode, final net.sf.hibernate.Interceptor interceptor) {
        return openSession(bridgeMode, null, interceptor);
    }

    public SessionBridge openSession(final HibernateBridgeMode bridgeMode, final Connection connection) {
        return createSessionBridge(
                bridgeMode,
                connection,
                c -> openV2Session(c, null, connection == null),
                this::openV5Session);
    }

    public SessionBridge openSession(
            final HibernateBridgeMode bridgeMode,
            final Connection connection,
            final net.sf.hibernate.Interceptor interceptor) {

        return createSessionBridge(
                bridgeMode,
                connection,
                c -> openV2Session(c, interceptor, connection == null),
                (sessionBridge, c) -> openV5Session(sessionBridge, c, interceptor));
    }

    public SessionBridge openSession(
            final HibernateBridgeMode bridgeMode,
            final Connection connection,
            final org.hibernate.Interceptor interceptor) {

        return createSessionBridge(
                bridgeMode,
                connection,
                c -> openV2Session(c, InterceptorV2Adapter.adapt(sessionFactoryBridge.getSessionFactoryV5(), interceptor), connection == null),
                (sessionBridge, c) -> openV5Session(sessionBridge, c, interceptor));
    }

    public SessionBridge openSession(final HibernateBridgeMode bridgeMode) {
        return createSessionBridge(
                bridgeMode,
                null,
                c -> openV2Session(c, null, true),
                this::openV5Session);
    }

    private SessionBridge createSessionBridge(
            final HibernateBridgeMode bridgeMode,
            final Connection connection,
            final SessionV2Supplier sessionV2Creator,
            final SessionV5Supplier sessionV5Creator) {

        final net.sf.hibernate.connection.ConnectionProvider connectionProvider =
                sessionFactoryBridge.getSessionFactoryV2().getConnectionProvider();
        return new SessionBridge(
                sessionFactoryBridge,
                bridgeMode,
                connectionProvider,
                connection,
                sessionV2Creator,
                sessionV5Creator);
    }

    private net.sf.hibernate.Session openV2Session(
            final Connection connection,
            final net.sf.hibernate.Interceptor interceptor,
            final boolean connectionSuppliedExternally) throws net.sf.hibernate.HibernateException {

        final net.sf.hibernate.Session session;
        if (interceptor == null) {
            session =
                    connection != null
                    ? sessionFactoryBridge.getSessionFactoryV2().openSession(connection)
                    : sessionFactoryBridge.getSessionFactoryV2().openSession();
        } else {
            session =
                    connection != null
                    ? sessionFactoryBridge.getSessionFactoryV2().openSession(connection, interceptor)
                    : sessionFactoryBridge.getSessionFactoryV2().openSession(interceptor);
        }

        // this mirrors v2 hibernate SessionImpl behaviour when the session takes it's connection
        // from ConnectionProvider (see net.sf.hibernate.impl.SessionFactoryImpl line 326)
        if (connectionSuppliedExternally) {
            SessionImplV2Reflection.setTimestamp(
                    (net.sf.hibernate.impl.SessionImpl) session,
                    sessionFactoryBridge.getV2settings().getCacheProvider().nextTimestamp());
        }
        return session;
    }

    private org.hibernate.Session openV5Session(
            final SessionBridge sessionBridge,
            final Connection connection) {

        final net.sf.hibernate.Interceptor defaultInterceptor =
                SessionFactoryImplV2Reflection.getInterceptor((net.sf.hibernate.impl.SessionFactoryImpl) sessionFactoryBridge.getSessionFactoryV2());
        return
                sessionFactoryBridge.getSessionFactoryV5().withOptions()
                        .interceptor(InterceptorV5WithSessionBridgeHolder.create(sessionBridge, defaultInterceptor))
                        .connection(connection)
                        .openSession();
    }

    private org.hibernate.Session openV5Session(
            final SessionBridge sessionBridge,
            final Connection connection,
            final net.sf.hibernate.Interceptor interceptor) {

        return
                sessionFactoryBridge.getSessionFactoryV5().withOptions()
                        .interceptor(InterceptorV5WithSessionBridgeHolder.create(sessionBridge, interceptor))
                        .connection(connection)
                        .openSession();
    }

    @SuppressWarnings("deprecation")
    private org.hibernate.Session openV5Session(
            final SessionBridge sessionBridge,
            final Connection connection,
            final org.hibernate.Interceptor interceptor) {

        final org.hibernate.Interceptor interceptorToUse =
                interceptor != null
                ? interceptor
                : sessionFactoryBridge.getSessionFactoryV5().getInterceptor();
        return
                sessionFactoryBridge.getSessionFactoryV5().withOptions()
                        .interceptor(InterceptorV5WithSessionBridgeHolder.create(sessionBridge, interceptorToUse))
                        .connection(connection)
                        .openSession();
    }
}
