package com.atlassian.hibernate.adapter.bridge.factory;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.HibernateBridgeMode;
import com.atlassian.hibernate.adapter.bridge.session.SessionBridge;
import com.atlassian.hibernate.adapter.bridge.session.SessionBridgeAssociations;
import com.atlassian.hibernate.adapter.reflection.SessionFactoryImplV2Reflection;
import com.atlassian.hibernate.adapter.util.Lazy;
import org.apache.commons.lang3.NotImplementedException;

import java.util.function.Supplier;

/**
 * The v2 / v5 (onwards) SessionFactory bridge.
 * Provide common functionality between the v2 and v5 SessionFactory implementations,
 * and provides alternate Session object versions.
 */
public class SessionFactoryBridge implements HibernateBridge {
    private final Lazy<net.sf.hibernate.engine.SessionFactoryImplementor> sessionFactoryV2;
    private final Lazy<org.hibernate.engine.spi.SessionFactoryImplementor> sessionFactoryV5;

    private Lazy<SessionFactoryV2BridgeProxy> sessionFactoryV2Proxy;
    private Lazy<SessionFactoryV5BridgeProxy> sessionFactoryV5Proxy;
    private Lazy<SessionFactoryV2orV5BridgeProxy> sessionFactoryV2orV5Proxy;
    private net.sf.hibernate.SessionFactory springV2SessionFactory;

    private Lazy<net.sf.hibernate.cfg.Settings> v2settings;
    private final Supplier<HibernateBridgeMode> bridgeMode;
    private final HibernateBridgeImpl hibernateBridge;

    public SessionFactoryBridge(
            final Supplier<net.sf.hibernate.SessionFactory> sessionFactoryV2Supplier,
            final Supplier<org.hibernate.SessionFactory> sessionFactoryV5Supplier,
            final Supplier<HibernateBridgeMode> bridgeMode) {

        this.bridgeMode = bridgeMode;

        sessionFactoryV2 = new Lazy<>(() ->
                (net.sf.hibernate.engine.SessionFactoryImplementor)
                SessionFactoryBridgeAssociations.associate(sessionFactoryV2Supplier.get(), this));
        sessionFactoryV5 = new Lazy<>(() ->
                (org.hibernate.engine.spi.SessionFactoryImplementor)
                SessionFactoryBridgeAssociations.associate(sessionFactoryV5Supplier.get(), this));
        v2settings = new Lazy<>(() ->
                SessionFactoryImplV2Reflection.getSettings(
                        (net.sf.hibernate.impl.SessionFactoryImpl) sessionFactoryV2.get()));

        sessionFactoryV2Proxy = new Lazy<>(() -> new SessionFactoryV2BridgeProxy(this, sessionFactoryV2.get()));
        sessionFactoryV5Proxy = new Lazy<>(() -> new SessionFactoryV5BridgeProxy(this, sessionFactoryV5.get()));
        sessionFactoryV2orV5Proxy = new Lazy<>(() -> new SessionFactoryV2orV5BridgeProxy(
                this,
                sessionFactoryV2::get,
                sessionFactoryV5::get,
                bridgeMode));
        this.hibernateBridge = new HibernateBridgeImpl().setSessionFactories(sessionFactoryV2Proxy::get, sessionFactoryV5Proxy::get, sessionFactoryV2orV5Proxy::get);
    }

    public static SessionFactoryBridge createFromSessionFactoryV5(final org.hibernate.SessionFactory sessionFactory) {
        SessionFactoryBridge bridge = new SessionFactoryBridge(
                () -> {
                    throw new NotImplementedException("v2 SessionFactory not available");
                },
                () -> sessionFactory,
                () -> HibernateBridgeMode.V5_ADAPTER);
        return bridge;
    }

    public HibernateBridgeMode getBridgeMode() {
        return bridgeMode.get();
    }

    public net.sf.hibernate.engine.SessionFactoryImplementor getSessionFactoryV2() {
        return sessionFactoryV2.get();
    }

    public org.hibernate.engine.spi.SessionFactoryImplementor getSessionFactoryV5() {
        return sessionFactoryV5.get();
    }

    public net.sf.hibernate.cfg.Settings getV2settings() {
        return v2settings.get();
    }

    public void close() throws net.sf.hibernate.HibernateException, org.hibernate.HibernateException {
        try {
            org.hibernate.SessionFactory factoryV5 = sessionFactoryV5.getNoAutoCreate();
            if (factoryV5 != null) {
                factoryV5.close();
            }
        } finally {
            net.sf.hibernate.SessionFactory factoryV2 = sessionFactoryV2.getNoAutoCreate();
            if (factoryV2 != null) {
                factoryV2.close();
            }
        }
    }

    //---------- HibernateBridge Implementation ----------//

    @Override
    public net.sf.hibernate.SessionFactory getV2SessionFactory() {
        return sessionFactoryV2Proxy.get();
    }

    @Override
    public org.hibernate.SessionFactory getV5SessionFactory() {
        return sessionFactoryV5Proxy.get();
    }

    @Override
    public net.sf.hibernate.SessionFactory getV2orV5SessionFactory() {
        return sessionFactoryV2orV5Proxy.get();
    }

    @Override
    public net.sf.hibernate.SessionFactory getSpringV2SessionFactory() {
        return springV2SessionFactory != null ? springV2SessionFactory : getV2orV5SessionFactory();
    }

    @Override
    public void setSpringV2SessionFactory(final net.sf.hibernate.SessionFactory sessionFactory) {
        springV2SessionFactory = sessionFactory;
    }

    @Override
    public net.sf.hibernate.Session getV2Session(final net.sf.hibernate.Session session) {
        final SessionBridge sessionBridge = SessionBridgeAssociations.getSessionBridge(session);
        return sessionBridge != null ? sessionBridge.getV2SessionProxy() : null;
    }

    @Override
    public net.sf.hibernate.Session getV2Session(final org.hibernate.Session session) {
        final SessionBridge sessionBridge = SessionBridgeAssociations.getSessionBridge(session);
        return sessionBridge != null ? sessionBridge.getV2SessionProxy() : null;
    }

    @Override
    public org.hibernate.Session getV5Session(final net.sf.hibernate.Session session) {
        org.hibernate.Session sessionV5 = hibernateBridge.getV5Session(session);
        if (!SessionBridgeAssociations.hasSessionBridge(sessionV5)) {
            return sessionV5;
        }

        final SessionBridge sessionBridge = SessionBridgeAssociations.getSessionBridge(session);
        return sessionBridge != null ? sessionBridge.getV5SessionProxy() : null;
    }

    @Override
    public net.sf.hibernate.Session getV2orV5Session(final org.hibernate.Session session) {
        if (!SessionBridgeAssociations.hasSessionBridge(session)) {
            return hibernateBridge.getV2orV5Session(session);
        }

        final SessionBridge sessionBridge = SessionBridgeAssociations.getSessionBridge(session);
        if (sessionBridge == null) {
            return null;
        }
        return
                sessionBridge.getBridgeMode().isV2()
                ? sessionBridge.getV2SessionProxy()
                : sessionBridge.getV2SessionAdapter();
    }
}
