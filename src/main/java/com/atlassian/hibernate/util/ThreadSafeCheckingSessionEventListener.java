package com.atlassian.hibernate.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionEventListener;

public class ThreadSafeCheckingSessionEventListener implements SessionEventListener {
    private static final Log LOG = LogFactory.getLog(ThreadSafeCheckingSessionEventListener.class);
    private final Thread createdThread = Thread.currentThread();
    private static volatile boolean loggedThreadError = false;

    @Override
    public void transactionCompletion(final boolean successful) {
        checkThread();
    }

    @Override
    public void jdbcConnectionAcquisitionStart() {
        checkThread();
    }

    @Override
    public void jdbcConnectionAcquisitionEnd() {
        checkThread();
    }

    @Override
    public void jdbcConnectionReleaseStart() {
        checkThread();
    }

    @Override
    public void jdbcConnectionReleaseEnd() {
        checkThread();
    }

    @Override
    public void jdbcPrepareStatementStart() {
        checkThread();
    }

    @Override
    public void jdbcPrepareStatementEnd() {
        checkThread();
    }

    @Override
    public void jdbcExecuteStatementStart() {
        checkThread();
    }

    @Override
    public void jdbcExecuteStatementEnd() {
        checkThread();
    }

    @Override
    public void jdbcExecuteBatchStart() {
        checkThread();
    }

    @Override
    public void jdbcExecuteBatchEnd() {
        checkThread();
    }

    @Override
    public void cachePutStart() {
        checkThread();
    }

    @Override
    public void cachePutEnd() {
        checkThread();
    }

    @Override
    public void cacheGetStart() {
        checkThread();
    }

    @Override
    public void cacheGetEnd(final boolean hit) {
        checkThread();
    }

    @Override
    public void flushStart() {
        checkThread();
    }

    @Override
    public void flushEnd(final int numberOfEntities, final int numberOfCollections) {
        checkThread();
    }

    @Override
    public void partialFlushStart() {
        checkThread();
    }

    @Override
    public void partialFlushEnd(final int numberOfEntities, final int numberOfCollections) {
        checkThread();
    }

    @Override
    public void dirtyCalculationStart() {
        checkThread();
    }

    @Override
    public void dirtyCalculationEnd(final boolean dirty) {
        checkThread();
    }

    @Override
    public void end() {
        checkThread();
    }

    private void checkThread() {
        if (createdThread != Thread.currentThread()) {
            if (!loggedThreadError) {
                LOG.error("Session called from wrong thread", new Exception());
            }
            loggedThreadError = true;
        }
    }
}
