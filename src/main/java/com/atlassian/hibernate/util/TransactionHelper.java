package com.atlassian.hibernate.util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import javax.transaction.Synchronization;

/**
 * Provides utility methods for creating Transaction objects, emulating hibernate v2 behaviour.
 */
public final class TransactionHelper {

    private TransactionHelper() { }

    /**
     * Begin a hibernate transaction, emulating the v2 hibernate behaviour of restarting the
     * transaction on commit of a 'nested' transaction.
     */
    public static Transaction beginTransactionCommitNested(final org.hibernate.Session session) {
        final Transaction transaction = session.getTransaction();

        // emulate the v2 behaviour
        if (transaction.getStatus() == TransactionStatus.ACTIVE) {
            return new RestartOnCommitTransaction(session, transaction);
        }
        return session.beginTransaction();
    }

    /**
     * A transaction that restarts the transaction on commit.
     */
    private static class RestartOnCommitTransaction implements Transaction {
        private final Session session;
        private Transaction transaction;

        public RestartOnCommitTransaction(final Session session, final Transaction transaction) {
            this.session = session;
            this.transaction = transaction;
        }

        @Override
        public void commit() {
            transaction.commit();

            // restart the transaction, as per the hibernate v2 behaviour
            transaction = session.beginTransaction();
        }

        //---------- Call through methods ----------//

        @Override
        public void begin() {
            transaction.begin();
        }

        @Override
        public void rollback() {
            transaction.rollback();
        }

        @Override
        public TransactionStatus getStatus() {
            return transaction.getStatus();
        }

        @Override
        public void registerSynchronization(final Synchronization synchronization) throws HibernateException {
            transaction.registerSynchronization(synchronization);
        }

        @Override
        public void setTimeout(final int seconds) {
            transaction.setTimeout(seconds);
        }

        @Override
        public int getTimeout() {
            return transaction.getTimeout();
        }

        @Override
        public boolean getRollbackOnly() {
            return transaction.getRollbackOnly();
        }

        @Override
        public void setRollbackOnly() {
            transaction.setRollbackOnly();
        }

        @Override
        public void markRollbackOnly() {
            transaction.markRollbackOnly();
        }

        @Override
        public boolean isActive() {
            return transaction.isActive();
        }
    }
}
