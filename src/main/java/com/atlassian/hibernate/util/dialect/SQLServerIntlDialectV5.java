package com.atlassian.hibernate.util.dialect;

import org.hibernate.dialect.SQLServer2005Dialect;

import java.sql.Types;

/**
 * A dialect for MS SQL Server 2005 onwards, which supports Unicode characters.
 * <p>
 * Copied from from atlassian:hibernate-2.1.8 for use with hibernate 5.
 */
public class SQLServerIntlDialectV5 extends SQLServer2005Dialect {
    private static final int MAX_LENGTH = 8000;

    public SQLServerIntlDialectV5() {
        super();
        registerColumnType(Types.CHAR, "nchar(1)");
        registerColumnType(Types.VARCHAR, "nvarchar(MAX)");
        registerColumnType(Types.VARCHAR, MAX_LENGTH, "nvarchar($l)");
        registerColumnType(Types.CLOB, "ntext");
    }
}
