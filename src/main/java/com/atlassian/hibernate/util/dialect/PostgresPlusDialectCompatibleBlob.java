package com.atlassian.hibernate.util.dialect;

import org.hibernate.dialect.PostgresPlusDialect;

import java.sql.Types;

/**
 * A PostgresPlusDialect that uses bytea instead of oid for the BLOB data type.
 */
public class PostgresPlusDialectCompatibleBlob extends PostgresPlusDialect {
    public PostgresPlusDialectCompatibleBlob() {
        super();
        // hibernate 5 uses oid, but hibernate 2 uses bytea
        registerColumnType(Types.BLOB, "bytea");
    }
}
