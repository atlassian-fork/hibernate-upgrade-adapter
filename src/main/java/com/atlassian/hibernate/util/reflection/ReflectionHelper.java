package com.atlassian.hibernate.util.reflection;

import com.atlassian.hibernate.util.ThrowableUtil;

import java.lang.reflect.Field;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Helper methods for using java reflection efficiently.
 */
public class ReflectionHelper {

    /**
     * Get the singleton instance of this object.
     */
    public static final ReflectionHelper INSTANCE = create();

    protected ReflectionHelper() { }

    private static ReflectionHelper create() {
        try {
            final String unsafeImpl = ReflectionHelper.class.getPackage() + ".UnsafeReflectionHelper";
            return (ReflectionHelper) Class.forName(unsafeImpl).newInstance();
        } catch (final Exception ex) {
            return new ReflectionHelper();
        }
    }

    /**
     * Get a function that returns the value of a private field, without throwing checked exceptions.
     */
    public Function<Object, Object> getPrivateFieldGetter(final Class clazz, final String fieldName) {
        try {
            final Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);

            return obj -> {
                try {
                    return field.get(obj);
                } catch (final IllegalAccessException ex) {
                    throw ThrowableUtil.propagateAll(ex);
                }
            };
        } catch (NoSuchFieldException | SecurityException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    /**
     * Get a function that sets the value of a private field, without throwing checked exceptions.
     */
    public BiConsumer<Object, Object> getPrivateFieldSetter(final Class clazz, final String fieldName) {
        try {
            final Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);

            return (obj, value) -> {
                try {
                    field.set(obj, value);
                } catch (final IllegalAccessException ex) {
                    throw ThrowableUtil.propagateAll(ex);
                }
            };
        } catch (NoSuchFieldException | SecurityException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }
}
