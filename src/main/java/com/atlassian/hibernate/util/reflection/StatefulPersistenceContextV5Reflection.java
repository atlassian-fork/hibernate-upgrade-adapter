package com.atlassian.hibernate.util.reflection;

import org.hibernate.collection.spi.PersistentCollection;
import org.hibernate.engine.internal.StatefulPersistenceContext;
import org.hibernate.engine.spi.CollectionEntry;
import org.hibernate.engine.spi.CollectionKey;
import org.hibernate.engine.spi.EntityKey;
import org.hibernate.engine.spi.EntityUniqueKey;
import org.hibernate.internal.util.collections.IdentityMap;

import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiConsumer;
import java.util.function.Function;

public final class StatefulPersistenceContextV5Reflection {
    private StatefulPersistenceContextV5Reflection() { }

    private static final Function<Object, Object> UNOWNED_COLLECTIONS_GETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(StatefulPersistenceContext.class, "unownedCollections");
    private static final BiConsumer<Object, Object> ARRAY_HOLDERS_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(StatefulPersistenceContext.class, "arrayHolders");
    private static final BiConsumer<Object, Object> ENTITIES_BY_KEY_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(StatefulPersistenceContext.class, "entitiesByKey");
    private static final BiConsumer<Object, Object> ENTITIES_BY_UNIQUE_KEY_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(StatefulPersistenceContext.class, "entitiesByUniqueKey");
    private static final BiConsumer<Object, Object> PARENTS_BY_CHILD_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(StatefulPersistenceContext.class, "parentsByChild");
    private static final BiConsumer<Object, Object> ENTITY_SNAPSHOTS_BY_KEY_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(StatefulPersistenceContext.class, "entitySnapshotsByKey");
    private static final BiConsumer<Object, Object> COLLECTIONS_BY_KEY_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(StatefulPersistenceContext.class, "collectionsByKey");
    private static final BiConsumer<Object, Object> COLLECTION_ENTRIES_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(StatefulPersistenceContext.class, "collectionEntries");
    private static final BiConsumer<Object, Object> UNOWNED_COLLECTIONS_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(StatefulPersistenceContext.class, "unownedCollections");
    private static final BiConsumer<Object, Object> PROXIES_BY_KEY_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(StatefulPersistenceContext.class, "proxiesByKey");
    private static final BiConsumer<Object, Object> NULLIFIED_ENTITY_KEYS_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(StatefulPersistenceContext.class, "nullifiableEntityKeys");

    public static void setArrayHolders(
            final StatefulPersistenceContext obj,
            final Map<Object, PersistentCollection> value) {
        ARRAY_HOLDERS_SETTER.accept(obj, value);
    }

    public static void setEntitiesByKey(
            final StatefulPersistenceContext obj,
            final Map<EntityKey, Object> value) {
        ENTITIES_BY_KEY_SETTER.accept(obj, value);
    }

    public static void setEntitiesByUniqueKey(
            final StatefulPersistenceContext obj,
            final Map<EntityUniqueKey, Object> value) {
        ENTITIES_BY_UNIQUE_KEY_SETTER.accept(obj, value);
    }

    public static void setParentsByChild(
            final StatefulPersistenceContext obj,
            final Map<Object, Object> value) {
        PARENTS_BY_CHILD_SETTER.accept(obj, value);
    }

    public static void setEntitySnapshotsByKey(
            final StatefulPersistenceContext obj,
            final Map<EntityKey, Object> value) {
        ENTITY_SNAPSHOTS_BY_KEY_SETTER.accept(obj, value);
    }

    public static void setCollectionsByKey(
            final StatefulPersistenceContext obj,
            final Map<CollectionKey, PersistentCollection> value) {
        COLLECTIONS_BY_KEY_SETTER.accept(obj, value);
    }

    public static void setCollectionEntries(
            final StatefulPersistenceContext obj,
            final IdentityMap<PersistentCollection, CollectionEntry> value) {
        COLLECTION_ENTRIES_SETTER.accept(obj, value);
    }

    @SuppressWarnings("unchecked")
    public static Map<CollectionKey, PersistentCollection> getUnownedCollections(
            final StatefulPersistenceContext obj) {
        return (Map<CollectionKey, PersistentCollection>) UNOWNED_COLLECTIONS_GETTER.apply(obj);
    }

    public static void setUnownedCollections(
            final StatefulPersistenceContext obj,
            final Map<CollectionKey, PersistentCollection> value) {
        UNOWNED_COLLECTIONS_SETTER.accept(obj, value);
    }

    public static void setProxiesByKey(
            final StatefulPersistenceContext obj,
            final ConcurrentMap<EntityKey, Object> value) {
        PROXIES_BY_KEY_SETTER.accept(obj, value);
    }

    public static void setNullifiableEntityKeys(
            final StatefulPersistenceContext obj,
            final HashSet<EntityKey> value) {
        NULLIFIED_ENTITY_KEYS_SETTER.accept(obj, value);
    }
}
