package com.atlassian.hibernate.util;

import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.Filter;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.IdentifierLoadAccess;
import org.hibernate.Interceptor;
import org.hibernate.LobHelper;
import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.MultiIdentifierLoadAccess;
import org.hibernate.NaturalIdLoadAccess;
import org.hibernate.ReplicationMode;
import org.hibernate.ScrollMode;
import org.hibernate.Session;
import org.hibernate.SessionEventListener;
import org.hibernate.SharedSessionBuilder;
import org.hibernate.SimpleNaturalIdLoadAccess;
import org.hibernate.Transaction;
import org.hibernate.TypeHelper;
import org.hibernate.UnknownProfileException;
import org.hibernate.collection.spi.PersistentCollection;
import org.hibernate.engine.jdbc.LobCreator;
import org.hibernate.engine.jdbc.connections.spi.JdbcConnectionAccess;
import org.hibernate.engine.jdbc.spi.JdbcCoordinator;
import org.hibernate.engine.jdbc.spi.JdbcServices;
import org.hibernate.engine.query.spi.sql.NativeSQLQuerySpecification;
import org.hibernate.engine.spi.ActionQueue;
import org.hibernate.engine.spi.EntityEntry;
import org.hibernate.engine.spi.EntityKey;
import org.hibernate.engine.spi.ExceptionConverter;
import org.hibernate.engine.spi.LoadQueryInfluencers;
import org.hibernate.engine.spi.PersistenceContext;
import org.hibernate.engine.spi.QueryParameters;
import org.hibernate.engine.spi.SessionEventListenerManager;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.event.spi.EventSource;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.hibernate.loader.custom.CustomQuery;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.spi.NativeQueryImplementor;
import org.hibernate.query.spi.QueryImplementor;
import org.hibernate.query.spi.ScrollableResultsImplementor;
import org.hibernate.resource.jdbc.spi.JdbcSessionContext;
import org.hibernate.resource.transaction.spi.TransactionCoordinator;
import org.hibernate.stat.SessionStatistics;
import org.hibernate.type.descriptor.sql.SqlTypeDescriptor;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManagerFactory;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Selection;
import javax.persistence.metamodel.Metamodel;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Base class for v5 Session implementations that wish to implement only parts of that contract
 * themselves while forwarding other method invocations to a delegate instance.
 */
public abstract class DelegatingSessionV5 implements Session, SessionImplementor, EventSource {
    private Session session;

    protected DelegatingSessionV5() {
    }

    /**
     * Get or create the wrapped Session object.
     */
    protected Session getWrappedSession() throws HibernateException {
        if (session == null) {
            session = createSession();
        }
        return session;
    }

    protected SessionImplementor getSessionImplementor() throws HibernateException {
        return (SessionImplementor) getWrappedSession();
    }

    protected Session getSessionNoCreate() {
        return session;
    }

    protected boolean hasSession() {
        return session != null;
    }

    /**
     * Create the wrapped Session object.
     */
    protected Session createSession() throws HibernateException {
        throw new NotImplementedException("Override createSession() or pass a session into the constructor");
    }

    //---------- Session / SessionImplementor ----------//

    @Override
    public <T> T execute(final Callback<T> callback) {
        return getSessionImplementor().execute(callback);
    }

    @Override
    public String getTenantIdentifier() {
        return getWrappedSession().getTenantIdentifier();
    }

    @Override
    public UUID getSessionIdentifier() {
        return getSessionImplementor().getSessionIdentifier();
    }

    @Override
    public JdbcConnectionAccess getJdbcConnectionAccess() {
        return getSessionImplementor().getJdbcConnectionAccess();
    }

    @Override
    public EntityKey generateEntityKey(final Serializable id, final EntityPersister persister) {
        return getSessionImplementor().generateEntityKey(id, persister);
    }

    @Override
    public Interceptor getInterceptor() {
        return getSessionImplementor().getInterceptor();
    }

    @Override
    public void setAutoClear(final boolean enabled) {
        getSessionImplementor().setAutoClear(enabled);
    }

    @Override
    public boolean isTransactionInProgress() {
        return getSessionImplementor().isTransactionInProgress();
    }

    @Override
    @Deprecated
    public LockOptions getLockRequest(final LockModeType lockModeType, final Map<String, Object> properties) {
        return getSessionImplementor().getLockRequest(lockModeType, properties);
    }

    @Override
    public LockOptions buildLockOptions(final LockModeType lockModeType, final Map<String, Object> properties) {
        return getSessionImplementor().buildLockOptions(lockModeType, properties);
    }

    @Override
    @Deprecated
    public <T> QueryImplementor<T> createQuery(
            final String jpaqlString,
            final Class<T> resultClass,
            final Selection selection,
            final QueryOptions queryOptions) {
        return getSessionImplementor().createQuery(jpaqlString, resultClass, selection, queryOptions);
    }

    @Override
    public void initializeCollection(final PersistentCollection collection, final boolean writing) throws HibernateException {
        getSessionImplementor().initializeCollection(collection, writing);
    }

    @Override
    public Object internalLoad(final String entityName, final Serializable id, final boolean eager, final boolean nullable) throws HibernateException {
        return getSessionImplementor().internalLoad(entityName, id, eager, nullable);
    }

    @Override
    public Object immediateLoad(final String entityName, final Serializable id) throws HibernateException {
        return getSessionImplementor().immediateLoad(entityName, id);
    }

    @Override
    public long getTimestamp() {
        return getSessionImplementor().getTimestamp();
    }

    @Override
    public SessionFactoryImplementor getFactory() {
        return getSessionImplementor().getFactory();
    }

    @Override
    public List list(final String query, final QueryParameters queryParameters) throws HibernateException {
        return getSessionImplementor().list(query, queryParameters);
    }

    @Override
    public Iterator iterate(final String query, final QueryParameters queryParameters) throws HibernateException {
        return getSessionImplementor().iterate(query, queryParameters);
    }

    @Override
    public ScrollableResultsImplementor scroll(final String query, final QueryParameters queryParameters) throws HibernateException {
        return getSessionImplementor().scroll(query, queryParameters);
    }

    @Override
    public ScrollableResultsImplementor scroll(final Criteria criteria, final ScrollMode scrollMode) {
        return getSessionImplementor().scroll(criteria, scrollMode);
    }

    @Override
    public List list(final Criteria criteria) {
        return getSessionImplementor().list(criteria);
    }

    @Override
    public List listFilter(final Object collection, final String filter, final QueryParameters queryParameters)
            throws HibernateException {
        return getSessionImplementor().listFilter(collection, filter, queryParameters);
    }

    @Override
    public Iterator iterateFilter(final Object collection, final String filter, final QueryParameters queryParameters)
            throws HibernateException {
        return getSessionImplementor().iterateFilter(collection, filter, queryParameters);
    }

    @Override
    public EntityPersister getEntityPersister(final String entityName, final Object object) throws HibernateException {
        return getSessionImplementor().getEntityPersister(entityName, object);
    }

    @Override
    public Object getEntityUsingInterceptor(final EntityKey key) throws HibernateException {
        return getSessionImplementor().getEntityUsingInterceptor(key);
    }

    @Override
    public Serializable getContextEntityIdentifier(final Object object) {
        return getSessionImplementor().getContextEntityIdentifier(object);
    }

    @Override
    public String bestGuessEntityName(final Object object) {
        return getSessionImplementor().bestGuessEntityName(object);
    }

    @Override
    public String guessEntityName(final Object entity) throws HibernateException {
        return getSessionImplementor().guessEntityName(entity);
    }

    @Override
    public Object instantiate(final String entityName, final Serializable id) throws HibernateException {
        return getSessionImplementor().instantiate(entityName, id);
    }

    @Override
    public List listCustomQuery(final CustomQuery customQuery, final QueryParameters queryParameters)
            throws HibernateException {
        return getSessionImplementor().listCustomQuery(customQuery, queryParameters);
    }

    @Override
    public ScrollableResultsImplementor scrollCustomQuery(final CustomQuery customQuery, final QueryParameters queryParameters)
            throws HibernateException {
        return getSessionImplementor().scrollCustomQuery(customQuery, queryParameters);
    }

    @Override
    public List list(final NativeSQLQuerySpecification spec, final QueryParameters queryParameters) throws HibernateException {
        return getSessionImplementor().list(spec, queryParameters);
    }

    @Override
    public ScrollableResultsImplementor scroll(final NativeSQLQuerySpecification spec, final QueryParameters queryParameters)
            throws HibernateException {
        return getSessionImplementor().scroll(spec, queryParameters);
    }

    @Override
    public int getDontFlushFromFind() {
        return getSessionImplementor().getDontFlushFromFind();
    }

    @Override
    public PersistenceContext getPersistenceContext() {
        return getSessionImplementor().getPersistenceContext();
    }

    @Override
    public int executeUpdate(final String query, final QueryParameters queryParameters) throws HibernateException {
        return getSessionImplementor().executeUpdate(query, queryParameters);
    }

    @Override
    public int executeNativeUpdate(final NativeSQLQuerySpecification specification, final QueryParameters queryParameters)
            throws HibernateException {
        return getSessionImplementor().executeNativeUpdate(specification, queryParameters);
    }

    @Override
    public CacheMode getCacheMode() {
        return getWrappedSession().getCacheMode();
    }

    @Override
    public void setCacheMode(final CacheMode cm) {
        getWrappedSession().setCacheMode(cm);
    }

    @Override
    public boolean isOpen() {
        return getWrappedSession().isOpen();
    }

    @Override
    public boolean isConnected() {
        return getWrappedSession().isConnected();
    }

    @Override
    public void checkOpen(final boolean markForRollbackIfClosed) {
        getSessionImplementor().checkOpen(markForRollbackIfClosed);
    }

    @Override
    public void markForRollbackOnly() {
        getSessionImplementor().markForRollbackOnly();
    }

    @Override
    public FlushModeType getFlushMode() {
        return getWrappedSession().getFlushMode();
    }

    @Override
    public void setFlushMode(final FlushModeType flushModeType) {
        getWrappedSession().setFlushMode(flushModeType);
    }

    @Override
    public void setHibernateFlushMode(final FlushMode flushMode) {
        getWrappedSession().setHibernateFlushMode(flushMode);
    }

    @Override
    public FlushMode getHibernateFlushMode() {
        return getWrappedSession().getHibernateFlushMode();
    }

    @Override
    @SuppressWarnings("deprecation")
    public void setFlushMode(final FlushMode fm) {
        getWrappedSession().setHibernateFlushMode(fm);
    }

    @Override
    public void lock(final Object entity, final LockModeType lockMode) {
        getWrappedSession().lock(entity, lockMode);
    }

    @Override
    public void lock(final Object entity, final LockModeType lockMode, final Map<String, Object> properties) {
        getWrappedSession().lock(entity, lockMode, properties);
    }

    @Override
    public Connection connection() {
        return getSessionImplementor().connection();
    }

    @Override
    public void flush() {
        getWrappedSession().flush();
    }

    @Override
    public boolean isEventSource() {
        return getSessionImplementor().isEventSource();
    }

    @Override
    public void afterScrollOperation() {
        getSessionImplementor().afterScrollOperation();
    }

    @Override
    public TransactionCoordinator getTransactionCoordinator() {
        return getSessionImplementor().getTransactionCoordinator();
    }

    @Override
    public JdbcCoordinator getJdbcCoordinator() {
        return getSessionImplementor().getJdbcCoordinator();
    }

    @Override
    public JdbcServices getJdbcServices() {
        return getSessionImplementor().getJdbcServices();
    }

    @Override
    public JdbcSessionContext getJdbcSessionContext() {
        return getSessionImplementor().getJdbcSessionContext();
    }

    @Override
    public boolean isClosed() {
        return getSessionImplementor().isClosed();
    }

    @Override
    public boolean shouldAutoClose() {
        return getSessionImplementor().shouldAutoClose();
    }

    @Override
    public boolean isAutoCloseSessionEnabled() {
        return getSessionImplementor().isAutoCloseSessionEnabled();
    }

    @Override
    public boolean shouldAutoJoinTransaction() {
        return getSessionImplementor().shouldAutoJoinTransaction();
    }

    @Override
    public LoadQueryInfluencers getLoadQueryInfluencers() {
        return getSessionImplementor().getLoadQueryInfluencers();
    }

    @Override
    public ExceptionConverter getExceptionConverter() {
        return getSessionImplementor().getExceptionConverter();
    }

    @Override
    public SessionEventListenerManager getEventListenerManager() {
        return getSessionImplementor().getEventListenerManager();
    }

    @Override
    public Transaction accessTransaction() {
        return getSessionImplementor().accessTransaction();
    }

    @Override
    public Transaction beginTransaction() {
        return getWrappedSession().beginTransaction();
    }

    @Override
    public Transaction getTransaction() {
        return getWrappedSession().getTransaction();
    }

    @Override
    public void afterTransactionBegin() {
        getSessionImplementor().afterTransactionBegin();
    }

    @Override
    public void beforeTransactionCompletion() {
        getSessionImplementor().beforeTransactionCompletion();
    }

    @Override
    public void afterTransactionCompletion(final boolean successful, final boolean delayed) {
        getSessionImplementor().afterTransactionCompletion(successful, delayed);
    }

    @Override
    public void flushBeforeTransactionCompletion() {
        getSessionImplementor().flushBeforeTransactionCompletion();
    }

    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        return getSessionImplementor().getFactory();
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        return getWrappedSession().getCriteriaBuilder();
    }

    @Override
    public Metamodel getMetamodel() {
        return getWrappedSession().getMetamodel();
    }

    @Override
    public <T> EntityGraph<T> createEntityGraph(final Class<T> rootType) {
        return getWrappedSession().createEntityGraph(rootType);
    }

    @Override
    public EntityGraph<?> createEntityGraph(final String graphName) {
        return getWrappedSession().createEntityGraph(graphName);
    }

    @Override
    public EntityGraph<?> getEntityGraph(final String graphName) {
        return getWrappedSession().getEntityGraph(graphName);
    }

    @Override
    public <T> List<EntityGraph<? super T>> getEntityGraphs(final Class<T> entityClass) {
        return getWrappedSession().getEntityGraphs(entityClass);
    }

    @Override
    public QueryImplementor getNamedQuery(final String name) {
        return getSessionImplementor().getNamedQuery(name);
    }

    @Override
    @Deprecated
    public NativeQueryImplementor getNamedSQLQuery(final String name) {
        return getSessionImplementor().getNamedSQLQuery(name);
    }

    @Override
    public NativeQueryImplementor getNamedNativeQuery(final String name) {
        return getSessionImplementor().getNamedNativeQuery(name);
    }

    @Override
    public QueryImplementor createQuery(final String queryString) {
        return getSessionImplementor().createQuery(queryString);
    }

    @Override
    public <T> QueryImplementor<T> createQuery(final String queryString, final Class<T> resultType) {
        return getSessionImplementor().createQuery(queryString, resultType);
    }

    @Override
    public <T> QueryImplementor<T> createQuery(final CriteriaQuery<T> criteriaQuery) {
        return getSessionImplementor().createQuery(criteriaQuery);
    }

    @Override
    public QueryImplementor createQuery(final CriteriaUpdate updateQuery) {
        return getSessionImplementor().createQuery(updateQuery);
    }

    @Override
    public QueryImplementor createQuery(final CriteriaDelete deleteQuery) {
        return getSessionImplementor().createQuery(deleteQuery);
    }

    @Override
    public QueryImplementor createNamedQuery(final String name) {
        return getSessionImplementor().createNamedQuery(name);
    }

    @Override
    public <T> QueryImplementor<T> createNamedQuery(final String name, final Class<T> resultClass) {
        return getSessionImplementor().createNamedQuery(name, resultClass);
    }

    @Override
    public NativeQueryImplementor createNativeQuery(final String sqlString) {
        return getSessionImplementor().createNativeQuery(sqlString);
    }

    @Override
    @SuppressWarnings("unchecked")
    public NativeQueryImplementor createNativeQuery(final String sqlString, final Class resultClass) {
        return getSessionImplementor().createNativeQuery(sqlString, resultClass);
    }

    @Override
    public NativeQueryImplementor createNativeQuery(final String sqlString, final String resultSetMapping) {
        return getSessionImplementor().createNativeQuery(sqlString, resultSetMapping);
    }

    @Override
    public StoredProcedureQuery createNamedStoredProcedureQuery(final String name) {
        return getWrappedSession().createNamedStoredProcedureQuery(name);
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(final String procedureName) {
        return getWrappedSession().createNamedStoredProcedureQuery(procedureName);
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(final String procedureName, final Class... resultClasses) {
        return getWrappedSession().createStoredProcedureQuery(procedureName, resultClasses);
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(final String procedureName, final String... resultSetMappings) {
        return getWrappedSession().createStoredProcedureQuery(procedureName, resultSetMappings);
    }

    @Override
    public void joinTransaction() {
        getWrappedSession().joinTransaction();
    }

    @Override
    public boolean isJoinedToTransaction() {
        return getWrappedSession().isJoinedToTransaction();
    }

    @Override
    public <T> T unwrap(final Class<T> cls) {
        return getWrappedSession().unwrap(cls);
    }

    @Override
    public Object getDelegate() {
        return getWrappedSession();
    }

    @Override
    @Deprecated
    public NativeQueryImplementor createSQLQuery(final String queryString) {
        return getSessionImplementor().createSQLQuery(queryString);
    }

    @Override
    public ProcedureCall getNamedProcedureCall(final String name) {
        return getWrappedSession().getNamedProcedureCall(name);
    }

    @Override
    public ProcedureCall createStoredProcedureCall(final String procedureName) {
        return getWrappedSession().createStoredProcedureCall(procedureName);
    }

    @Override
    public ProcedureCall createStoredProcedureCall(final String procedureName, final Class... resultClasses) {
        return getWrappedSession().createStoredProcedureCall(procedureName, resultClasses);
    }

    @Override
    public ProcedureCall createStoredProcedureCall(final String procedureName, final String... resultSetMappings) {
        return getWrappedSession().createStoredProcedureCall(procedureName, resultSetMappings);
    }

    @Override
    @Deprecated
    public Criteria createCriteria(final Class persistentClass) {
        return getWrappedSession().createCriteria(persistentClass);
    }

    @Override
    @Deprecated
    public Criteria createCriteria(final Class persistentClass, final String alias) {
        return getWrappedSession().createCriteria(persistentClass, alias);
    }

    @Override
    @Deprecated
    public Criteria createCriteria(final String entityName) {
        return getWrappedSession().createCriteria(entityName);
    }

    @Override
    @Deprecated
    public Criteria createCriteria(final String entityName, final String alias) {
        return getWrappedSession().createCriteria(entityName, alias);
    }

    @Override
    public SharedSessionBuilder sessionWithOptions() {
        return getWrappedSession().sessionWithOptions();
    }

    @Override
    public SessionFactoryImplementor getSessionFactory() {
        return getSessionImplementor().getSessionFactory();
    }

    @Override
    public void close() throws HibernateException {
        getWrappedSession().close();
    }

    @Override
    public void cancelQuery() throws HibernateException {
        getWrappedSession().cancelQuery();
    }

    @Override
    public boolean isDirty() throws HibernateException {
        return getWrappedSession().isDirty();
    }

    @Override
    public boolean isDefaultReadOnly() {
        return getWrappedSession().isDefaultReadOnly();
    }

    @Override
    public void setDefaultReadOnly(final boolean readOnly) {
        getWrappedSession().setDefaultReadOnly(readOnly);
    }

    @Override
    public Serializable getIdentifier(final Object object) {
        return getWrappedSession().getIdentifier(object);
    }

    @Override
    public boolean contains(final String entityName, final Object object) {
        return getWrappedSession().contains(entityName, object);
    }

    @Override
    public boolean contains(final Object object) {
        return getWrappedSession().contains(object);
    }

    @Override
    public LockModeType getLockMode(final Object entity) {
        return getWrappedSession().getLockMode(entity);
    }

    @Override
    public void setProperty(final String propertyName, final Object value) {
        getWrappedSession().setProperty(propertyName, value);
    }

    @Override
    public Map<String, Object> getProperties() {
        return getWrappedSession().getProperties();
    }

    @Override
    public void evict(final Object object) {
        getWrappedSession().evict(object);
    }

    @Override
    public <T> T load(final Class<T> theClass, final Serializable id, final LockMode lockMode) {
        return getWrappedSession().load(theClass, id, lockMode);
    }

    @Override
    public <T> T load(final Class<T> theClass, final Serializable id, final LockOptions lockOptions) {
        return getWrappedSession().load(theClass, id, lockOptions);
    }

    @Override
    public Object load(final String entityName, final Serializable id, final LockMode lockMode) {
        return getWrappedSession().load(entityName, id, lockMode);
    }

    @Override
    public Object load(final String entityName, final Serializable id, final LockOptions lockOptions) {
        return getWrappedSession().load(entityName, id, lockOptions);
    }

    @Override
    public <T> T load(final Class<T> theClass, final Serializable id) {
        return getWrappedSession().load(theClass, id);
    }

    @Override
    public Object load(final String entityName, final Serializable id) {
        return getWrappedSession().load(entityName, id);
    }

    @Override
    public void load(final Object object, final Serializable id) {
        getWrappedSession().load(object, id);
    }

    @Override
    public void replicate(final Object object, final ReplicationMode replicationMode) {
        getWrappedSession().replicate(object, replicationMode);
    }

    @Override
    public void replicate(final String entityName, final Object object, final ReplicationMode replicationMode) {
        getWrappedSession().replicate(entityName, object, replicationMode);
    }

    @Override
    public Serializable save(final Object object) {
        return getWrappedSession().save(object);
    }

    @Override
    public Serializable save(final String entityName, final Object object) {
        return getWrappedSession().save(entityName, object);
    }

    @Override
    public void saveOrUpdate(final Object object) {
        getWrappedSession().saveOrUpdate(object);
    }

    @Override
    public void saveOrUpdate(final String entityName, final Object object) {
        getWrappedSession().saveOrUpdate(entityName, object);
    }

    @Override
    public void update(final Object object) {
        getWrappedSession().update(object);
    }

    @Override
    public void update(final String entityName, final Object object) {
        getWrappedSession().update(entityName, object);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object merge(final Object object) {
        return getWrappedSession().merge(object);
    }

    @Override
    public Object merge(final String entityName, final Object object) {
        return getWrappedSession().merge(entityName, object);
    }

    @Override
    public void persist(final Object object) {
        getWrappedSession().persist(object);
    }

    @Override
    public void remove(final Object entity) {
        getWrappedSession().remove(entity);
    }

    @Override
    public <T> T find(final Class<T> entityClass, final Object primaryKey) {
        return getWrappedSession().find(entityClass, primaryKey);
    }

    @Override
    public <T> T find(final Class<T> entityClass, final Object primaryKey, final Map<String, Object> properties) {
        return getWrappedSession().find(entityClass, primaryKey, properties);
    }

    @Override
    public <T> T find(final Class<T> entityClass, final Object primaryKey, final LockModeType lockMode) {
        return getWrappedSession().find(entityClass, primaryKey, lockMode);
    }

    @Override
    public <T> T find(final Class<T> entityClass, final Object primaryKey, final LockModeType lockMode, final Map<String, Object> properties) {
        return getWrappedSession().find(entityClass, primaryKey, lockMode, properties);
    }

    @Override
    public <T> T getReference(final Class<T> entityClass, final Object primaryKey) {
        return getWrappedSession().getReference(entityClass, primaryKey);
    }

    @Override
    public void persist(final String entityName, final Object object) {
        getWrappedSession().persist(entityName, object);
    }

    @Override
    public void delete(final Object object) {
        getWrappedSession().delete(object);
    }

    @Override
    public void delete(final String entityName, final Object object) {
        getWrappedSession().delete(entityName, object);
    }

    @Override
    public void lock(final Object object, final LockMode lockMode) {
        getWrappedSession().lock(object, lockMode);
    }

    @Override
    public void lock(final String entityName, final Object object, final LockMode lockMode) {
        getWrappedSession().lock(entityName, object, lockMode);
    }

    @Override
    public LockRequest buildLockRequest(final LockOptions lockOptions) {
        return getWrappedSession().buildLockRequest(lockOptions);
    }

    @Override
    public void refresh(final Object object) {
        getWrappedSession().refresh(object);
    }

    @Override
    public void refresh(final Object entity, final Map<String, Object> properties) {
        getWrappedSession().refresh(entity, properties);
    }

    @Override
    public void refresh(final Object entity, final LockModeType lockMode) {
        getWrappedSession().refresh(entity, lockMode);
    }

    @Override
    public void refresh(final Object entity, final LockModeType lockMode, final Map<String, Object> properties) {
        getWrappedSession().refresh(entity, lockMode, properties);
    }

    @Override
    public void refresh(final String entityName, final Object object) {
        getWrappedSession().refresh(entityName, object);
    }

    @Override
    public void refresh(final Object object, final LockMode lockMode) {
        getWrappedSession().refresh(object, lockMode);
    }

    @Override
    public void refresh(final Object object, final LockOptions lockOptions) {
        getWrappedSession().refresh(object, lockOptions);
    }

    @Override
    public void refresh(final String entityName, final Object object, final LockOptions lockOptions) {
        getWrappedSession().refresh(entityName, object, lockOptions);
    }

    @Override
    public LockMode getCurrentLockMode(final Object object) {
        return getWrappedSession().getCurrentLockMode(object);
    }

    @Override
    public org.hibernate.query.Query createFilter(final Object collection, final String queryString) {
        return getWrappedSession().createFilter(collection, queryString);
    }

    @Override
    public void clear() {
        getWrappedSession().clear();
    }

    @Override
    public void detach(final Object entity) {
        getWrappedSession().detach(entity);
    }

    @Override
    public <T> T get(final Class<T> theClass, final Serializable id) {
        return getWrappedSession().get(theClass, id);
    }

    @Override
    public <T> T get(final Class<T> theClass, final Serializable id, final LockMode lockMode) {
        return getWrappedSession().get(theClass, id, lockMode);
    }

    @Override
    public <T> T get(final Class<T> theClass, final Serializable id, final LockOptions lockOptions) {
        return getWrappedSession().get(theClass, id, lockOptions);
    }

    @Override
    public Object get(final String entityName, final Serializable id) {
        return getWrappedSession().get(entityName, id);
    }

    @Override
    public Object get(final String entityName, final Serializable id, final LockMode lockMode) {
        return getWrappedSession().get(entityName, id, lockMode);
    }

    @Override
    public Object get(final String entityName, final Serializable id, final LockOptions lockOptions) {
        return getWrappedSession().get(entityName, id, lockOptions);
    }

    @Override
    public String getEntityName(final Object object) {
        return getWrappedSession().getEntityName(object);
    }

    @Override
    public IdentifierLoadAccess byId(final String entityName) {
        return getWrappedSession().byId(entityName);
    }

    @Override
    public <T> MultiIdentifierLoadAccess<T> byMultipleIds(final Class<T> entityClass) {
        return getWrappedSession().byMultipleIds(entityClass);
    }

    @Override
    public MultiIdentifierLoadAccess byMultipleIds(final String entityName) {
        return getWrappedSession().byMultipleIds(entityName);
    }

    @Override
    public <T> IdentifierLoadAccess<T> byId(final Class<T> entityClass) {
        return getWrappedSession().byId(entityClass);
    }

    @Override
    public NaturalIdLoadAccess byNaturalId(final String entityName) {
        return getWrappedSession().byNaturalId(entityName);
    }

    @Override
    public <T> NaturalIdLoadAccess<T> byNaturalId(final Class<T> entityClass) {
        return getWrappedSession().byNaturalId(entityClass);
    }

    @Override
    public SimpleNaturalIdLoadAccess bySimpleNaturalId(final String entityName) {
        return getWrappedSession().bySimpleNaturalId(entityName);
    }

    @Override
    public <T> SimpleNaturalIdLoadAccess<T> bySimpleNaturalId(final Class<T> entityClass) {
        return getWrappedSession().bySimpleNaturalId(entityClass);
    }

    @Override
    public Filter enableFilter(final String filterName) {
        return getWrappedSession().enableFilter(filterName);
    }

    @Override
    public Filter getEnabledFilter(final String filterName) {
        return getWrappedSession().getEnabledFilter(filterName);
    }

    @Override
    public void disableFilter(final String filterName) {
        getWrappedSession().disableFilter(filterName);
    }

    @Override
    public SessionStatistics getStatistics() {
        return getWrappedSession().getStatistics();
    }

    @Override
    public boolean isReadOnly(final Object entityOrProxy) {
        return getWrappedSession().isReadOnly(entityOrProxy);
    }

    @Override
    public void setReadOnly(final Object entityOrProxy, final boolean readOnly) {
        getWrappedSession().setReadOnly(entityOrProxy, readOnly);
    }

    @Override
    public void doWork(final Work work) throws HibernateException {
        getWrappedSession().doWork(work);
    }

    @Override
    public <T> T doReturningWork(final ReturningWork<T> work) throws HibernateException {
        return getWrappedSession().doReturningWork(work);
    }

    @Override
    public Connection disconnect() {
        return getWrappedSession().disconnect();
    }

    @Override
    public void reconnect(final Connection connection) {
        getWrappedSession().reconnect(connection);
    }

    @Override
    public boolean isFetchProfileEnabled(final String name) throws UnknownProfileException {
        return getWrappedSession().isFetchProfileEnabled(name);
    }

    @Override
    public void enableFetchProfile(final String name) throws UnknownProfileException {
        getWrappedSession().enableFetchProfile(name);
    }

    @Override
    public void disableFetchProfile(final String name) throws UnknownProfileException {
        getWrappedSession().disableFetchProfile(name);
    }

    @Override
    public TypeHelper getTypeHelper() {
        return getWrappedSession().getTypeHelper();
    }

    @Override
    public LobHelper getLobHelper() {
        return getWrappedSession().getLobHelper();
    }

    @Override
    public void addEventListeners(final SessionEventListener... listeners) {
        getWrappedSession().addEventListeners(listeners);
    }

    @Override
    @Deprecated
    public boolean isFlushBeforeCompletionEnabled() {
        return getSessionImplementor().isFlushBeforeCompletionEnabled();
    }

    @Override
    public ActionQueue getActionQueue() {
        return getSessionImplementor().getActionQueue();
    }

    @Override
    public Object instantiate(final EntityPersister persister, final Serializable id) throws HibernateException {
        return getSessionImplementor().instantiate(persister, id);
    }

    @Override
    public void forceFlush(final EntityEntry e) throws HibernateException {
        getSessionImplementor().forceFlush(e);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void merge(final String entityName, final Object object, final Map copiedAlready) throws HibernateException {
        getSessionImplementor().merge(entityName, object, copiedAlready);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void persist(final String entityName, final Object object, final Map createdAlready) throws HibernateException {
        getSessionImplementor().persist(entityName, object, createdAlready);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void persistOnFlush(final String entityName, final Object object, final Map copiedAlready) {
        getSessionImplementor().persistOnFlush(entityName, object, copiedAlready);
    }

    @Override
    @Deprecated
    public void refresh(final String entityName, final Object object, final Map refreshedAlready) throws HibernateException {
        getSessionImplementor().refresh(entityName, object, refreshedAlready);
    }

    @Override
    @Deprecated
    public void delete(final String entityName, final Object child, final boolean isCascadeDeleteEnabled, final Set transientEntities) {
        getSessionImplementor().delete(entityName, child, isCascadeDeleteEnabled, transientEntities);
    }

    @Override
    @Deprecated
    public void removeOrphanBeforeUpdates(final String entityName, final Object child) {
        getSessionImplementor().removeOrphanBeforeUpdates(entityName, child);
    }

    @Override
    public SessionImplementor getSession() {
        return this;
    }

    @Override
    public boolean useStreamForLobBinding() {
        return getSessionImplementor().useStreamForLobBinding();
    }

    @Override
    public LobCreator getLobCreator() {
        return getSessionImplementor().getLobCreator();
    }

    @Override
    public SqlTypeDescriptor remapSqlTypeDescriptor(final SqlTypeDescriptor sqlTypeDescriptor) {
        return getSessionImplementor().remapSqlTypeDescriptor(sqlTypeDescriptor);
    }

    @Override
    public Integer getJdbcBatchSize() {
        return getWrappedSession().getJdbcBatchSize();
    }

    @Override
    public void setJdbcBatchSize(final Integer jdbcBatchSize) {
        getWrappedSession().setJdbcBatchSize(jdbcBatchSize);
    }
}
